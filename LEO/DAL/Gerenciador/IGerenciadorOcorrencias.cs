﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DAL.Models.LivroEletronico;
using Microsoft.AspNetCore.Http;

namespace LEO.Gerenciador
{
    public interface IGerenciadorOcorrencias
    {
        List<OcorrenciaLBG> GetTabelasOcorrenciaByLivroId(string livroId);
        List<Object> GetOcorrenciasAllLivroId(string livroId, string tabela);
        List<Object> GetOcorrenciasByPagina(string paginaId, string tabela);
        OcorrenciaLBG AddTabela(OcorrenciaLBG tabelaOcorrencia);
        Object AddOcorrencia(string tabelaOcorrenciaId, Object ocorrencia);
        Task<HttpResponseMessage> DownloadFile(string basePath, string id_file);
        Task<List<string>> GetFilesId(string basePath, string id_doc);
        Task<string> AddArquivoEmDocumento(IFormFile formFile, string baseName, string id_doc, string campo);
        Task<string> GetField(string nomeDoLivro, string id_doc, string v);
    }
}
