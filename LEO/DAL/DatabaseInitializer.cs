﻿// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
// 
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================

using DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Core;
using DAL.Core.Interfaces;
using DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG;

namespace DAL
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }

    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly IAccountManager _accountManager;
        private readonly ILogger _logger;

        public DatabaseInitializer(ApplicationDbContext context, IAccountManager accountManager, ILogger<DatabaseInitializer> logger)
        {
            _accountManager = accountManager;
            _context = context;
            _logger = logger;
        }

        public async Task SeedAsync()
        {
            await _context.Database.MigrateAsync().ConfigureAwait(false);

            if (!await _context.Users.AnyAsync())
            {
                _logger.LogInformation("Generating inbuilt accounts");

                const string adminRoleName = "administrator";
                const string userRoleName = "user";
                DocumentoIdentificacao adminDoc = new DocumentoIdentificacao
                {
                    Numero = "79229019000164",
                    Orgao = "Receita",
                    Descricao = "Cnpj",
                    TipoDocumento = TipoDocumento.Cnpj
                };
                DocumentoIdentificacao userDoc = new DocumentoIdentificacao
                {
                    Numero = "16371945440",
                    Orgao = "Receita",
                    Descricao = "Cpf",
                    TipoDocumento = TipoDocumento.Cpf
                };

                Endereco _ende = new Endereco
                {
                    Bairro = "Bairro",
                    Cep = "69000000",
                    Localidade = "Campina Grande",
                    Logradouro = "Rua dos Bobos",
                    Uf = UF.PB,
                    Complemento ="Complemento",
                    Numero = "Zero"
                };
                await _ensureRoleAsync(adminRoleName, "Administrador", ApplicationPermissions.GetAllPermissionValues());
                await _ensureRoleAsync(userRoleName, "Usuário padrão", new string[] { ApplicationPermissions.ViewUsers });

                await _createUserAsync("admin", "Admin@123", "Arnaldo Cezar Coelho",
                    "livroeletronicoocorrencia@gmail.com", "+1 (123) 000-0000", new string[] { adminRoleName },"");
                var _admin = await _context.Users.FirstAsync();
                var funcionario = await _createUserAsync("user", "senha123", "Jose da Silva",
                    email: "livroeletronicoocorrencia2@gmail.com", phoneNumber: "+1 (123) 000-0001", 
                    roles: new string[] { userRoleName }, 
                    userIdCreatedBy: _admin.Id);
                var locais = await _createLocaisAsync(_admin.Id);
                await _createConfigTablesAsync();
                _logger.LogInformation("Inbuilt account generation completed");
            }

        }

        private async Task _createConfigTablesAsync()
        {
            var configTabelaEntradaVeiculos = new ConfigTdo
            {
                ConfigProperties =
                new List<PropertyConfig>
                {
                    new PropertyConfig
                    {
                        LabelName = "id",
                        HtmlInput = "",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "id",
                        Tamanho = 12,
                    },

                    new PropertyConfig
                    {
                        LabelName = "paginaId",
                        HtmlInput = "",
                        IsColumn = false,
                        Mask = "",
                        PropertyName = "paginaId",
                    },

                    new PropertyConfig
                    {
                        LabelName = "Placa",
                        HtmlInput = "text",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "placa",
                        Tamanho = 9,
                    },

                    new PropertyConfig
                    {
                        LabelName = "Data Entrada",
                        HtmlInput = "date",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "entrada.data",
                        Tamanho = 10,
                    },
                    new PropertyConfig
                    {
                        LabelName = "Hora Entrada",
                        HtmlInput = "time",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "entrada.hora",
                        Tamanho = 7,
                    },

                    new PropertyConfig
                    {
                        LabelName = "Nome Motorista",
                        HtmlInput = "text",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "motorista.nome",
                        Tamanho = 30,
                    },

                    new PropertyConfig
                    {
                        LabelName = "Motorista Doc",
                        HtmlInput = "text",
                        IsColumn = false,
                        Mask = "",
                        PropertyName = "motorista.documento",
                        Tamanho = 30,
                    },

                    new PropertyConfig
                    {
                        LabelName = "Observaçao",
                        HtmlInput = "textarea",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "observacao",
                        Tamanho = 30,
                    }
                }
            };

            var configTabelaSaidaVeiculos = new ConfigTdo
            {
                ConfigProperties =
                new List<PropertyConfig>
                {
                    new PropertyConfig
                    {
                        LabelName = "id",
                        HtmlInput = "",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "id",
                        Tamanho = 12,
                    },

                    new PropertyConfig
                    {
                        LabelName = "paginaId",
                        HtmlInput = "",
                        IsColumn = false,
                        Mask = "",
                        PropertyName = "paginaId",
                    },

                    new PropertyConfig
                    {
                        LabelName = "Placa",
                        HtmlInput = "text",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "placa",
                        Tamanho = 9,
                    },

                    new PropertyConfig
                    {
                        LabelName = "Data Saida",
                        HtmlInput = "date",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "saida.data",
                        Tamanho = 10,
                    },
                    new PropertyConfig
                    {
                        LabelName = "Hora Saida",
                        HtmlInput = "time",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "saida.hora",
                        Tamanho = 7,
                    },

                    //new PropertyConfig
                    //{
                    //    LabelName = "Data Saida",
                    //    HtmlInput = "date",
                    //    IsColumn = true,
                    //    Mask = "",
                    //    PropertyName = "saida.data",
                    //    Tamanho = 10,
                    //},
                    //new PropertyConfig
                    //{
                    //    LabelName = "Hora Saida",
                    //    HtmlInput = "time",
                    //    IsColumn = true,
                    //    Mask = "",
                    //    PropertyName = "saida.hora",
                    //    Tamanho = 7,
                    //},

                    new PropertyConfig
                    {
                        LabelName = "Nome Motorista",
                        HtmlInput = "text",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "motorista.nome",
                        Tamanho = 30,
                    },

                    new PropertyConfig
                    {
                        LabelName = "Motorista Doc",
                        HtmlInput = "text",
                        IsColumn = false,
                        Mask = "",
                        PropertyName = "motorista.documento",
                        Tamanho = 30,
                    },

                    new PropertyConfig
                    {
                        LabelName = "Observaçao",
                        HtmlInput = "textarea",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "observacao",
                        Tamanho = 30,
                    }
                }
            };

            var configTabelaAnotacoes = new ConfigTdo
            {
                ConfigProperties =
                new List<PropertyConfig>
                {
                    new PropertyConfig
                    {
                        LabelName = "id",
                        HtmlInput = "",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "id",
                        Tamanho = 12,
                    },
                    new PropertyConfig
                    {
                        LabelName = "Data",
                        HtmlInput = "date",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "data",
                        Tamanho = 10,
                    },
                    new PropertyConfig
                    {
                        LabelName = "Hora",
                        HtmlInput = "time",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "hora",
                        Tamanho = 7,
                    },
                    new PropertyConfig
                    {
                        LabelName = "Observaçao",
                        HtmlInput = "textarea",
                        IsColumn = true,
                        Mask = "",
                        PropertyName = "observacao",
                        Tamanho = 30,
                    },
                    new PropertyConfig
                    {
                        LabelName = "Notificar",
                        HtmlInput = "checkbox",
                        IsColumn = false,
                        Mask = "",
                        PropertyName = "notificar",
                        Tamanho = 7,
                    },

                }
            };

            var conf1 = new OcorrenciasViewConfiguration
            {
                Name = "EntradaVeiculos",
                JsonConfigString = configTabelaEntradaVeiculos.ToString()
            };

            var conf3 = new OcorrenciasViewConfiguration
            {
                Name = "SaidaVeiculos",
                JsonConfigString = configTabelaSaidaVeiculos.ToString()
            };

            var conf2 = new OcorrenciasViewConfiguration
            {
                Name = "Anotacoes",
                JsonConfigString = configTabelaAnotacoes.ToString()
            };

            await _context.ConfiguracoesTabelas.AddAsync(conf1);
            await _context.ConfiguracoesTabelas.AddAsync(conf2);
            await _context.ConfiguracoesTabelas.AddAsync(conf3);
            await _context.SaveChangesAsync();

        }

        private async Task _ensureRoleAsync(string roleName, string description, string[] claims)
        {
            if ((await _accountManager.GetRoleByNameAsync(roleName)) == null)
            {
                ApplicationRole applicationRole = new ApplicationRole(roleName, description);

                var result = await this._accountManager.CreateRoleAsync(applicationRole, claims);

                if (!result.Item1)
                    throw new Exception($"Seeding \"{description}\" role failed. Errors: {string.Join(Environment.NewLine, result.Item2)}");
            }
        }

        private async Task<ApplicationUser> _createUserAsync(string userName, string password, 
            string fullName, string email, string phoneNumber, string[] roles, 
            string userIdCreatedBy)
        {
            ApplicationUser applicationUser = new ApplicationUser
            {
                UserName = userName,
                Nome = fullName,
                Email = email,
                PhoneNumber = phoneNumber,
                EmailConfirmed = false,
                IsEnabled = true,
                CreatedBy = userIdCreatedBy,
                
            };

            var result = await _accountManager.CreateUserAsync(applicationUser, roles, password);

            if (!result.Item1)
                throw new Exception($"Seeding \"{userName}\" user failed. Errors: {string.Join(Environment.NewLine, result.Item2)}");


            return applicationUser;
        }

        

        private async Task<List<LocalEntity>> _createLocaisAsync(string ownerIdCreatedBy)
        {
            // Avenida Da Recuperação, 7480, Córrego do Jenipapo, Recife - PE, CEP: 52291 - 000

            var endereco = new Endereco
            {
                Bairro = "Corrego do Jenipapo",
                Cep = "52291000",
                Complemento = "",
                Localidade = "Recife",
                Logradouro = "Avenida Da Recuperacao",
                Numero = "7480",
                
            };

            var endereco2 = new Endereco
            {
                Bairro = "Centro",
                Cep = "58400565",
                Complemento = "",
                Localidade = "Campina Grande",
                Logradouro = "Floriano",
                Numero = "574",

            };

            var local1 = new LocalEntity
            {
                CreatedBy = ownerIdCreatedBy,
                Endereco = endereco,
                CreatedDate = DateTime.Now,
                Descricao = "Construtora Escritorio"
            };
            var local2 = new LocalEntity
            {
                CreatedBy = ownerIdCreatedBy,
                Endereco = endereco2,
                CreatedDate = DateTime.Now,
                Descricao = "Filal de Campina Grande"
            };

            List<LocalEntity> locais = new List<LocalEntity>
            {
               local1, local2
            };
            await _context.Locais.AddRangeAsync(locais);
            _context.SaveChanges();
            return locais;
        }
    }
}
