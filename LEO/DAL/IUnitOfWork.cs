﻿using DAL.Repositories.Interfaces;
using LEO.Auth.DAL.Repositories.Interfaces;

namespace DAL
{
    public interface IUnitOfWork
    {
        ILivroRepository Livros { get; }
        ILocalRepository Locais { get; }
        IOcorrenciaRepository Ocorrencias { get; }
        ITurnoRepository Turnos { get; }
        IEscalaRepository Escalas { get; }
        ISetorRepository Setores { get; }
        IPaginaRepository Paginas { get; }
        IConfigOcorrenciaRepository ConfiguracoesDeTabelasDeOcorrencias { get; }
        string GetCurrentId { get; }
        int SaveChanges();
    }
}
