﻿
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using DAL.Repositories.Interfaces;
using DAL.Models.LivroEletronico;

namespace DAL.Repositories
{
    public class OcorrenciaRepository : Repository<OcorrenciaLBG>, 
        IOcorrenciaRepository
    {
        public OcorrenciaRepository(DbContext context) : base(context)
        { }




        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }
    }
}
