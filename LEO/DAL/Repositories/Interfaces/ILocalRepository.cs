﻿using DAL.Models.LivroEletronico;
using System.Collections;
using System.Collections.Generic;

namespace DAL.Repositories.Interfaces
{
    public interface ILocalRepository : IRepository<LocalEntity>
    {
        IEnumerable<LocalEntity> MeusLocais();
    }
}
