﻿using DAL.Models.LivroEletronico;

namespace DAL.Repositories.Interfaces
{
    public interface IOcorrenciaRepository : IRepository<OcorrenciaLBG>
    {
    }
}
