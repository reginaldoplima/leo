﻿using DAL.Repositories.Interfaces;
using LEO.Auth.DAL.Models.LivroEletronico;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Repositories.Interfaces
{
    public interface IPaginaRepository : IRepository<PaginaEntity>
    {
        IEnumerable<PaginaEntity> GetAllPaginaWithAllInformationByLivroId(string livroId);
        IEnumerable<PaginaEntity> UltimasPaginasFechadasByEscalaId(string escalaId);
    }
}
