﻿using DAL.Repositories.Interfaces;
using LEO.Auth.DAL.Models.LivroEletronico;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Repositories.Interfaces
{
    public interface ISetorRepository : IRepository<SetorEntity>
    {
        List<SetorEntity> GetMeusSetoresWithLocal();
    }
}
