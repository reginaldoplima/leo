﻿using DAL.Repositories.Interfaces;
using LEO.Auth.DAL.Models;
using System.Collections.Generic;

namespace LEO.Auth.DAL.Repositories.Interfaces
{
    public interface INotificacaoRepository : IRepository<Notificacao>
    {
        List<Notificacao> MinhasNotificacoes(string userId);
    }
}
