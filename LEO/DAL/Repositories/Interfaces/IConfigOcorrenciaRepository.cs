﻿using DAL.Repositories.Interfaces;
using LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Repositories.Interfaces
{
    public interface IConfigOcorrenciaRepository : IRepository<OcorrenciasViewConfiguration>
    {
    }
}
