﻿using DAL.Models.LivroEletronico;
using System.Collections.Generic;

namespace DAL.Repositories.Interfaces
{
    public interface IEscalaRepository : IRepository<EscalaEntity>
    {
        IEnumerable<EscalaEntity> AllEscalasByUserId(string userId);
        IEnumerable<EscalaEntity> AllEscalasWithUserBySetorId(string setorId);
    }
}