﻿using DAL.Models.LivroEletronico;
using System.Collections.Generic;

namespace DAL.Repositories.Interfaces
{
    public interface ILivroRepository : IRepository<LivroEntity>
    {
    }
}