﻿using DAL.Models;

namespace DAL.Repositories.Interfaces
{
    public interface IEnderecoRepository : IRepository<Endereco>
    {
    }
}
