﻿using DAL.Models;
using DAL.Models.LivroEletronico;

namespace DAL.Repositories.Interfaces
{
    public interface ITurnoRepository : IRepository<TurnoEntity>
    {
    }
}
