﻿using DAL;
using DAL.Repositories;
using LEO.Auth.DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LEO.Auth.DAL.Repositories
{
    public class SetorRepository : Repository<SetorEntity>, ISetorRepository
    {
        public SetorRepository(DbContext context) : base(context)
        { }
        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }

        public List<SetorEntity> GetMeusSetoresWithLocal()
        {
            List<SetorEntity> meusSetores = new List<SetorEntity>(); ;
            var locais = _appContext.Locais
                .Where(l => l.Setores.All( 
                    s=> s.Escalas.All( e=> e.ApplicationUserId.Equals(_appContext.CurrentUserId ) ) ) );
            foreach (var item in locais)
            {
                var setor = _appContext.Setores.Where(s => s.LocalEntityId.Equals(item.Id)).ToList();
                setor.ForEach(s => meusSetores.Add(s));
            }
            return meusSetores.ToList();
        }
    }
}
