﻿
using System.Collections.Generic;
using System.Linq;
using DAL.Models.LivroEletronico;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class EscalaRepository : Repository<EscalaEntity>, IEscalaRepository
    {
        public EscalaRepository(DbContext context) : base(context)
        { }
        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }

        public IEnumerable<EscalaEntity> AllEscalasByUserId(string userId)
        {
            return _appContext.Escalas
                .Where( e=> e.ApplicationUserId.Equals(userId) )
                .Include(e=> e.Setor).ThenInclude( s=> s.Local ).ThenInclude(l=>l.Endereco).ToList();
        }

        public IEnumerable<EscalaEntity> AllEscalasWithUserBySetorId(string setorId)
        {
            return _appContext.Escalas
               .Where(e => e.SetorEntityId.Equals(setorId) ).Include(e => e.ApplicationUser).ToList();
        }
    }
}
