﻿
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using DAL.Repositories.Interfaces;
using DAL.Models.LivroEletronico;

namespace DAL.Repositories
{
    public class TurnoRepository : Repository<TurnoEntity>, ITurnoRepository
    {
        public TurnoRepository(DbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }
    }
}
