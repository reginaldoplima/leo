﻿using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class EnderecoRepository : Repository<Endereco>, IEnderecoRepository
    {
        public EnderecoRepository(ApplicationDbContext context) : base(context)
        { }


        //public IEnumerable<Usuario> GetTopActiveCustomers(int count)
        //{
        //    throw new NotImplementedException();
        //}


        //public IEnumerable<Usuario> GetAllCustomersData()
        //{
        //    return appContext.Customers
        //        .Include(c => c.Orders).ThenInclude(o => o.OrderDetails).ThenInclude(d => d.Product)
        //        .Include(c => c.Orders).ThenInclude(o => o.Cashier)
        //        .OrderBy(c => c.Name)
        //        .ToList();
        //}



        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }
    }
}
