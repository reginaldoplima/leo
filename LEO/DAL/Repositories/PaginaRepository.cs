﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using DAL.Repositories;
using LEO.Auth.DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LEO.Auth.DAL.Repositories
{
    public class PaginaRepository : Repository<PaginaEntity>, IPaginaRepository
    {
        public PaginaRepository(DbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }

        public IEnumerable<PaginaEntity> GetAllPaginaWithAllInformationByLivroId(string livroId)
        {
            var paginas = _appContext.Paginas
                .Include(p => p.Escala).ThenInclude(e => e.ApplicationUser)
                .Include(p => p.EscalaAnterior).ThenInclude(e => e.ApplicationUser)
                .Include(p => p.Livro).ThenInclude(l=>l.Setor).ThenInclude(s=>s.Local)
                .Include(p=>p.InformacoesAberturaPagina).Include(p=>p.InformacoesFechamentoPagina);
            return paginas;
        }

        public IEnumerable<PaginaEntity> UltimasPaginasFechadasByEscalaId(string escalaId)
        {
           var paginas = _appContext.Paginas
                .Where( p => !String.IsNullOrEmpty(p.EscalaEntityId)
                   && p.EscalaEntityId.Equals(escalaId)
                   && !String.IsNullOrEmpty(p.InformacoesFechamentoPaginaEntityId))
                .Include( p => p.Escala ).ThenInclude( e => e.ApplicationUser )
                .Include( p => p.EscalaAnterior ).ThenInclude( e => e.ApplicationUser )
                .Include( p => p.Livro ).ThenInclude( l=>l.Setor ).ThenInclude( s=>s.Local )
                .Include( p=>p.InformacoesAberturaPagina ).Include( p=>p.InformacoesFechamentoPagina );
            var retVal = paginas
                .OrderByDescending( p => p.Numero ).Take(2);
            return retVal;
        }
    }
}
