﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class DocumentoIdentificacaoRepository : Repository<DocumentoIdentificacao>, 
        IDocumentoRepository
    {
        public DocumentoIdentificacaoRepository(DbContext context) : base(context)
        { }




        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }
    }
   
}
