﻿using Microsoft.EntityFrameworkCore;
using DAL.Repositories.Interfaces;
using DAL.Models.LivroEletronico;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class LocalRepository : Repository<LocalEntity>, ILocalRepository
    {
        public LocalRepository(ApplicationDbContext context) : base(context)
        { }


        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }

        public IEnumerable<LocalEntity> MeusLocais()
        {
            return _appContext.
                Locais.Include( l=> l.Endereco )
                .Where(l => l.CreatedBy.Equals(_appContext.CurrentUserId)).ToList();
        }
    }
}
