﻿using DAL;
using DAL.Repositories;
using DAL.Repositories.Interfaces;
using LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Repositories.Interfaces
{
    public class ConfigOcorrenciaRepository : Repository<OcorrenciasViewConfiguration>, IConfigOcorrenciaRepository
    {
        public ConfigOcorrenciaRepository(DbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }
    }
}
