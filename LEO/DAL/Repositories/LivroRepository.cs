﻿
using Microsoft.EntityFrameworkCore;
using DAL.Repositories.Interfaces;
using DAL.Models.LivroEletronico;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class LivroRepository : Repository<LivroEntity>, ILivroRepository
    {
        public LivroRepository(DbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }
    }
}
