﻿using DAL;
using DAL.Repositories;
using LEO.Auth.DAL.Models;
using LEO.Auth.DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LEO.Auth.DAL.Repositories
{
    public class NotificacaoRepository : Repository<Notificacao>, INotificacaoRepository
    {
        public NotificacaoRepository(DbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext
        {
            get { return (ApplicationDbContext)_context; }
        }

        public List<Notificacao> MinhasNotificacoes(string userId)
        {
            var notificacoes = _appContext
                .Notificacoes.Where(n => n.ToUserId.Equals(userId)).ToList();
            return notificacoes;
        }
    }
}
