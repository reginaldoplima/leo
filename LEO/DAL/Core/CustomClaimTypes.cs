﻿namespace DAL.Core
{
    public static class CustomClaimTypes
    {
        ///<summary>A claim that specifies the permission of an entity</summary>
        public const string Permission = "permission";

        ///<summary>A claim that specifies the full name of an entity</summary>
        public const string Nome = "fullname";

        ///<summary>A claim that specifies the job title of an entity</summary>
        public const string Cpf = "cpf";

        ///<summary>A claim that specifies the email of an entity</summary>
        public const string Email = "email";

        ///<summary>A claim that specifies the phone number of an entity</summary>
        public const string Phone = "phone";

        ///<summary>A claim that specifies the configuration/customizations of an entity</summary>
        public const string Configuration = "configuration";
        
        ///<summary>A claim that specifies the time of expiration of an entity</summary>
        public const string Expiration = "expiration";

        ///<summary>A claim that specifies the time of matricula of an entity</summary>
        public const string Matricula = "matricula";
       
        ///<summary>A claim that specifies the time of matricula of an entity</summary>
        public const string Cnpj = "cnpj";
        ///<summary>escpecifica os id dos  permitidos setores</summary>
        public const string Setores = "setores";
    }
}
