﻿// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
// 
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace DAL.Core
{
    public static class ApplicationPermissions
    {
        public static ReadOnlyCollection<ApplicationPermission> AllPermissions;


        public const string UsersPermissionGroupName = "Permissões de Usuário";
        public static ApplicationPermission ViewUsers = new ApplicationPermission("Ver Usuários", "users.view",
            UsersPermissionGroupName, "Ver informações de outros Usuários");
        public static ApplicationPermission ManageUsers = new ApplicationPermission("Gerenciar  Usuário",
            "users.manage", UsersPermissionGroupName, 
            "Permite criar, deletar e modificar outros usuários");

        public const string RolesPermissionGroupName = "Permissões";
        public static ApplicationPermission ViewRoles = new ApplicationPermission("Ver permissões", "roles.view", 
            RolesPermissionGroupName, "Permite ver todas as permissões");
        public static ApplicationPermission ManageRoles = new ApplicationPermission("Gerenciar Permissões", 
            "roles.manage", RolesPermissionGroupName, "Permite criar, deletar e modificar permissões");
        public static ApplicationPermission AssignRoles = new ApplicationPermission("Atribuir Permissões", 
            "roles.assign", RolesPermissionGroupName, "Permite modificar as permissões de um usuário");




        static ApplicationPermissions()
        {
            List<ApplicationPermission> allPermissions = new List<ApplicationPermission>()
            {
                ViewUsers,
                ManageUsers,

                ViewRoles,
                ManageRoles,
                AssignRoles
            };

            AllPermissions = allPermissions.AsReadOnly();
        }

        public static ApplicationPermission GetPermissionByName(string permissionName)
        {
            return AllPermissions.Where(p => p.Name == permissionName).FirstOrDefault();
        }

        public static ApplicationPermission GetPermissionByValue(string permissionValue)
        {
            return AllPermissions.Where(p => p.Value == permissionValue).FirstOrDefault();
        }

        public static string[] GetAllPermissionValues()
        {
            return AllPermissions.Select(p => p.Value).ToArray();
        }

        public static string[] GetAdministrativePermissionValues()
        {
            return new string[] { ManageUsers, ManageRoles, AssignRoles };
        }
    }





    public class ApplicationPermission
    {
        public ApplicationPermission()
        { }

        public ApplicationPermission(string name, string value, string groupName, string description = null)
        {
            Name = name;
            Value = value;
            GroupName = groupName;
            Description = description;
        }



        public string Name { get; set; }
        public string Value { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }


        public override string ToString()
        {
            return Value;
        }


        public static implicit operator string(ApplicationPermission permission)
        {
            return permission.Value;
        }
    }
}
