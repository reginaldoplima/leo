﻿

using System;
using DAL.Repositories;
using DAL.Repositories.Interfaces;
using LEO.Auth.DAL.Repositories;
using LEO.Auth.DAL.Repositories.Interfaces;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;
        private IDocumentoRepository _documentos;
        private ILocalRepository _locais;
        private ITurnoRepository _turnos;
        private ISetorRepository _setores;
        private ILivroRepository _livros;
        private IEscalaRepository _escalas;
        private IOcorrenciaRepository _ocorrencias;
        private IPaginaRepository _paginas;
        private IConfigOcorrenciaRepository _configOcorrenciaRepository;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public ILivroRepository Livros
        {
            get
            {
                if (_livros == null)
                    _livros = new LivroRepository(_context);
                return _livros;
            }
        }

        public IConfigOcorrenciaRepository ConfiguracoesDeTabelasDeOcorrencias
        {
            get
            {
                if (_configOcorrenciaRepository == null)
                    _configOcorrenciaRepository = new ConfigOcorrenciaRepository(_context);
                return _configOcorrenciaRepository;
            }
        }

        public IPaginaRepository Paginas
        {
            get
            {
                if (_paginas == null)
                    _paginas = new PaginaRepository(_context);
                return _paginas;
            }
        }

        public ISetorRepository Setores
        {
            get
            {
                if (_setores == null)
                    _setores = new SetorRepository(_context);
                return _setores;
            }
        }

        public ILocalRepository Locais
        {
            get
            {
                if (_locais == null)
                    _locais = new LocalRepository(_context);
                return _locais;
            }
        }

        public IOcorrenciaRepository Ocorrencias
        {
            get
            {
                if (_ocorrencias == null)
                    _ocorrencias = new OcorrenciaRepository(_context);
                return _ocorrencias;
            }
        }


        public ITurnoRepository Turnos
        {
            get
            {
                if (_turnos == null)
                    _turnos = new TurnoRepository(_context);
                return _turnos;
            }
        }

        public IDocumentoRepository Documentos
        {
            get
            {
                if (_documentos == null)
                    _documentos = new DocumentoIdentificacaoRepository(_context);
                return _documentos;
            }
        }

        public IEscalaRepository Escalas
        {
            get
            {
                if (_escalas == null)
                    _escalas = new EscalaRepository(_context);
                return _escalas;
            }
        }

        public string GetCurrentId
        {
            get { return _context.CurrentUserId; }
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
