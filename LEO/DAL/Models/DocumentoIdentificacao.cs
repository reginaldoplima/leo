﻿using DAL.Core.Utils;
using System;

namespace DAL.Models
{
    public class DocumentoIdentificacao
    {

        public DocumentoIdentificacao()
        {
        }

        public DocumentoIdentificacao(string numero,string applicationUserId, 
            string orgao="", string descricao="")
        {
            if (String.IsNullOrEmpty(applicationUserId))
            {
                throw new ArgumentException("applicationUserId não pode ser null or empty");
            }
            var cpf = new CPFValidator(numero);
            var isCpf = cpf.IsValid();
            if (isCpf)
            {
                Numero = cpf.ToString();
                TipoDocumento = TipoDocumento.Cpf;
                ApplicationUserId = applicationUserId;
            }
            else
            {
                var cnpj = new CNPJValidator(numero);
                var isCnpj = cnpj.IsValid();
                if (isCnpj)
                {
                    Numero = cnpj.ToString();
                    TipoDocumento = TipoDocumento.Cnpj;
                    ApplicationUserId = applicationUserId;
                }
                else
                {
                    throw new ArgumentException("Numero Invalido para cpf e cnpj");
                }
            }

        }

        public int Id { get; set; }
        public string Numero { get; set; }
        public string Descricao { get; set; }
        public string Orgao { get; set; }
        public TipoDocumento TipoDocumento { get; set; }
        //
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual string ApplicationUserId { get; set; }

    }

    public enum TipoDocumento
    {
        Cpf,
        Cnpj
    }
}
