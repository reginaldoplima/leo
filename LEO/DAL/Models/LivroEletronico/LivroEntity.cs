using LEO.Auth.DAL.Models.LivroEletronico;
using System;
using System.Collections.Generic;

namespace DAL.Models.LivroEletronico
{
    public class LivroEntity : AuditableEntity
    {
        public string Id { get; set; }

        public string PaginaAnteriorId { get; set; }
        public string ProximaPaginaId { get; set; }

        public DateTime DataCriacao { get; set; }

        public string SetorId { get; set; }
        public SetorEntity Setor { get; set; }
        public virtual List<PaginaEntity> Paginas { get; set; }

        public string OcorrenciaLBGId { get; set; }
        public virtual OcorrenciaLBG TabelaOcorrencia { get; set; }
    }
}