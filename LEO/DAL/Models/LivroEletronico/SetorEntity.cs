﻿using DAL.Models;
using DAL.Models.LivroEletronico;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Models.LivroEletronico
{
    public class SetorEntity : AuditableEntity
    {
        public string Id { get; set; }
        public string Nome { get; set; }

        public string  LocalEntityId { get; set; }
        public LocalEntity Local { get; set; }

        public string LivroEntityId { get; set; }
        public List<LivroEntity> Livros { get; set; }

        public virtual List<EscalaEntity> Escalas { get; set; }
    }
}
