﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Models.LivroEletronico
{
    public class InformacoesFechamentoPaginaEntity
    {
        public string Id { get; set; }
        public string IdUsuarioRecebedor { get; set; }
        public DateTime DataFechamento { get; set; }
        public string Obeservacoes { get; set; }
    }
}
