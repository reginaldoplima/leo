﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG
{
    public class OcorrenciasViewConfiguration
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string JsonConfigString { get; set; }
    }
}
