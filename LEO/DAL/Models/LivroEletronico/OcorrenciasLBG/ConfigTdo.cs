﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG
{
    public class ConfigTdo
    {
        public List<PropertyConfig> ConfigProperties{ get; set; }

        public override string ToString()
        {
            var jsonConfigString = JsonConvert.SerializeObject(ConfigProperties);
            return jsonConfigString;
        }
    }

    public class PropertyConfig
    {
        public string  PropertyName { get; set; }
        public string  HtmlInput { get; set; }
        public string  LabelName { get; set; }
        public bool    IsColumn { get; set; }
        public string  Mask { get; set; }
        public int     Tamanho { get; set; }
    }
}
