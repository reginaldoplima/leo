using LEO.Auth.DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models.LivroEletronico
{
    // referente a uma tabela do lbg
    public  class OcorrenciaLBG : AuditableEntity
    {
        public string Id { get; set; }

        public string LivroEntityId { get; set; }
        public DateTime Data { get; set; }
        public string NomeTabela { get; set; }
        public string Descricao { get; set; }
        public string EmailsParaComunicacao { get; set; }
        public bool IsComunicacaoObrigatoria { get; set; }
        public bool Confirmado { get; set; }
        public LivroEntity LivroEntity { get; set; }

        public string OcorrenciasViewConfigurationId { get; set; }
        public OcorrenciasViewConfiguration ViewJsonConfig { get; set; }

    }
}