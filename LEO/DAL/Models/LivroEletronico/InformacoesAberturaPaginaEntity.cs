﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Models.LivroEletronico
{
    public class InformacoesAberturaPaginaEntity
    {
        public string Id { get; set; }
        public string IdUsuarioAnterior { get; set; }
        public DateTime DataAbertura { get; set; }
        public string Obeservacoes { get; set; }
    }
}
