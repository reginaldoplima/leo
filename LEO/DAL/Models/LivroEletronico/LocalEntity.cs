using LEO.Auth.DAL.Models.LivroEletronico;
using System.Collections.Generic;

namespace DAL.Models.LivroEletronico
{
    public class LocalEntity : AuditableEntity
    {
        public string Id { get; set; }
        public string Descricao { get; set; }

        public List<SetorEntity> Setores { get; set; }

        public int EnderecoId { get; set; }
        public Endereco Endereco { get; set; }
    }
}