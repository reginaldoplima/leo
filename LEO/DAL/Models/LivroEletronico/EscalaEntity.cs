﻿using LEO.Auth.DAL.Models.LivroEletronico;
using System;
using System.Collections.Generic;

namespace DAL.Models.LivroEletronico
{
    public class EscalaEntity
    {
        public string Id { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public string Tags { get; set; }
        public string SetorEntityId { get; set; }
        public virtual SetorEntity Setor { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public bool IsOpen { get; set; }
    }
}