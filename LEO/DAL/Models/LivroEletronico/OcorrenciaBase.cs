﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Models.LivroEletronico
{
    public abstract class OcorrenciaBase
    {
        public string Id { get; set; }
        public string PaginaId { get; set; }
    }
}
