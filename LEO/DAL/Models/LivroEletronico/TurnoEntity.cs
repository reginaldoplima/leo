using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models.LivroEletronico
{
    public  class TurnoEntity : AuditableEntity
    {
        public string Id { get; set; }

        public string LivroId { get; set; }
        public virtual LivroEntity Livro { get; set; }

    }
}