﻿using DAL.Models.LivroEletronico;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Models.LivroEletronico
{
    public class PaginaEntity
    {
        public string  Id { get; set; }
        public int Numero { get; set; }
        public string  InformacoesAberturaPaginaId { get; set; }
        public virtual InformacoesAberturaPagina InformacoesAberturaPagina { get; set; }
        public string  InformacoesFechamentoPaginaEntityId { get; set; }
        public virtual InformacoesFechamentoPaginaEntity InformacoesFechamentoPagina { get; set; }
        public string  EscalaEntityId { get; set; }
        public virtual EscalaEntity Escala { get; set; }
        public string  EscalaAnteriorId { get; set; }
        public virtual EscalaEntity EscalaAnterior { get; set; }
        public string  LivroEntityId { get; set; }
        public virtual LivroEntity Livro { get; set; }

        public string Criador()
        {
            var nomeCriador = "SISTEMA";
            if (Escala != null && Escala.ApplicationUser != null)
            {
                nomeCriador = Escala.ApplicationUser.Nome;
            }
            return nomeCriador;
        }

        public string RecebidoDe()
        {
            string nomeUsuarioDaEscalaAnterior = "SISTEMA";
            nomeUsuarioDaEscalaAnterior = EscalaAnterior?.ApplicationUser?.Nome;
            return nomeUsuarioDaEscalaAnterior;
        }

        public string Endereco()
        {
            string enderecoDoLivro = "sistema";
            enderecoDoLivro = Livro?.Setor?.Local?.Endereco.ToString();
            return enderecoDoLivro;
        }

        public string Setor()
        {
            string setor = "sistema";
            setor = Livro?.Setor?.Nome;
            return setor;
        }

        public bool IsClosedPage()
        {
            var contemFechamento = !String.IsNullOrEmpty(InformacoesFechamentoPaginaEntityId);
            return contemFechamento;
        }

    }
}
