﻿using System;

namespace DAL.Models
{
    public class Endereco
    {
        public string Id { get; set; }
        public string Localidade { get; set; }
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        public string Numero { get; set; }
        public string Cep { get; set; }
        public UF Uf { get; set; }
        public string Unidade { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public string ApplicationUserId { get; set; }

        public override string ToString()
        {
            string endereco = String.Format("{0}, {1}. {2}", Logradouro,Numero, Bairro);
            return endereco;
        }
    }
}
