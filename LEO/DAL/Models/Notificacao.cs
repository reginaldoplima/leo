﻿using LEO.Auth.DAL.Models.LivroEletronico;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEO.Auth.DAL.Models
{
    public class Notificacao
    {
        public string Id { get; set; }
        public DateTime DataEnvio { get; set; }
        public DateTime DataVisualizacao { get; set; }
        public string ToUserId { get; set; }
        public string PaginaEntityId { get; set; }
        public PaginaEntity Pagina { get; set; }
        public string Mensagem { get; set; }
    }
}
