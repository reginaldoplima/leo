﻿// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
// 
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================

using DAL.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using DAL.Models.Interfaces;
using DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG;
using LEO.Auth.DAL.Models;

namespace DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public string CurrentUserId { get; set; }

        //todo fazer um contexto so pra isso
        public DbSet<OcorrenciaLBG> TabelasOcorrencias { get; set; }

        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<LocalEntity> Locais { get; set; }
        public DbSet<SetorEntity> Setores { get; set; }
        public DbSet<TurnoEntity> Turnos { get; set; }
        public DbSet<PaginaEntity> Paginas { get; set; }
        public DbSet<LivroEntity> Livros { get; set; }
        public DbSet<EscalaEntity> Escalas { get; set; }
        public DbSet<Notificacao> Notificacoes { get; set; }
        public DbSet<OcorrenciasViewConfiguration> ConfiguracoesTabelas { get; set; }
        public DbSet<DocumentoIdentificacao> DocumentosDeIdentificacao { get; set; }


        public ApplicationDbContext(DbContextOptions options) : base(options)
        { }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().HasMany(u => u.Claims).WithOne().HasForeignKey(c => c.UserId)
                .IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>().HasMany(u => u.Roles).WithOne().HasForeignKey(r => r.UserId)
                .IsRequired().OnDelete(DeleteBehavior.Cascade);

            builder.Entity<ApplicationRole>().HasMany(r => r.Claims).WithOne().HasForeignKey(c => c.RoleId)
                .IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationRole>().HasMany(r => r.Users).WithOne().HasForeignKey(r => r.RoleId)
                .IsRequired().OnDelete(DeleteBehavior.Cascade);


            builder.Entity<Endereco>().Property(c => c.Logradouro).IsRequired().HasMaxLength(100);
            builder.Entity<Endereco>().HasIndex(c => c.Cep);
            builder.Entity<Endereco>().Property(c => c.Cep).HasMaxLength(8);
            builder.Entity<Endereco>().Property(c => c.Localidade).HasMaxLength(50);
            builder.Entity<Endereco>().Property(c => c.Bairro).HasMaxLength(70);
            
            builder.Entity<Endereco>().ToTable($"App{nameof(this.Enderecos)}");
            builder.Entity<LocalEntity>().ToTable($"Locais")
                .HasOne<Endereco>( (l) => l.Endereco ).WithOne().HasForeignKey("Endereco", "EnderecoId");


            builder.Entity<OcorrenciasViewConfiguration>()
                .HasMany<OcorrenciaLBG>()
                .WithOne().HasForeignKey(o=> o.OcorrenciasViewConfigurationId);
            
            builder.Entity<LivroEntity>()
                .HasOne( l=> l.TabelaOcorrencia ).WithOne().HasForeignKey("LivroEntity", "LivroEntityId");

            builder.Entity<EscalaEntity>()
                .HasOne(e => e.Setor).WithMany(s => s.Escalas).HasForeignKey(e=> e.SetorEntityId);

            //builder.Entity<ProductCategory>().Property(p => p.Name).IsRequired().HasMaxLength(100);
            //builder.Entity<ProductCategory>().Property(p => p.Description).HasMaxLength(500);
            //builder.Entity<ProductCategory>().ToTable($"App{nameof(this.ProductCategories)}");

            //builder.Entity<Product>().Property(p => p.Name).IsRequired().HasMaxLength(100);
            //builder.Entity<Product>().HasIndex(p => p.Name);
            //builder.Entity<Product>().Property(p => p.Description).HasMaxLength(500);
            //builder.Entity<Product>().Property(p => p.Icon).IsUnicode(false).HasMaxLength(256);
            //builder.Entity<Product>().HasOne(p => p.Parent).WithMany(p => p.Children).OnDelete(DeleteBehavior.Restrict);
            //builder.Entity<Product>().ToTable($"App{nameof(this.Products)}");

            //builder.Entity<Order>().Property(o => o.Comments).HasMaxLength(500);
            //builder.Entity<Order>().ToTable($"App{nameof(this.Orders)}");

            //builder.Entity<OrderDetail>().ToTable($"App{nameof(this.OrderDetails)}");
        }




        public override int SaveChanges()
        {
            UpdateAuditEntities();
            return base.SaveChanges();
        }


        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            UpdateAuditEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(cancellationToken);
        }


        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        private void UpdateAuditEntities()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));


            foreach (var entry in modifiedEntries)
            {
                var entity = (IAuditableEntity)entry.Entity;
                DateTime now = DateTime.UtcNow;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedDate = now;
                    entity.CreatedBy = !String.IsNullOrEmpty(entity.CreatedBy) ? entity.CreatedBy : CurrentUserId;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                }

                entity.UpdatedDate = now;
                entity.UpdatedBy = !String.IsNullOrEmpty(entity.CreatedBy) ? entity.CreatedBy : CurrentUserId;
            }
        }
    }
}
