﻿// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
//
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================

import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { LoginComponent } from "./components/login/login.component";
import { CadastroComponent } from "./components/cadastro/cadastro.component";
import { ConfirmaComponent } from "./components/cadastro/confirma.component";
import { HomeComponent } from "./components/home/home.component";
import { CustomersComponent } from "./components/customers/customers.component";
import { OrdersComponent } from "./components/orders/orders.component";
// import { SettingsComponent } from "./components/settings/settings.component";
import { AboutComponent } from "./components/about/about.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";

import { AuthService } from "./services/auth.service";
import { AuthGuard } from "./services/auth-guard.service";
import { LocaisComponent } from "./components/locais/locais-management.component";
import { TurnoResolve } from "./services/turno/turno.resolve";
import { LocalResolve } from "./services/local/local.resolve";
import { TurnoListComponent } from "./components/turno/turno-list.component";
import { MeusTurnosListResolve } from "./services/turno/meus-turno-list.resolve";
import { OcorrenciaComponent } from "./components/ocorrencia/ocorrencia-management.component";

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: "", component: HomeComponent, canActivate: [AuthGuard],
               data: { title: "Inicio" } },
            { path: "login", component: LoginComponent,
               data: { title: "Login" } },
            { path: "cadastro", component: CadastroComponent,
               data: { title: "Cadastro" } },
            { path: "confirma/:token", component: ConfirmaComponent,
               data : { title: "Confirmação" }, pathMatch: "full" },
            { path: "usuarios", component: CustomersComponent, canActivate: [AuthGuard],
               data: { title: "Usuários" } },
            { path: "pagina/:turnoId", component: OcorrenciaComponent, canActivate: [AuthGuard],
               data: { title: "Ocorrencias Do Turno" } },
            // { path: "funcionario/turnos", component: TurnoListComponent, canActivate: [AuthGuard],
            //    data: { title: "Meus Turnos" }, resolve: { turnos: MeusTurnosListResolve } },
            { path: "orders", component: OrdersComponent, canActivate: [AuthGuard],
               data: { title: "Ocorrências" } },
            // { path: "settings", component: SettingsComponent, canActivate: [AuthGuard],
            //    data: { title: "Configurações" } },
            { path: "about", component: AboutComponent,
               data: { title: "Nossa Empresa" } },
            { path: "home", redirectTo: "/", pathMatch: "full" },
            { path: "**", component: NotFoundComponent,
               data: { title: "Página Não Encontrada" } },
        ], { enableTracing: false , useHash: true }),
    ],
    exports: [
        RouterModule,
    ],
    providers: [
        AuthService, AuthGuard,
    ],
})
export class AppRoutingModule { }
