import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PtBrDatePipe } from "./date-ptBr.pipe";

@NgModule({
    declarations: [ PtBrDatePipe ],
    exports: [ PtBrDatePipe ],
    providers: [],
})
export class DateFormatModule {}
