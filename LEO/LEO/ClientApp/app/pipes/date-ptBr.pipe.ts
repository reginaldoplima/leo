// https://stackoverflow.com/questions/43264992/angular-4-date-pipe-converting-wrongily

import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "ptBrDate",
})
export class PtBrDatePipe implements PipeTransform {

   transform(value: string): any {

    if (!value) {
      return "";
    }
    const DIAS_SEMANA = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];
    const dateValue = new Date(value);
    const dtString = `${DIAS_SEMANA[dateValue.getDay()]} - ${dateValue.toTimeString().slice(0, 5)}`;
    return dtString;
  }
}
