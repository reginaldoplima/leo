import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../_shared/shared.module';
import { RolesChangedEventArg, AccountService} from '../../services/account/account.service';
import { AppTranslationService } from '../../services/app-translation.service';
import { SettingsComponent } from './settings.component';
import { Router } from '@angular/router';

@NgModule({
    declarations: [ SettingsComponent ],
    imports: [ CommonModule, SharedModule ],
    exports: [ SettingsComponent, Router ],
    providers: [ AccountService, AppTranslationService ],
})
export class SettingsModule {}