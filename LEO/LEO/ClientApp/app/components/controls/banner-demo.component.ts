﻿// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
//
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================

import { Component } from "@angular/core";
import { AuthService } from "../../services/auth.service";

@Component({
    selector: "banner-demo",
    templateUrl: "./banner-demo.component.html",
})
export class BannerDemoComponent {
   data: string;

   constructor( private authService: AuthService) {

   }
   ngOnInit() {
     const currentUser = this.authService.currentUser;
     // tslint:disable-next-line:no-console
     console.log(currentUser);
     if (currentUser.expDate) {
        this.data = currentUser.expDate.slice(0, 10);
     }
   }
}
