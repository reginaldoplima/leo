import { ActivatedRoute, Router,  } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
// tslint:disable:no-console
@Component({
    selector: "show-detalhes",
    template: `
       <ocorrencia-detalhes
        [paginaid]="paginaid"
        [livroid]="livroid"
        [iddoc]="iddoc"
        (backButton)="onBackHandler()">
      </ocorrencia-detalhes>`,
    styles: [``],
})
export class ShowDetalhesComponent implements OnInit {
    constructor(private activeRouter: ActivatedRoute,
                private router: Router,
                private location: Location,
    ) { }

    paginaid: string;
    livroid: string;
    iddoc: string;

    ngOnInit() {

        this.paginaid = this.activeRouter.snapshot.params.paginaid;
        this.livroid = this.activeRouter.snapshot.params.livroid;
        this.iddoc = this.activeRouter.snapshot.params.ocorrenciaid;
        console.log("");
    }

    onBackHandler() {
        this.location.back();
    }
}
