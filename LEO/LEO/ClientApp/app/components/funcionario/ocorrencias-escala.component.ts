import { FuncionarioService } from "./../../services/funcionario/funcionario.service";
import { Component, OnInit } from "@angular/core";
import { fadeInOut } from "../../services/animations";
import { ActivatedRoute, Router } from "@angular/router";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { AlertService, MessageSeverity, DialogType } from "../../services/alert.service";
import { Utilities } from "../../services/utilities";
import { FechamentoDeTurno } from "../../models/livro/fechamento";

// tslint:disable:no-console
@Component({
    selector: "ocorrencias-escala",
    templateUrl: `./ocorrencias-escala.component.html`,
    styles: [``],
    animations: [fadeInOut],
})
export class OcorrenciasEscalaComponent implements OnInit {

    escalaId: string;
    setorId: string;
    loadingIndicator: boolean = false;
    local: string;
    nomeSetor: string;
    livros: any[] = [];
    livroAtivo: string;

    constructor( private activeRouter: ActivatedRoute,
                 private ocorrenciasService: OcorrenciaService,
                 private alertService: AlertService,
                 private funcionarioService: FuncionarioService,
                 private router: Router,
    ) {}

    ngOnInit() {
        this.escalaId = this.activeRouter.snapshot.params.escalaid;
        this.setorId = this.activeRouter.snapshot.params.setorid;
        this.loadData();
    }

    loadData() {
        this.ocorrenciasService
          .getLastOpenPageBySetorId(this.setorId).subscribe(
              (result) => this.onDataLoadSuccessful(result),
              (error) => this.onDataLoadFailed(error),
        );
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar Paginas em branco",
        `Unable to retrieve Paginas em branco from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }

    onDataLoadSuccessful(result: any) {
        this.alertService.stopLoadingMessage();
        if (result) {
            this.nomeSetor = result[0].setor;
            this.local = result[0].local;
            this.livros = result;
        }
        // console.log(result);
    }

    changeTable(index) {
       this.livroAtivo = this.livros[index].id;
       // console.log(this.livros[index]);
    }

    fechar() {
        this.alertService.showDialog("Deseja Fechar esse turno?" ,
           DialogType.prompt, (msg) => this.fecharTurnoHelper(msg) );
    }

    fecharTurnoHelper(observacoes: string) {
        const fechamento = new FechamentoDeTurno();
        fechamento.paginas = [];
        fechamento.observacoes = observacoes;
        fechamento.dataFechamento = new Date();
        this.livros.forEach( (t) => {
            fechamento.paginas.push(t.paginas[0].id);
        });
        this.funcionarioService
           .fecharEscala(this.escalaId, fechamento).subscribe(
                (succes) => this.onFechamentoSucess(succes),
                (error) => this.onDataLoadFailed(error),
           );
    }

    onFechamentoSucess(result: any) {
        this.router.navigate(["/funcionario/setores"]);
    }
}
