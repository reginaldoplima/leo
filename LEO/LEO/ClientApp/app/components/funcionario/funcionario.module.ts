import { DynamicFormModule } from "./../_shared/dynamic-form/dynamic-form.module";
import { ReactiveFormsModule } from "@angular/forms";
import { PtBrDatePipe } from "./../../pipes/date-ptBr.pipe";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FuncionarioEscalaComponent } from "./funcionario-escala.component";
import { SharedModule } from "../_shared/shared.module";
import { FuncionarioService } from "../../services/funcionario/funcionario.service";
import { RouterModule } from "@angular/router";
import { ModalDirective, ModalModule } from "ngx-bootstrap/modal";
import { DateFormatModule } from "../../pipes/date-pipes.module";
import { AbrirEscalaComponent } from "./abrir-escala.component";
import { LocalService } from "../../services/local/local-service";
import { OcorrenciasEscalaComponent } from "./ocorrencias-escala.component";
import { LivroRoutingModule } from "../livro/livro.module";
import { PaginaEscalaComponent } from "./pagina-escala.component";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { AlertService } from "../../services/alert.service";
import { ShowDetalhesComponent } from "./show-detalhes.component";

@NgModule({
    declarations: [
        FuncionarioEscalaComponent, AbrirEscalaComponent,
        OcorrenciasEscalaComponent, PaginaEscalaComponent, ShowDetalhesComponent ],
    imports: [
        RouterModule.forChild([
            { path: "funcionario/setores", component: FuncionarioEscalaComponent },
            { path: "funcionario/setores/:setorid/escala/:escalaid/abrir",
                 component: AbrirEscalaComponent },
            { path: "funcionario/setores/:setorid/escala/:escalaid/fechar",
                  component: OcorrenciasEscalaComponent ,
            },
            { path: "funcionario/setores/:setorid/escala/:escalaid/livro/:livroid/:paginaid/:ocorrenciaid",
               component: ShowDetalhesComponent },
        ]),
        CommonModule,
        SharedModule,
        ModalModule,
        DateFormatModule,
        ReactiveFormsModule,
        LivroRoutingModule,
        DynamicFormModule,

     ],
    providers: [ Location , FuncionarioService, LocalService, OcorrenciaService, AlertService ],
})
export class FuncionarioModule {}
