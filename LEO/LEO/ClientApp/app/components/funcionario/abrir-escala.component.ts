
import { Escala } from "./../../models/escala.model";
import { Component, OnInit } from "@angular/core";
import { FuncionarioService } from "../../services/funcionario/funcionario.service";
import { AccountService } from "../../services/account/account.service";
import { User } from "../../models/user.model";
import { AlertService, MessageSeverity, DialogType } from "../../services/alert.service";
import { Utilities } from "../../services/utilities";
import { ActivatedRoute, Router } from "@angular/router";
import { LocalService } from "../../services/local/local-service";
import { AberturaDeTurno } from "../../models/livro/abertura";
import { fadeInOut } from "../../services/animations";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Livro } from "../../models/livro/livro";
// tslint:disable:no-console
@Component({
    selector: "abrir-escala",
    templateUrl: `./abrir-escala.component.html`,
    styles: [`.btn {margin-top:15px;}
    `],
    animations: [fadeInOut],
})

export class AbrirEscalaComponent implements OnInit {
    escalas: Escala [] = [];
    loadingIndicator: boolean = false;
    escalaId: string;
    setorId: string;
    abertura: AberturaDeTurno = new AberturaDeTurno();
    formulario: FormGroup;
    informacoesTurnoAnterior: any [] = [];
    livros: Livro [] = [];

    constructor(private localService: LocalService,
                private funcionarioService: FuncionarioService ,
                private alertService: AlertService,
                private route: ActivatedRoute,
                private fb: FormBuilder,
                private router: Router,
     ) { }

    ngOnInit() {
        this.escalaId = this.route.snapshot.params.escalaid;
        this.setorId = this.route.snapshot.params.setorid;
        this.formulario = this.fb.group(
            {
                observacoes: [null],
                escalaAnterior: [null],
            },
        );
        this.load();
    }

    onChangeEscala(event) {
        console.log(event);
        if ( event == null ) {
            console.log("Vazio");
            return;
        }
        this.funcionarioService.ultimasPaginasFechadasByEscalaId(event).subscribe(
            (result) => this.changeEscalaAnteriorHandler(result),
            (error) => this.onDataLoadFailed(error),
        );
    }

    changeEscalaAnteriorHandler(result: any []) {
        console.log(result);
        this.informacoesTurnoAnterior = result;
        this.livros = result;
    }

    onSubmit(form) {
        console.log(form);
        this.alertService.startLoadingMessage();
        this.alertService.showDialog("Atesta as informações contidas ?" ,
          DialogType.confirm, () => this.handlerSubmit(form) );

    }

    handlerSubmit(form) {
        this.abertura.dataAbertura = new Date();
        this.abertura.idEscalaAnterior = form.value.escalaAnterior;
        this.abertura.observacoes =  form.value.observacoes;
        this.funcionarioService.openEscala(this.escalaId, this.abertura).subscribe(
            (result) => this.openEscalaHelper(result),
            (error) => this.onOpenEscalaError(error),
        );
    }

    load() {
       this.localService.getEscalas("-1", this.setorId).subscribe(
           (result) => this.onDataLoadSuccessful(result),
           (error) => this.onDataLoadFailed(error),
       );
    }

    onDataLoadSuccessful(result: Escala[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.escalas = result.filter( (e) => e.id !== this.escalaId );
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar locais",
        `Unable to retrieve locais from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }

    openEscalaHelper(result: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        console.log(result);
        this.router.navigate([`/funcionario/setores/${this.setorId}/escala/${this.escalaId}/fechar`]);
    }

    onOpenEscalaError(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar locais",
        `Unable to retrieve locais from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
        console.log(error);
    }
}
