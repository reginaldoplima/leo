import { Component, OnInit, ViewChild, TemplateRef, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { Livro } from "../../models/livro/livro";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Utilities } from "../../services/utilities";
// tslint:disable:no-console
@Component({
    selector: "pagina-escala",
    templateUrl: "./pagina-escala.component.html",
    styles: [`.ocorrencia-table { margin-top: 5px;}
         .ocorrencia-view {margin-top: 50px;}
    `],
})

export class PaginaEscalaComponent implements OnInit {

    columns: any [] = [];
    rows: any [] = [];
    loadingIndicator: boolean = false;
    configForm: any [] = [];
    nomeLivro: string;
    ocorrenciaId: number;

    @ViewChild("editorModal")
    editorModal: ModalDirective;
    @ViewChild("uploadFilesModal")
    uploadFilesModal: ModalDirective;

    @ViewChild("fileInput") fileInput;

    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;

    @Input()
    livro: Livro;
    @Input()
    editMode: boolean = true;

    constructor(
        private activeRouter: ActivatedRoute,
        private ocorrenciaService: OcorrenciaService,
        private router: Router,
    ) {}

    ngOnInit() {
        this.createColumns();
        this.loadOcorrencias();
        this.nomeLivro = this.livro.nome;
    }

    detalhes(row) {
        console.log(row);
        console.log(this.activeRouter.routeConfig.path);
        const pathNav =
        `funcionario/setores/setorid/escala/escalaid/livro/${this.livro.id}/${this.livro.paginas[0].id}/${row.id}`;
        console.log(pathNav);
        this.router.navigate([pathNav]);
    }

    upload() {
        const fileBrowser = this.fileInput.nativeElement;
        const formData = new FormData();
        if ( fileBrowser.files && fileBrowser.files[0]) {
            for ( let i = 0; i < fileBrowser.files.length; i++  ) {
                formData.append(`files`, fileBrowser.files[i], fileBrowser.files[i].name );
            }
            console.log(formData.getAll("files"));
            this.ocorrenciaService.addArquivoEmOcorrencia(this.livro.id,
                this.livro.paginas[0].id, this.ocorrenciaId , formData).subscribe(
                    (result) => this.onUpLoadSucess(result),
                    (error) => console.warn(error),
            );
        }
    }

    onUpLoadSucess(result) {
        console.log("result file upload", result);
        this.uploadFilesModal.hide();
    }

    loadOcorrencias() {
        this.ocorrenciaService
        .getOcorrencias(this.livro.id, this.livro.paginas[0].id)
        .subscribe((result) =>
             this.onDataLoadSuccessful(result),
               (error) => this.onLoadError(error));
    }

    onDataLoadSuccessful(result) {
        this.rows = result;
    }

    createColumns() {
        this.livro
        .configView.configProperties
        .filter( (pr) => pr.propertyName !== "id" && pr.propertyName !== "paginaId" )
        .forEach(
            (pr) => {
              const col = {
                  prop: pr.propertyName,
                  name: pr.labelName,
              };
              const cfgForm = {
                type: pr.htmlInput.startsWith("text") ? "input" :  pr.htmlInput ,
                label: pr.labelName,
                name: pr.propertyName,
              };
              this.configForm.push(cfgForm);
              this.columns.push(col);
            },
        );
        this.configForm.push({
            label: "Salvar",
            name: "submit",
            type: "button",
        });
        this.livro
        .configView.configProperties.filter( (pr) => pr.propertyName === "id" ).forEach(
            (pr) => {
                const col = {
                    prop: pr.propertyName,
                    name: "Ações",
                    cellTemplate: this.acoesTemplate,
                };
                this.columns.push(col);
            },
        );
    }

    addArquivos(id) {
        console.log(id);
        this.ocorrenciaId = id;
        this.uploadFilesModal.show();
    }

    onEditorModalHidden() {
        console.log("onEditorModalHidden");
    }

    onLoadError(error) {
        console.error(error);
    }

    openEditorModal() {
        this.editorModal.show();
    }

    onSubmitHandler(value) {
        this.ocorrenciaService
        .novaOcorrencia(this.livro.id, this.livro.paginas[0].id, value)
        .subscribe(
            (result) => this.saveSucess(result, value),
            (error) => console.error(error),
        );
    }

    saveSucess(result, value) {
        console.log(result, value);
        this.rows.push(result);
        this.editorModal.hide();
    }

    formSubmitted(value) {
       const toSaveOcorrencia = Utilities
          .createOcorrenciaObject(this.livro.paginas[0].id, value);
       this.onSubmitHandler(toSaveOcorrencia);
    }

}
