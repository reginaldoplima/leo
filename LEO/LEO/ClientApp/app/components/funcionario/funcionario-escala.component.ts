import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { Escala } from "../../models/escala.model";
import { FuncionarioService } from "../../services/funcionario/funcionario.service";
import { AlertService, MessageSeverity } from "../../services/alert.service";
import { Setor } from "../../models/setor.model";
import { Utilities } from "../../services/utilities";
import { fadeInOut } from "../../services/animations";

@Component({
    selector: "funcionario-escala",
    templateUrl: `funcionario-escala.component.html`,
    styles: [`.red-icon {  color:red;}`],
    animations: [fadeInOut],
})
export class FuncionarioEscalaComponent implements OnInit {

    columns: any[] = [];
    rows: Escala[] = [];
    rowsCache: Escala[] = [];
    loadingIndicator: boolean;
    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;
    @ViewChild("horarioTemplate")
    horarioTemplate: TemplateRef<any>;
    @ViewChild("enderecoTemplate")
    enderecoTemplate: TemplateRef<any>;
    constructor(private funcionarioService: FuncionarioService,
                private alertService: AlertService,
    ) {}
    ngOnInit() {
        this.columns = [
            { name: "Endereco", width: 400, cellTemplate: this.enderecoTemplate },
            { prop: "nomeSetor" , name: "Setor", width: 200 },
            { prop: "inicio", name: "Inicio", width: 150, cellTemplate: this.horarioTemplate },
            { prop: "fim", name: "Fim", width: 150, cellTemplate: this.horarioTemplate },
            { prop: "id", name: "", width: 100, cellTemplate: this.acoesTemplate },
        ];
        this.load();

    }

    load() {
        this.loadingIndicator = true;
        this.funcionarioService.getUserEscalas().subscribe(
            (result) => this.onDataLoadSuccessful(result),
            (error) => this.onDataLoadFailed(error),
        );
    }

    onDataLoadSuccessful(result: Escala[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.rows = result;
        this.rowsCache = [...result];
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar locais",
        `Unable to retrieve locais from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }
}
