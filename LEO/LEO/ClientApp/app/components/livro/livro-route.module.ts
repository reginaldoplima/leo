import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { LivroSetorComponent } from "./livro-setor.component";
import { PaginaComponent } from "./pagina.component";

const routes: Routes = [
    { path: "setores/:setorid/livro", component: LivroSetorComponent },
    { path: "setores/:setorid/livro/:paginaid", component: PaginaComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LivroRoutingModule {}
