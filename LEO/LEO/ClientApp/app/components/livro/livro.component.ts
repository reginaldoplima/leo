
import { Component, OnInit, ViewChild, TemplateRef, Input } from "@angular/core";
import { fadeInOut } from "../../services/animations";
import { Livro } from "../../models/livro/livro";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { AlertService, MessageSeverity } from "../../services/alert.service";
import { Page } from "../../models/livro/pagina";
import { Utilities } from "../../services/utilities";
import { ActivatedRoute, Router } from "@angular/router";
import { UtcDatePipe } from "../../pipes/data-notimezone.pipe";
import { DatatableComponent } from "@swimlane/ngx-datatable";
// tslint:disable:no-console
@Component({
    selector: "livro",
    templateUrl: `./livro.component.html`,
    styles: [``],
    animations: [fadeInOut],
})
export class LivroComponent implements OnInit {

    columns: any[] = [];
    loadingIndicator: boolean = false;
    page: any = { size: 1, count: 1, pageNumber: 0, totalPages: 0 };

    @Input()
    livro: Livro;
    rows: any[] = [];
    rowsCache: any[] = [];
    pag: Page = new Page();

    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private ocorrenciaService: OcorrenciaService,
                private alertService: AlertService,
                private route: ActivatedRoute,
                private router: Router,
    ) { }
    ngOnInit() {
        this.pag = this.livro.paginas[0];
        this.createColumns();
        this.loadData();
    }

    changePage(pageInfo) {
        console.log(pageInfo.offset);
        this.page.pageNumber = pageInfo.offset;
        if ( pageInfo.offset >= this.livro.paginas.length) {
            return;
        }

        this.ocorrenciaService
         .getOcorrencias(this.livro.id, this.livro.paginas[pageInfo.offset].id)
         .subscribe((result) =>
                this.onDataLoadSuccessful(result, pageInfo.offset),
                  (error) => this.onDataLoadFailed(error));
    }

    detalhes(row) {
        console.log(row);
        const pathNav =
        `setores/setorid/livro/${this.livro.id}/${this.livro.paginas[0].id}/${row.id}`;
        console.log(pathNav);
        this.router.navigate([pathNav]);
    }

    createColumns() {
        this.livro
          .configView.configProperties.filter(
              (prop) =>  prop.propertyName !== "id" &&  prop.propertyName !== "paginaId" ).forEach(
              (pr) => {
                const col = {
                    prop: pr.propertyName,
                    name: pr.labelName,
                };
                this.columns.push(col);
              },
        );
        this.livro
        .configView.configProperties.filter( (pr) => pr.propertyName === "id" ).forEach(
            (pr) => {
                const col = {
                    prop: pr.propertyName,
                    name: "Ações",
                    cellTemplate: this.acoesTemplate,
                };
                this.columns.push(col);
            },
        );
        this.page.totalPages = this.livro.paginas.length;
        this.page.count = this.page.totalPages * this.page.size;
    }

    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.ocorrenciaService
          .getOcorrencias(this.livro.id, this.livro.paginas[0].id)
          .subscribe((result) =>
                  this.onDataLoadSuccessful(result, 0),
                    (error) => this.onDataLoadFailed(error));
    }

    onDataLoadSuccessful(result: any, indexPage: number) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.rows = result;
        this.page.size = this.rows.length + 1;
        this.page.count = this.page.totalPages * this.page.size;
        this.pag = this.livro.paginas[indexPage];
        console.log(result);
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar Escala",
        `Unable to retrieve Escala from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }
}
