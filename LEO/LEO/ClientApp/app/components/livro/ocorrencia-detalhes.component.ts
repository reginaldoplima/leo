
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { ActivatedRoute } from "@angular/router";
import { OcorrenciaView } from "../../models/livro/ocorrencia-view";
 // tslint:disable:no-console
@Component({
    selector: "ocorrencia-detalhes",
    templateUrl: `ocorrencia-detalhes.component.html`,
    styles: [``],
})
export class OcorrenciaDetalhesComponent implements OnInit {
    constructor(private activeRouter: ActivatedRoute,
                private ocorrenciaService: OcorrenciaService) { }

    @Input()
    paginaid: string;
    @Input()
    livroid: string;
    @Input()
    iddoc: string;
    @Output()
    backButton = new EventEmitter();
    imgUrls: string [] = [];
    observacao: string;

    onPressBackButton() {
        this.backButton.emit();
    }

    ngOnInit() {
        this.ocorrenciaService.getDetalhes(this.livroid, this.iddoc).subscribe(
            (result) => this.onLoadSuccess(result),
            (error) => console.error(error),
        );
    }
    onLoadSuccess(result: OcorrenciaView ) {
        this.imgUrls = result.urlArquivos;
        this.observacao = result.observacao;
    }
}
