import { FechamentoDeTurno } from "./../../models/livro/fechamento";
import { AlertService, MessageSeverity, DialogType } from "./../../services/alert.service";
import { Component, OnInit, ViewChild, TemplateRef, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { fadeInOut } from "../../services/animations";
import { BootstrapTabDirective } from "../../directives/bootstrap-tab.directive";
import { Utilities } from "../../services/utilities";
import { Page } from "../../models/livro/pagina";
import { FuncionarioService } from "../../services/funcionario/funcionario.service";

@Component({
    selector: "pagina-component",
    templateUrl: `./pagina.component.html`,
    styles: [`.side-menu {
        min-width: 75px;
    }
    .separator-hr {
        margin-top: 0;
        margin-bottom: 10px;
    }
    [hidden] {
        display: none;
    }`],
    animations: [fadeInOut],
})
export class PaginaComponent implements OnInit {

    @Input()
    viewOnly: boolean = true;
    indexSalvo: number;
    paginaid: string;
    setorId: string;
    escalaId: string;
    nomeSetor: string;
    local: string;
    editing: any = {};
    tabelas: any [] = [];
    loadingIndicator: boolean = false;
    pagina: Page;
    rows: any[] = [];
    columns: any[] = [];
    rowsDiversas: any[] = [];
    columnsDiversas: any[] = [];
    @ViewChild("tab")
    tab: BootstrapTabDirective;
    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;
    @ViewChild("inputDataEntrada")
    inputDataEntrada: TemplateRef<any>;
    @ViewChild("inputHoraEntrada")
    inputHoraEntrada: TemplateRef<any>;
    @ViewChild("inputHoraSaida")
    inputHoraSaida: TemplateRef<any>;
    @ViewChild("inputDataSaida")
    inputDataSaida: TemplateRef<any>;
    @ViewChild("inputPlaca")
    inputPlaca: TemplateRef<any>;
    @ViewChild("inputNomeMotorista")
    inputNomeMotorista: TemplateRef<any>;

    constructor(private routeService: ActivatedRoute,
                private ocorrenciasService: OcorrenciaService,
                private alertService: AlertService,
                private funcionarioService: FuncionarioService,
                private router: Router,
    ) {

    }

    onSave(result: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.rows[this.indexSalvo].enviado = true;
        this.rows = [...this.rows];
    }
    ngOnInit() {
        this.setorId = this.routeService.snapshot.params.setorid;
        this.escalaId = this.routeService.snapshot.params.escalaid;
        // tslint:disable:no-console
        console.log(this.routeService.snapshot.params);
        this.columns = [
            { prop: "entrada.data", name: "Data  de Entrada", width: 100 ,
               cellTemplate : this.inputDataEntrada },
            { prop: "entrada.hora", name: "Hora", width: 45,
                cellTemplate: this.inputHoraEntrada },
            { prop: "saida.data", name: "Data  de Saída", width: 100,
                cellTemplate: this.inputDataSaida },
            { prop: "saida.hora", name: "Hora", width: 45,
                cellTemplate: this.inputHoraSaida },
            { prop: "placa", name: "Placa", width: 100, cellTemplate: this.inputPlaca },
            { prop: "motorista.nome", name: "Motorista", width: 100, cellTemplate: this.inputNomeMotorista },
            { prop: "id" , name: "", width: 50, cellTemplate: this.acoesTemplate },
        ];

        this.columnsDiversas = [
            { prop: "data", name: "Data", width: 30 },
            { prop: "hora", name: "Hora", width: 15 },
            { prop: "ocorrido", name: "Ocorrencia", width: 125 },
            { prop: "tipo", name: "Tipo", width: 50 },
            { prop: "id" , name: "", width: 15, cellTemplate: this.acoesTemplate },
        ];
        this.loadData();
    }

    onShowTab(event) {
        console.log(event);
    }

    newAnotacao() {
        const initDate: Date =  new Date();
        const initHora: string = ("0" + initDate.getHours()).slice(-2) + ":"
           + ("0" + initDate.getMinutes()).slice(-2);

        const r: any = {
            id: "",
            paginaId: "",
            data : initDate.toISOString().substring(0, 10),
            hora : initHora,
            ocorrido: "",
            tipo: "",

        };
        this.rowsDiversas.push(r);
    }

    salvar(row: any, index: number) {

        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        const tab1  = this.tabelas.filter( (t) => t.nome === "Entrada Veiculos" );
        const livroId = tab1[0].id;
        const paginaId = tab1[0].paginas[0].id;
        this.indexSalvo = index;
        row.paginaId = paginaId;

        this.ocorrenciasService.novaOcorrencia(livroId, paginaId, row).subscribe(
            (result) => this.onSave(result),
            (error) => this.onDataLoadFailed(error),
        );
    }

    loadData() {
        this.ocorrenciasService
          .getLastOpenPageBySetorId(this.setorId).subscribe(
              (result) => this.onDataLoadSuccessful(result),
              (error) => this.onDataLoadFailed(error),
        );

        this.rows = [
        ];
        this.rowsDiversas = [
        ];
        this.editing = {};

    }

    setActiveTab(tab: string) {
        // this.activeTab = tab.split("#", 2).pop();
    }

    fechar() {
        this.alertService.showDialog("Deseja Fechar esse turno?" ,
           DialogType.confirm, () => this.fecharTurnoHelper());
    }

    fecharTurnoHelper() {
        const fechamento = new FechamentoDeTurno();
        fechamento.paginas = [];
        fechamento.dataFechamento = new Date();
        this.tabelas.forEach( (t) => {
            fechamento.paginas.push(t.paginas[0].id);
        });
        console.log(this.escalaId);
        this.funcionarioService
           .fecharEscala(this.escalaId, fechamento).subscribe(
                (succes) => this.onFechamentoSucess(succes),
                (error) => this.onDataLoadFailed(error),
           );
    }

    onFechamentoSucess(result: any) {
        this.router.navigate(["/funcionario/setores"]);
    }

    newRow() {
        const initDate: Date =  new Date();
        const initHora: string = ("0" + initDate.getHours()).slice(-2) + ":"
           + ("0" + initDate.getMinutes()).slice(-2);
        const r: any =             {
            id: "",
            paginaid: "",
            entrada : {
                data : initDate.toISOString().substring(0, 10),
                hora: initHora,
            },
            saida: {
                data : "",
                hora: "",
            },
            placa : "",
            motorista: { nome : "jose" },
            enviado : false,
        };
        this.rows.push(r);
    }
    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar Escala",
        `Unable to retrieve Escala from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }

    onDataLoadSuccessful(result: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.tabelas = result;
        this.nomeSetor = result[0].setor;
        this.local = result[0].local;
        console.log(result);
    }

    updateValue(event, cell, rowIndex) {
        console.log("inline editing rowIndex", rowIndex);
        this.editing[rowIndex + "-" + cell] = false;
        this.rows[rowIndex][cell] = event.target.value;
        this.rows = [...this.rows];
        console.log("UPDATED!", this.rows[rowIndex][cell]);
        console.log(this.editing);
    }

    updateComplexValue(event, cell1, cell2, rowIndex) {
        console.log("inline editing rowIndex", rowIndex);
        this.editing[rowIndex + "-" + cell1 + "." + cell2] = false;
        this.rows[rowIndex][cell1][cell2] = event.target.value;
        this.rows = [...this.rows];
    }

}
