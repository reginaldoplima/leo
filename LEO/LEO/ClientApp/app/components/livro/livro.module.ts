import { LivroShowDetalhesComponent } from './livro-show-detalhes.component';
import { AlertService } from "../../services/alert.service";
import { LivroComponent } from "./livro.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UtcDatePipe } from "../../pipes/data-notimezone.pipe";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../_shared/shared.module";

import { PaginaComponent } from "./pagina.component";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { CriarOcorrenciaComponent } from "./criar-ocorrencia.component";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FuncionarioService } from "../../services/funcionario/funcionario.service";
import { LivroSetorComponent } from "./livro-setor.component";
import { DateFormatModule } from "../../pipes/date-pipes.module";
import { OcorrenciaDetalhesComponent } from "./ocorrencia-detalhes.component";

const routes: Routes = [
    { path: "setores/:setorid/livro", component: LivroSetorComponent },
    { path: "setores/:setorid/livro/:livroid/:paginaid/:ocorrenciaid", component: LivroShowDetalhesComponent },
];

@NgModule({
    imports: [CommonModule, SharedModule, DateFormatModule , RouterModule.forChild(routes)],
    exports: [ PaginaComponent, CriarOcorrenciaComponent, OcorrenciaDetalhesComponent ],
    providers: [ OcorrenciaService, AlertService, FuncionarioService ],
    declarations: [ LivroComponent, PaginaComponent, CriarOcorrenciaComponent,
        LivroSetorComponent, OcorrenciaDetalhesComponent, LivroShowDetalhesComponent ],

})
export class LivroRoutingModule {}
