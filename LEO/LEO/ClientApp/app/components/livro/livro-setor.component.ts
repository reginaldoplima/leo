
import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { fadeInOut } from "../../services/animations";
import { Livro } from "../../models/livro/livro";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { AlertService, MessageSeverity } from "../../services/alert.service";
import { Page } from "../../models/livro/pagina";
import { Utilities } from "../../services/utilities";
import { ActivatedRoute } from "@angular/router";
import { UtcDatePipe } from "../../pipes/data-notimezone.pipe";
// tslint:disable:no-console
@Component({
    selector: "livro-setor",
    templateUrl: `./livro-setor.component.html`,
    styles: [``],
    animations: [fadeInOut],
})
export class LivroSetorComponent implements OnInit {

    livros: Livro[];
    livroLocal: string;
    livroSetor: string;
    setorid: string;
    livroAtivo: string;

    constructor(private ocorrenciaService: OcorrenciaService,
                private alertService: AlertService,
                private route: ActivatedRoute,
    ) { }
    ngOnInit() {
        this.setorid =  this.route.snapshot.params.setorid;
        this.loadData();
    }

    loadData() {
        this.alertService.startLoadingMessage();
        this.ocorrenciaService.getLivrosBySetorId(this.setorid).subscribe((result) =>
                this.onDataLoadSuccessful(result),
                  (error) => this.onDataLoadFailed(error));
    }

    onDataLoadSuccessful(result: Livro[]) {
        this.alertService.stopLoadingMessage();
        this.livros = result;
        this.livroLocal = result[0].local;
        this.livroSetor = result[0].setor;
    }
    changeTable(i) {
        this.livroAtivo = this.livros[i].id;
    }

    changePage(event) {
        console.log(event);
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Erro ao carregar Escala",
        `Unable to retrieve Escala from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }
}
