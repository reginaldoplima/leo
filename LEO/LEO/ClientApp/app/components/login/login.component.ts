﻿
import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { Router, NavigationStart } from "@angular/router";
import { AlertService, MessageSeverity, DialogType } from "../../services/alert.service";
import { AuthService } from "../../services/auth.service";
import { ConfigurationService } from "../../services/configuration.service";
import { Utilities } from "../../services/utilities";
import { UserLogin } from "../../models/user-login.model";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
})

export class LoginComponent implements OnInit, OnDestroy {

    userLogin = new UserLogin();
    isLoading = false;
    formResetToggle = true;
    modalClosedCallback: () => void;
    loginStatusSubscription: any;
    teste = "nada ainda";

    @Input()
    isModal = false;

    constructor(private alertService: AlertService,
                private authService: AuthService,
                private configurations: ConfigurationService,
                public router: Router,
    ) {}

    ngOnInit() {

        this.userLogin.rememberMe = this.authService.rememberMe;

        this.router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                const url = (event as NavigationStart).url;

                if (url !== url.toLowerCase()) {
                    this.router.navigateByUrl((event as NavigationStart).url.toLowerCase());
                }
            }
        });

        if (this.getShouldRedirect()) {
            this.authService.redirectLoginUser();
        } else {
            this.loginStatusSubscription = this.authService.getLoginStatusEvent().
              subscribe((isLoggedIn) => {
                if (this.getShouldRedirect()) {
                    this.authService.redirectLoginUser();
                }
            });
        }
    }

    ngOnDestroy() {
        if (this.loginStatusSubscription) {
            this.loginStatusSubscription.unsubscribe();
        }
    }

    getShouldRedirect() {
        return !this.isModal && this.authService.isLoggedIn && !this.authService.isSessionExpired;
    }

    showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

    closeModal() {
        if (this.modalClosedCallback) {
            this.modalClosedCallback();
        }
    }

    login() {
        this.isLoading = true;
        this.alertService.startLoadingMessage("", "Tentando logar...");

        this.authService.login(this.userLogin.email, this.userLogin.password,
              this.userLogin.rememberMe)
            .subscribe(
              (user) => {
                setTimeout(() => {
                    this.alertService.stopLoadingMessage();
                    this.isLoading = false;
                    this.reset();

                    if (!this.isModal) {
                        this.alertService.showMessage("Login", `Bem Vindo ${user.userName}!`,
                           MessageSeverity.success);
                    } else {
                        this.alertService.showMessage("Login",
                           `Sessão para ${user.userName} reparada!`, MessageSeverity.success);
                        setTimeout(() => {
                            this.alertService.showStickyMessage("Sessão reparada",
                               "Por favor tente novamente", MessageSeverity.default);
                        }, 500);

                        this.closeModal();
                    }
                }, 500);
            },
              (error) => {

                this.alertService.stopLoadingMessage();

                if (Utilities.checkNoNetwork(error)) {
                    this.alertService.showStickyMessage(Utilities.noNetworkMessageCaption,
                        Utilities.noNetworkMessageDetail, MessageSeverity.error, error);
                    this.offerAlternateHost();
                } else {
                    const errorMessage = Utilities.findHttpResponseMessage("error_description", error);
                    // console.log(error)
                    if (errorMessage) {
                        this.alertService.showStickyMessage("Unable to login",
                        errorMessage, MessageSeverity.error, error);
                    } else {
                        this.alertService.showStickyMessage("Unable to login",
                         "An error occured whilst logging in, please try again later.\nError: "
                          + error.statusText || error.status, MessageSeverity.error, error);
                    }
                }

                setTimeout(() => {
                    this.isLoading = false;
                }, 500);
            });
    }

    offerAlternateHost() {

        if (Utilities.checkIsLocalHost(location.origin) &&
        Utilities.checkIsLocalHost(this.configurations.baseUrl)) {
            this.alertService.showDialog("It appears your backend Web API service is not running...\n" +
                "Would you want to temporarily switch to the online Demo API below?(Or specify another)",
                DialogType.prompt,
                (value: string) => {
                    this.configurations.baseUrl = value;
                    this.alertService.showStickyMessage("API Changed!",
                     "The target Web API has been changed to: " + value, MessageSeverity.warn);
                },
                null,
                null,
                null,
                this.configurations.fallbackBaseUrl);
        }
    }

    reset() {
        this.formResetToggle = false;

        setTimeout(() => {
            this.formResetToggle = true;
        });
    }
}
