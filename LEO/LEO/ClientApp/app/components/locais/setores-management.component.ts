
import { Endereco } from "./../../models/endereco-model";
import { Setor } from "./../../models/setor.model";
import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { fadeInOut } from "../../services/animations";
import { LocalService } from "../../services/local/local-service";
import { AlertService, MessageSeverity } from "../../services/alert.service";
import { Utilities } from "../../services/utilities";
import { ModalDirective } from "ngx-bootstrap/modal";
import { ModelUtil } from "../../models/model-utils";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "setores-management",
    templateUrl: "./setores-management.component.html",
    styles: [""],
    animations: [fadeInOut],
})

export class SetorComponent implements OnInit {

    @ViewChild("editorModal")
    editorModal: ModalDirective;
    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;
    columns: any[] = [];
    rows: Setor[] = [];
    rowsCache: Setor[] = [];
    loadingIndicator: boolean;
    canViewSetor = true;
    endereco: string;
    localId: string;
    newSetorObj: Setor = new Setor("");

    ngOnInit(): void {
        this.columns = [
            { prop: "nome", name: "Nome", width: 100},
            { prop: "id", name: "", width: 100, cellTemplate: this.acoesTemplate },
        ];
        this.localId =  this.route.snapshot.params.id;
        this.endereco = decodeURI(this.route.snapshot.params.endereco);
        this.loadData();
    }

    constructor(private localService: LocalService,
                private route: ActivatedRoute,
                private alertService: AlertService) {
    }

    newSetor() {
        this.editorModal.show();
    }

    // onSearchChanged(value: string) {
    // }

    onSubmit(form) {
        // tslint:disable-next-line:no-console
        console.log(form);
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;

        if (!form.valid) {
            return;
        }
        this._novoSetorHelper(this.newSetorObj.nome);
    }

    private _novoSetorHelper(nome: string) {
        this.localService.novoSetor(this.localId, nome).subscribe(
            (setor) => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.editorModal.hide();
                const retVal = new Setor(this.localId);
                Object.assign(retVal, setor);
                this.rowsCache.push(retVal);
                this.rows.push(retVal);
            },
            (error) => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.alertService.showStickyMessage("Erro ao criar novo setor",
                `Unable to retrieve locais from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
                   MessageSeverity.error, error);
            },
        );
    }

    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.localService.getSetores(this.localId).subscribe((result) =>
                this.onDataLoadSuccessful(result),
                  (error) => this.onDataLoadFailed(error));

    }

    getEnderecoUrlEncode(endereco: string) {
       return encodeURI(endereco);
    }

    onDataLoadSuccessful(result: Setor[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.rows = result;
        this.rowsCache = [...result];
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar setor",
        `Unable to retrieve setor from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }

}
