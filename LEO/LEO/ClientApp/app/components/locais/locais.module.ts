import { DateFormatModule } from "./../../pipes/date-pipes.module";

import { NgModule } from "@angular/core";
import { RouterModule, ActivatedRoute } from "@angular/router";
import { LocaisComponent } from "./locais-management.component";
import { LocalService } from "../../services/local/local-service";
import { AlertService, MessageSeverity } from "../../services/alert.service";
import { CepService } from "../../services/cep.service";
import { Utilities } from "../../services/utilities";
import { ModalDirective, ModalModule } from "ngx-bootstrap/modal";
import { AuthGuard } from "../../services/auth-guard.service";
import { AuthService } from "../../services/auth.service";
import { SearchBoxComponent } from "../controls/search-box.component";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { TextMaskModule } from "angular2-text-mask";
import { SharedModule } from "../_shared/shared.module";
import { SetorComponent } from "./setores-management.component";
import { AccountService } from "../../services/account/account.service";
import { EscalaComponent } from "./escalas-management.component";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: "locais", component: LocaisComponent, canActivate: [AuthGuard],
         data: { title: "Locais" } },
      { path: "locais/:id/:endereco", component: SetorComponent },
      { path: "locais/:id/:endereco/:nome/:setorid", component: EscalaComponent },
    ]),
    SharedModule,
    ModalModule,
    ReactiveFormsModule,
    DateFormatModule,
  ],
  declarations: [
    LocaisComponent,
    SetorComponent,
    EscalaComponent,

  ],
  providers: [
    LocalService,
    AlertService,
    CepService,
    AuthService, AuthGuard, AccountService,
  ],
})
export class LocaisModule {}
