
import { Endereco } from "./../../models/endereco-model";
import { Escala } from "./../../models/escala.model";
import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { fadeInOut } from "../../services/animations";
import { LocalService } from "../../services/local/local-service";
import { AlertService, MessageSeverity } from "../../services/alert.service";
import { Utilities } from "../../services/utilities";
import { ModalDirective } from "ngx-bootstrap/modal";
import { ModelUtil } from "../../models/model-utils";
import { ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { User } from "../../models/user.model";
import { AccountService } from "../../services/account/account.service";

@Component({
    selector: "escalas-management",
    templateUrl: "./escalas-management.component.html",
    styles: [""],
    animations: [fadeInOut],
})

export class EscalaComponent implements OnInit {

    @ViewChild("editorModal")
    editorModal: ModalDirective;
    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;
    @ViewChild("horarioTemplate")
    horarioTemplate: TemplateRef<any>;
    columns: any[] = [];
    rows: Escala[] = [];
    rowsCache: Escala[] = [];
    loadingIndicator: boolean;
    canViewEscala = true;
    endereco: string;
    nome: string;
    setorid: string;
    localId: string;
    newEscalaObj: Escala = new Escala();
    userSelectObj: User = new User();
    DIAS_SEMANA = ["", "Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];
    formulario: FormGroup;
    funcionarios: User[];

    ngOnInit(): void {
        this.columns = [
            { prop: "inicio", name: "Incio", width: 75, cellTemplate : this.horarioTemplate },
            { prop: "fim", name: "Fim", width: 75, cellTemplate : this.horarioTemplate  },
            { prop: "applicationUser.nome", name: "Funcionario", width: 175 },
            { prop: "id", name: "", width: 100, cellTemplate: this.acoesTemplate },
        ];
        this.formulario = this.fb.group(
            {
                diaInicio: [this.DIAS_SEMANA[0], Validators.required],
                diaFim: [this.DIAS_SEMANA[0], Validators.required],
                horaInicio: [null, Validators.required],
                horaFim: [null, Validators.required],
                funcionario: [new User(), Validators.required],
            },
        );
        this.setorid =  this.route.snapshot.params.setorid;
        this.localId = this.route.snapshot.params.id;
        this.nome = this.route.snapshot.params.nome;
        this.endereco = decodeURI(this.route.snapshot.params.endereco);
        this.loadData();
        this.loadUsers();

        this.formulario.controls["funcionario"].setValue("", {onlySelf: true});
    }

    constructor(private localService: LocalService,
                private route: ActivatedRoute,
                private fb: FormBuilder,
                private accountService: AccountService,
                private alertService: AlertService) {
    }

    novaEscala() {
        this.editorModal.show();
    }

    // onSearchChanged(value: string) {
    // }

    onSubmit(form) {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;

        if (!form.valid) {
            return;
        }
        let inicio = form.value.horaInicio.split(":");
        let fim = form.value.horaFim.split(":");
        let diaInicio = this.DIAS_SEMANA.findIndex((d) => d === form.value.diaInicio);
        let diaFim = this.DIAS_SEMANA.findIndex((d) => d === form.value.diaFim);
        let dtInicial = this._transformToDate(diaInicio, +inicio[0], +inicio[1]);
        let dtFinal = this._transformToDate(diaFim, +fim[0], +fim[1]);
        this.newEscalaObj = new Escala();
        this.newEscalaObj.id = null;
        this.newEscalaObj.inicio = dtInicial;
        this.newEscalaObj.fim = dtFinal;
        this.newEscalaObj.applicationUserId = form.value.funcionario;
        console.log(this.newEscalaObj);
        this._novoEscalaHelper(this.newEscalaObj);

    }

    private _novoEscalaHelper(escala: Escala) {
        this.localService.novaEscala(escala, this.localId, this.setorid).subscribe(
            (result) => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.editorModal.hide();
                const retVal = new Escala();
                Object.assign(retVal, result);
                retVal.inicio = new Date(retVal.inicio);
                retVal.fim = new Date(retVal.fim);
                retVal.inicio = new Date(retVal.inicio.getTime() + retVal.inicio.getTimezoneOffset() * 60000);
                retVal.fim = new Date(retVal.fim.getTime() + retVal.fim.getTimezoneOffset() * 60000);
                this.rowsCache.push(retVal);
                this.rows.push(retVal);
            },
            (error) => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.alertService.showStickyMessage("Erro ao criar novo Escala",
                `Unable to retrieve locais from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
                   MessageSeverity.error, error);
            },
        );
    }

    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.localService.getEscalas(this.localId, this.setorid).subscribe((result) =>
                this.onDataLoadSuccessful(result),
                  (error) => this.onDataLoadFailed(error));
    }

    loadUsers() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.funcionarios = [];
        this.funcionarios.push(this.userSelectObj);
        this.accountService.getUsers().subscribe(
            (result) =>
                this.onDataLoadSuccessfulUsers(result),
            (error) => this.onDataLoadFailed(error),
        );
    }

    onDataLoadSuccessfulUsers(result: User[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.funcionarios.push(...result);
    }

    onDataLoadSuccessful(result: Escala[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.rows = result;
        this.rowsCache = [...result];
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar Escala",
        `Unable to retrieve Escala from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }

    _transformToDate(intDiaSemana: number, numeroHoras: number, numeroMinutos: number) {
        const MULTIPLICADOR = 1000;
        const UNIX_PRIMEIRO_DOMINGO = 259200 * MULTIPLICADOR;
        const diaEmUnixTime = 86400 * MULTIPLICADOR;
        const horasEmUnixTime = 3600 * MULTIPLICADOR;
        const minutosEmUnixTime = 60 * MULTIPLICADOR;
        let totalMilesegundos =
           UNIX_PRIMEIRO_DOMINGO + diaEmUnixTime * (intDiaSemana - 1)
           + horasEmUnixTime * numeroHoras + minutosEmUnixTime * numeroMinutos;

           return new Date(totalMilesegundos);
    }

}
