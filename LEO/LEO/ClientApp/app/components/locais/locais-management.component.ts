import { CepService } from "./../../services/cep.service";
import { Endereco } from "./../../models/endereco-model";
import { Local } from "./../../models/local.model";
import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { fadeInOut } from "../../services/animations";
import { LocalService } from "../../services/local/local-service";
import { AlertService, MessageSeverity } from "../../services/alert.service";
import { Utilities } from "../../services/utilities";
import { ModalDirective } from "ngx-bootstrap/modal";
import { ModelUtil } from "../../models/model-utils";

@Component({
    selector: "locais-management",
    templateUrl: "./locais-management.component.html",
    styleUrls: ["./locais-management.component.css"],
    animations: [fadeInOut],
})

export class LocaisComponent implements OnInit {

    @ViewChild("editorModal")
    editorModal: ModalDirective;

    @ViewChild("enderecoTemplate")
    enderecoTemplate: TemplateRef<any>;
    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;
    columns: any[] = [];
    rows: Local[] = [];
    rowsCache: Local[] = [];
    loadingIndicator: boolean;
    canViewLocais = true;
    newLocalObj: Local = new Local();
    maskCep = ModelUtil.CEP_MASK;

    ngOnInit(): void {
        this.newLocalObj.endereco = new Endereco();
        this.newLocalObj.descricao = "";
        this.columns = [
            { prop: "endereco", name: "Endereco", width: 400, cellTemplate: this.enderecoTemplate },
            { prop: "descricao", name: "Descricao", width: 150 },
            { prop: "id", name: "", width: 100, cellTemplate: this.acoesTemplate },
        ];
        this.loadData();
    }

    constructor(private localService: LocalService, private alertService: AlertService,
                private cepService: CepService) {
    }

    searchCep(cep) {

        if (cep.valid) {
            this.alertService.showMessage("Procurando Endereço", "", MessageSeverity.info);
            this.cepService.GetEndereco(cep.value).subscribe(
                (endereco) => {
                    // console.log(endereco);
                    this.newLocalObj.endereco.bairro = endereco.bairro;
                    this.newLocalObj.endereco.logradouro = endereco.logradouro;
                    this.newLocalObj.endereco.localidade = endereco.localidade;
                    this.newLocalObj.endereco.uf = endereco.uf;
                    this.newLocalObj.endereco.complemento = endereco.complemento;
                },
                (error) => {
                    // console.log(error);
                },
            );
        }
    }

    newLocal() {
        // this.alertService.startLoadingMessage();
        this.editorModal.show();
    }

    editar(row: Local) {
        this.newLocalObj = row;
        this.editorModal.show();
    }

    getEndereco(row: Endereco) {
        if (row !== null ) {
            const enderecoUrl = encodeURI(`${row.logradouro}, ${row.numero} - ${row.bairro}`);
            return enderecoUrl;
        } else {
            return "";
        }
    }

    onSearchChanged(value: string) {
        // console.log(this.rowsCache);
        this.rows = this.rowsCache.filter((r) =>
            Utilities.searchArray(value, false, r.endereco.localidade,
                r.endereco.bairro, r.endereco.logradouro));
    }

    onSubmit(form) {
        // tslint:disable-next-line:no-console
        console.log(form);
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;

        if (!form.valid) {
            return;
        }

        this.localService.newLocal(this.newLocalObj).subscribe(
            (local) => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.editorModal.hide();

                // tslint:disable-next-line:no-console
                // console.log(local);
                const retVal = new Local();
                Object.assign(retVal, local);
                this.rowsCache.push(retVal);
                this.rows.push(retVal);
            },
            (error) => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.alertService.showStickyMessage("Erro ao carregar locais",
                `Unable to retrieve locais from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
                   MessageSeverity.error, error);
            },
        );
    }

    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;

        if (this.canViewLocais) {
            this.localService.getLocais().subscribe((result) =>
                this.onDataLoadSuccessful(result),
                  (error) => this.onDataLoadFailed(error));
        } else {
            this.localService.getLocais().subscribe((users) =>
                this.onDataLoadSuccessful(users), (error) => this.onDataLoadFailed(error));
        }
    }

    onDataLoadSuccessful(result: Local[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.rows = result;
        this.rowsCache = [...result];
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar locais",
        `Unable to retrieve locais from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }

}
