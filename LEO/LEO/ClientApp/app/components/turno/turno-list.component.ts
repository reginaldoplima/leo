import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { fadeInOut } from "../../services/animations";
import { Turno } from "../../models/turno-model";
import { ActivatedRoute, Router } from "@angular/router";
import { ModalDirective } from "ngx-bootstrap/modal";
import { TurnoService } from "../../services/turno/turno-service";
import { Funcionario } from "../../models/funcionario";
import { BootstrapSelectDirective } from "../../directives/bootstrap-select.directive";
import { AlertService, DialogType, MessageSeverity } from "../../services/alert.service";
import { AberturaDeTurno } from "../../models/livro/abertura";
import { Pagina } from "../../models/livro/pagina-model";

import { isToday, distanceInWordsToNow } from "date-fns";

@Component({
    selector: "turnos-list",
    templateUrl: "./turno-list.component.html",
    styles: [``],
    animations: [fadeInOut],
  })
export class TurnoListComponent implements OnInit {

    constructor(private route: ActivatedRoute,
                private turnoService: TurnoService,
                private alertService: AlertService,
                private routeNav: Router,
    ) { }
    columns: any[] = [];
    rows: Turno[] = [];
    rowsCache: Turno[] = [];
    funcionarios: Funcionario[];
    funciorario: Funcionario;
    turno: Turno;
    today: Date;
    atraso: string;

    @ViewChild("selectDirective")
    selectDirective: BootstrapSelectDirective;

    @ViewChild("horarioTemplate")
    horarioTemplate: TemplateRef<any>;
    @ViewChild("situacaoTemplate")
    situacaoTemplate: TemplateRef<any>;
    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;
    @ViewChild("abrirTurnoModal")
    abrirTurnoModal: ModalDirective;

    ngOnInit(): void {
        this.today = new Date();
        this.atraso = distanceInWordsToNow(this.today);
        this.abrirTurnoModal.onShown.subscribe((s) => {
            setTimeout(() => {
                this.selectDirective.refresh();
            });
        });
        this.funcionarios = [];
        // tslint:disable-next-line:no-string-literal
        this.rows = this.route.snapshot.data["turnos"].sort( (a, b) => this.sorteByDate(a, b) );
        this.columns = [
            { prop: "inicio", name: "Inicio", width: 150, cellTemplate: this.horarioTemplate },
            { prop: "fim", name: "Fim", width: 150, cellTemplate: this.horarioTemplate },
            { prop: "pagina", name: "Situação", width: 150, cellTemplate: this.situacaoTemplate },
            { prop: "pagina", name: "", width: 300, cellTemplate: this.acoesTemplate },
        ];

    }

    private sorteByDate(d1: Turno, d2: Turno): number {
        const diff =  new Date(d1.inicio).getTime() - new Date(d2.inicio).getTime();
        return  diff;
    }

    openTurno(turno: Turno) {
        this.turno = turno;
        this.turnoService.getTurnos(-1 , -1, this.turno.localId ).subscribe((turnos) => {
            // tslint:disable-next-line:no-shadowed-variable
            turnos.map( (turno) => {
                this.funcionarios = this.funcionarios.concat(turno.escalados);
                this.funcionarios = this.funcionarios.filter((value, index, array) =>
                    !array.filter((v, i) => JSON.stringify(value)
                       === JSON.stringify(v) && i < index).length);
            });
            this.abrirTurnoModal.show();
        });
    }

    confirmar(fun: Funcionario) {
        this.alertService.showDialog(`Abrindo turno recebido de ${fun.nome}`,
           DialogType.confirm, () => this.AbrirTurnoHelper(this.turno, fun));
    }

    fechar(turno: Turno) {
        this.alertService.showDialog(`Voce Esta fechando seu turno`,
           DialogType.confirm, () => this.fecharTurnoHelper(turno));
    }
    onAbrirTurnoModalHidden(){ }

    fecharTurnoHelper(turno: Turno) {
       this.routeNav.navigate(["pagina", turno.id]);
    }

    AbrirTurnoHelper(turno: Turno, fun: Funcionario) {
        const eventoAbertura = new AberturaDeTurno();
        // eventoAbertura.data = this.today;
        // eventoAbertura.chaveAbertura = fun.id;
        this.turnoService.abrirTurno(turno.id, eventoAbertura).subscribe( (sturno) => {
            this.rows = this.rows.filter((t) => t.id !== turno.id );
            this.rows.push(sturno);
        });
    }

}
