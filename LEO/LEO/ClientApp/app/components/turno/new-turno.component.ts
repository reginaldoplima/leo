
import { Router, ActivatedRoute } from "@angular/router";
import { AccountService } from "./../../services/account/account.service";
import { style } from "@angular/animations";
import { Component, Input, Output, EventEmitter, ViewChild, OnInit } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { User } from "../../models/user.model";
import { Turno } from "../../models/turno-model";
import { TurnoService } from "../../services/turno/turno-service";
import { AlertService, DialogType, MessageSeverity } from "../../services/alert.service";

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  addMinutes,
} from "date-fns";
import { Time } from "ngx-bootstrap/timepicker/timepicker.models";

@Component({
  selector: "new-turno",
  templateUrl: "./new-turno.component.html",
  styles: [``],
})
// tslint:disable:no-console
export class NewTurnoComponent implements OnInit {

  constructor(private accountService: AccountService,
              private turnoService: TurnoService, route: ActivatedRoute ) {

  }

  users: User[] = [];
  turno: Turno;
  bsConfig = new BsDatepickerConfig();

  private horaInicio: Date;
  private horaFim: Date;

  ngOnInit(): void {
    this.bsConfig.locale = "pt-br";
    this.bsConfig.showWeekNumbers = false;
    this.bsConfig.dateInputFormat = "LL";
    this.accountService.getUsers().subscribe(
      (users) => {
        this.users = users;
      },

    );
    const today = new Date();
    const todayUTC = new Date( Date.UTC(today.getUTCFullYear() , today.getMonth(), today.getDay() ) );
    this.turno = new Turno(todayUTC, todayUTC , this.localId);
  }

  @Input()
  localId: string;

  @ViewChild("modalAdd")
  modalAdd: ModalDirective;

  public open() {
    this.modalAdd.show();
  }

  escalado(event, user: User) {
    if (event.target.checked) {
      this.turno.escalados.push(user);
    } else {
      this.turno.escalados = this.turno.escalados.filter( (u) => u.id !==  user.id );
    }
  }
  onModalHidden() {
    this.hideModal.emit();
  }

  @Output() saveTurno: EventEmitter<any> = new EventEmitter();
  @Output() hideModal: EventEmitter<any> = new EventEmitter();

  save() {
    this.turno.inicio = new Date(this.turno.inicio.setHours(this.horaInicio.getHours()));
    this.turno.inicio = new Date(this.turno.inicio.setMinutes(this.horaInicio.getMinutes()));
    this.turno.fim =  new Date(this.turno.fim.setHours(this.horaFim.getHours()));
    this.turno.fim = new Date(this.turno.fim.setMinutes(this.horaFim.getMinutes()));
    console.log(this.turno);
    this.turnoService.newTurno(this.turno).subscribe(
      (response) => {
        this.saveTurno.emit(response);
      },
      (error) => {
        console.error(error);
      },
    );
  }

  hide() {
    this.modalAdd.hide();
  }
}
