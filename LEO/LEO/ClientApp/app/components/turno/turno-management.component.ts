// // ======================================
// // Author: Ebenezer Monney
// // Email:  info@ebenmonney.com
// // Copyright (c) 2017 www.ebenmonney.com
// //
// // ==> Gun4Hire: contact@ebenmonney.com
// // ======================================

// import { Component, EventEmitter, ChangeDetectionStrategy, ViewChild, OnInit, OnDestroy } from "@angular/core";
// import { fadeInOut } from "../../services/animations";
// import { CalendarComponent } from "ng-fullcalendar";
// import { Options } from "fullcalendar";
// import { Subject } from "rxjs/Subject";
// import { NewTurnoComponent } from "./new-turno.component";
// import { Subscription } from "rxjs/Subscription";
// import { ActivatedRoute } from "@angular/router";
// import { TurnoService } from "../../services/turno/turno-service";
// import { LocalService } from "./../../services/local/local-service";
// import { Funcionario } from "./../../models/funcionario";
// import { Input } from "@angular/core/src/metadata/directives";
// import "rxjs/add/operator/mergeMap";
// import { Turno } from "../../models/turno-model";
// import { AlertService, DialogType, MessageSeverity } from "../../services/alert.service";

// import {
//   startOfDay,
//   endOfDay,
//   subDays,
//   addDays,
//   endOfMonth,
//   isSameDay,
//   isSameMonth,
//   addHours,
// } from "date-fns";
// import { AccountService } from "../../services/account/account.service";
// import { User } from "../../models/user.model";

// // tslint:disable:no-console
// @Component({
//   selector: "turnos-management",
//   templateUrl: "./turno-management.component.html",
//   styleUrls: ["./turno-management.component.css"],
//   animations: [fadeInOut],
//   changeDetection: ChangeDetectionStrategy.OnPush,
// })
// export class TurnosManagementComponent implements OnInit, OnDestroy {

//   localId: string;
//   local: any;
//   turnos: any;
//   displayEvent: any;
//   users: User[] = [];
//   data: any [];
//   constructor(private route: ActivatedRoute,
//               private alertService: AlertService,
//               private turnoService: TurnoService,
//               private accountService: AccountService,
//               private localService: LocalService) {
//   }

//   ngOnDestroy() {
//     console.log("destroy:turno-managenment");
//   }

//   ngOnInit(): void {
//     // tslint:disable:no-string-literal
//     const localId = this.route.snapshot.params["localId"];
//     this.localId = localId;
//     this.turnos = this.route.snapshot.data["turnos"];
//     this.local = this.route.snapshot.data["local"];
//     const yearMonth = this.bsValue.getUTCFullYear() + "-" + (this.bsValue.getUTCMonth() + 1);
//     this.data = [
//     ];
//     this._createEventByTurno(this.turnos);
//     this.accountService.getUsers().subscribe(
//       (users) => {
//         this.users = users;
//       },

//     );
//     this.calendarOptions = {
//       locale: "pt-br",
//       editable: true,
//       eventLimit: false,
//       header: {
//         left: "prev,next today",
//         center: "title",
//         right: "month,agendaWeek,agendaDay,listMonth",
//       },
//       events: this.data,
//     };
//   }

//   private _createEventByTurno(turnos: Turno[]) {
//     turnos.forEach( (t) => {
//       const event = {
//         id: t.id,
//         start: t.inicio,
//         end: t.fim,
//         title: this._escaladosNomes(t.escalados) ,
//         editable: false,
//         timezone: "UTC",
//         escalados: t.escalados,
//       };
//       this.data.push(event);
//     });
//   }

//   // TODO move to Turno model;
//   private _escaladosNomes(escalados: Funcionario[]) {
//     let stringNomes = "";
//     escalados.forEach( (f) => {
//       console.log(f);
//       stringNomes = stringNomes.concat(f.nome + "-");
//     });

//     return stringNomes.slice(0 , -1);
//   }

//   clickButton(model: any) {
//     this.displayEvent = model;
//   }

//   eventClick(model: any) {
//     model = {
//       event: {
//         id: model.event.id,
//         start: model.event.start,
//         end: model.event.end,
//         title: model.event.title,
//         allDay: model.event.allDay,
//         escalados: model.event.escalados,
//         // other params
//       },
//       duration: {},
//     },
//     this.displayEvent = model.event;
//     console.log(model);
//     this.removerTurnoConfirm(model.event);
//   }

//   updateEvent(model: any) {
//     model = {
//       event: {
//         id: model.event.id,
//         start: model.event.start,
//         end: model.event.end,
//         title: model.event.title,
//         // other params
//       },
//       duration: {
//         _data: model.duration._data,
//       },
//     },
//     this.displayEvent = model;
//   }

//   calendarOptions: Options;
//   @ViewChild(CalendarComponent)
//   ucCalendar: CalendarComponent;

//   bsValue: Date = new Date();

//   @ViewChild("addTurno")
//   addTurno: NewTurnoComponent;

//   newTurno() {
//     this.addTurno.open();
//   }

//   onHideModal(event) {
//     this.ucCalendar.fullCalendar("rerenderEvents");
//   }

//   onSaveTurno(turno) {
//     const event = this.turnoToEvent(turno);
//     this.ucCalendar.fullCalendar("renderEvent", event);
//   }

//   private turnoToEvent(turno: Turno) {
//     const event = {
//       id: turno.id,
//       start: turno.inicio,
//       end: turno.fim,
//       title: this._escaladosNomes(turno.escalados) ,
//       editable: false,
//       timezone: "UTC",
//       escalados: turno.escalados,
//     };
//     return event;
//   }

//   removerTurnoConfirm(turnoEvent: any) {
//      this.alertService.showDialog(`Deseja remover esse turno ?`,
//        DialogType.confirm, () => this.removerTurnoHelper(turnoEvent));
//   }

//   removerTurnoHelper(turnoEvent: any) {
//     console.log(turnoEvent);
//     this.turnoService.deleteTurno(turnoEvent.id).subscribe(
//       (succes) => {
//         console.log(succes);
//         if (succes) {
//           this.ucCalendar.fullCalendar("removeEvents", [turnoEvent.id]);
//         }
//       },
//       (error) => {
//         console.log(error);
//       },
//     );
//   }

// }
