import { NgModule }  from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { SearchBoxComponent } from "../controls/search-box.component";
import { TextMaskModule } from "angular2-text-mask";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { UtcDatePipe } from "../../pipes/data-notimezone.pipe";
import { BootstrapTabDirective } from "../../directives/bootstrap-tab.directive";

@NgModule({
  imports: [ CommonModule ],
  exports : [
    CommonModule,
    FormsModule,
    SearchBoxComponent,
    TextMaskModule,
    NgxDatatableModule,
    UtcDatePipe,
    BootstrapTabDirective,
  ],
  declarations: [ SearchBoxComponent, UtcDatePipe, BootstrapTabDirective ],
})
export class SharedModule { }
