import { Component } from "@angular/core";

@Component({
    selector: "input-mask",
    template: `
      <input [textMask]="{mask: mask}" [(ngModel)]="myModel" type="text"/>
    `,
  })
  export class InputMaskComponent {
    public myModel = "";
    public mask = ["(", /[1-9]/, /\d/, /\d/, ")", " ", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/];
  }
