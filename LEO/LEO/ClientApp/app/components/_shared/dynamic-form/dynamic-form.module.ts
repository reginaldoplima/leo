import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { DynamicFieldDirective } from "./components/dynamic-field/dynamic-field.directive";
import { DynamicFormComponent } from "./containers/dynamic-form/dynamic-form.component";
import { FormButtonComponent } from "./components/form-button/form-button.component";
import { FormInputComponent } from "./components/form-input/form-input.component";
import { FormSelectComponent } from "./components/form-select/form-select.component";
import { FormInputTimeComponent } from "./components/form-input-time/form-input-time.component";
import { FormInputDateComponent } from "./components/form-input-date/form-input-date.component";
import { TextMaskModule } from "angular2-text-mask";
import { FormCheckBoxComponent } from "./components/form-checkbox/form-checkbox.component";

@NgModule({
  imports: [
    CommonModule,
    TextMaskModule,
    ReactiveFormsModule,
  ],
  declarations: [
    DynamicFieldDirective,
    DynamicFormComponent,
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent,
    FormInputDateComponent,
    FormInputTimeComponent,
    FormCheckBoxComponent,
  ],
  exports: [
    DynamicFormComponent,
  ],
  entryComponents: [
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent,
    FormInputDateComponent,
    FormInputTimeComponent,
    FormCheckBoxComponent,
  ],
})
export class DynamicFormModule {}
