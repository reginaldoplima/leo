import { Component, ViewContainerRef } from "@angular/core";
import { FormGroup } from "@angular/forms";

import { Field } from "../../models/field.interface";
import { FieldConfig } from "../../models/field-config.interface";

@Component({
  selector: "form-input-date",
  styleUrls: ["form-input-date.component.scss"],
  template: `
    <div 
      class="dynamic-field form-input-date col-md-6"
      [formGroup]="group">
      <label>{{ config.label }}</label>
      <input
        type="date"
        [attr.placeholder]="config.placeholder"
        [formControlName]="config.name">
    </div>
  `,
})
export class FormInputDateComponent implements Field {
  config: FieldConfig;
  group: FormGroup;
}
