import { Component, ViewContainerRef } from "@angular/core";
import { FormGroup } from "@angular/forms";

import { Field } from "../../models/field.interface";
import { FieldConfig } from "../../models/field-config.interface";

@Component({
  selector: "form-input-time",
  styleUrls: ["form-input-time.component.scss"],
  template: `
    <div 
      class="dynamic-field form-input-time col-md-6"
      [formGroup]="group">
      <label>{{ config.label }}</label>
      <input
        type="time"
        [attr.placeholder]="config.placeholder"
        [formControlName]="config.name">
    </div>
  `,
})
export class FormInputTimeComponent implements Field {
  config: FieldConfig;
  group: FormGroup;
}
