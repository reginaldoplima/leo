import { Component, ViewContainerRef } from "@angular/core";
import { FormGroup } from "@angular/forms";

import { Field } from "../../models/field.interface";
import { FieldConfig } from "../../models/field-config.interface";

@Component({
  selector: "form-checkbox",
  styleUrls: ["form-checkbox.component.scss"],
  template: `
    <div
      class="dynamic-field form-input-date col-md-12"
      [formGroup]="group">
      <label>{{ config.label }}</label>
      <input
        type="checkbox"
        [attr.placeholder]="config.placeholder"
        [formControlName]="config.name">
    </div>
  `,
})
export class FormCheckBoxComponent implements Field {
  config: FieldConfig;
  group: FormGroup;
}
