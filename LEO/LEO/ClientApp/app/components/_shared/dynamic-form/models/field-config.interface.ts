import { ValidatorFn } from "@angular/forms";

// tslint:disable:interface-name
export interface FieldConfig {
  disabled?: boolean;
  label?: string;
  name: string;
  options?: string[];
  placeholder?: string;
  type: string;
  validation?: ValidatorFn[];
  value?: any;
}
