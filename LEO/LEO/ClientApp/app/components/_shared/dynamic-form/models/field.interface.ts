import { FormGroup } from "@angular/forms";
import { FieldConfig } from "./field-config.interface";

// tslint:disable-next-line:interface-name
export interface Field {
  config: FieldConfig;
  group: FormGroup;
}
