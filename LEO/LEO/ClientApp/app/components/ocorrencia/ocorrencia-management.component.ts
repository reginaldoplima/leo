// tslint:disable:no-console
import { CepService } from "./../../services/cep.service";
import { Endereco } from "./../../models/endereco-model";
import { Ocorrencia } from "./../../models/livro/ocorrencia";
import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { fadeInOut } from "../../services/animations";
import { OcorrenciaService } from "../../services/livro/ocorrencia-service";
import { AlertService, MessageSeverity } from "../../services/alert.service";
import { Utilities } from "../../services/utilities";
import { ModalDirective } from "ngx-bootstrap/modal";
import { ModelUtil } from "../../models/model-utils";
import { ActivatedRoute } from "@angular/router";
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { BsDatepickerComponent } from "ngx-bootstrap/datepicker/bs-datepicker.component";

import {  isToday, distanceInWordsToNow } from "date-fns";

@Component({
    selector: "ocorrencia-management",
    templateUrl: "./ocorrencia-management.component.html",
    styleUrls: ["./ocorrencia-management.component.css"],
    animations: [fadeInOut],
})

export class OcorrenciaComponent implements OnInit {

    @ViewChild("editorModal")
    editorModal: ModalDirective;

    @ViewChild("horarioTemplate")
    horarioTemplate: TemplateRef<any>;
    @ViewChild("acoesTemplate")
    acoesTemplate: TemplateRef<any>;
    columns: any[] = [];
    rows: Ocorrencia[] = [];
    rowsCache: Ocorrencia[] = [];
    loadingIndicator: boolean;
    canViewocorrencia = true;
    newOcorrenciaObj: Ocorrencia = new Ocorrencia();
    turnoId: string;
    bsConfig = new BsDatepickerConfig();
    dp: BsDatepickerComponent;
    dataOcorrido: Date;
    horaOcorrido: Date;

    ngOnInit(): void {
        this.newOcorrenciaObj.descricao = "";
        this.newOcorrenciaObj.ocorrido = "";
        this.bsConfig.locale = "pt-br";
        this.bsConfig.showWeekNumbers = false;
        this.bsConfig.dateInputFormat = "LL";
        this.turnoId = this.route.snapshot.params["turnoId"];
        this.columns = [
            { prop: "nome", name: "Nome", width: 75 },
            { prop: "data", name: "Data", width: 80, cellTemplate: this.horarioTemplate },
            { prop: "descricao", name: "Descricao", width: 150 },
            { prop: "anexos", name: "Anexos", width: 150 },
            { prop: "nome", name: "Ações", width: 300 , cellTemplate: this.acoesTemplate }
        ];
        this.loadData();
    }

    constructor(private ocorrenciaService: OcorrenciaService,
                private alertService: AlertService, private route: ActivatedRoute) {}
    newOcorrenciaForm() {
        this.editorModal.show();
    }

    onSubmit(form) {

        console.log(form);
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;

        if (!form.valid) {
            return;
        }
        this.submitHelper();

    }

    submitHelper() {
        const data = Date.UTC(this.dataOcorrido.getFullYear(),
            this.dataOcorrido.getMonth(), this.dataOcorrido.getDay(),
              this.horaOcorrido.getHours(), this.horaOcorrido.getMinutes());
        this.newOcorrenciaObj.data = new Date(data);
        this.ocorrenciaService.newOcorrencia(this.newOcorrenciaObj, this.turnoId).subscribe(
            (result) => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.editorModal.hide();
                // console.log(Ocorrencia);
                const retVal = new Ocorrencia();
                Object.assign(retVal, result);
                this.rowsCache.push(retVal);
                this.rows.push(retVal);
            },
            (error) => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.alertService.showStickyMessage("Erro ao carregar ocorrencia",
                `Unable to retrieve ocorrencia from the server.\r\nErrors:
                   "${Utilities.getHttpResponseMessage(error)}"`,
                   MessageSeverity.error, error);
            },
        );
    }

    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.ocorrenciaService.getOcorrencias(this.turnoId, "").subscribe((result) =>
            this.onDataLoadSuccessful(result),
             (error) => this.onDataLoadFailed(error));
    }

    onDataLoadSuccessful(result: Ocorrencia[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.rows = result;
        this.rowsCache = [...result];
    }

    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Erro ao carregar ocorrencia",
        `Unable to retrieve ocorrencia from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
           MessageSeverity.error, error);
    }

}
