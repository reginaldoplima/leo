import { Component, OnInit } from "@angular/core";
import { AlertService, MessageSeverity, DialogType } from "../../services/alert.service";
import { AuthService } from "../../services/auth.service";
import { ActivatedRoute } from "@angular/router";
import { Utilities } from "../../services/utilities";
import { UserCadastro } from "../../models/user-cadastro";
import { ModelUtil } from "../../models/model-utils";
import { CepService } from "../../services/cep.service";

@Component({
    selector: "app-confirma",
    templateUrl: "./confirma.component.html",
    styleUrls: ["./cadastro.component.css"],
})

export class  ConfirmaComponent {
    validLink = false;
    token = "";
    isLoading = false;
    private sub: any;
    userConfirma: UserCadastro = new UserCadastro();
    tipoDeDocumento: boolean = false;

    maskCep = ModelUtil.CEP_MASK;
    maskTelefone = ModelUtil.TELEFONE_MASK;
    maskCnpj = ModelUtil.CNPJ_MASK;
    constructor(private alertService: AlertService,
                private authService: AuthService, private route: ActivatedRoute, private cepService: CepService) {
    }

    chooseMask() {
        if (!this.tipoDeDocumento) {
            return ModelUtil.CPF_MASK;
        } else {
            return ModelUtil.CNPJ_MASK;
        }

    }
    // TODO metodo para limpar
    validaDocumento(identidade) {
        if (!this.tipoDeDocumento) {
            let cpf: string = identidade.value;
            cpf = cpf.replace(/([^0-9]+)/gi, "");
            // console.log(cpf);
            return ModelUtil.IsCpfValid(cpf);
        } else {
            let cnpj: string = identidade.value;
            cnpj = cnpj.replace(/([^0-9]+)/gi, "");
            // console.log(cnpj);
            return ModelUtil.IsCnpjValid(cnpj);
        }
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe((params) => {
           this.token = params.token;
           this.authService.confirmaCadastro(this.token).subscribe(
               (response) => {
                   this.validLink = true;
                   this.alertService.showMessage("Preencha os dados faltantes.", ""
                   , MessageSeverity.info);
               },
               (error) => {
                this.validLink = false;
                this.alertService.showMessage("Informações inválidas.", "", MessageSeverity.error);
               },
           );
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    searchCep(cep) {

        if (cep.valid) {
            this.alertService.showMessage("Procurando Endereço", "", MessageSeverity.info);
            this.cepService.GetEndereco(cep.value).subscribe(
                (endereco) => {
                    // console.log(endereco);
                    this.userConfirma.Bairro = endereco.bairro;
                    this.userConfirma.Logradouro = endereco.logradouro;
                    this.userConfirma.Localidade = endereco.localidade;
                    this.userConfirma.Uf = endereco.uf;
                    this.userConfirma.Complemento = endereco.complemento;
                },
                (error) => {
                    // console.log(error);
                },
            );
        }

    }

    showAlert(msg: string) {
        this.alertService.showMessage("Link Inválido");
    }
    confirmaCadastro() {
        this.isLoading = true;
        this.authService.confirmaDados(this.userConfirma, this.token).subscribe(

            (response) => {
                setTimeout( () => {
                    this.isLoading = false;
                    // console.log(response);
                    this.alertService.showMessage("Obrigado por usar o LEO.", ""
                    , MessageSeverity.success);
                    this.authService.redirectForLogin();
                }, 500);
            },
            (error) => {
                // console.log(error);
                this.showAlert(error);
                this.isLoading = false;
            },
        );
    }

}
