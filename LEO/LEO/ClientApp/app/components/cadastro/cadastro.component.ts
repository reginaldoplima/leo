
import { Component, OnInit, OnDestroy, Input } from "@angular/core";

import { AlertService, MessageSeverity, DialogType } from "../../services/alert.service";
import { AuthService } from "../../services/auth.service";
import { ConfigurationService } from "../../services/configuration.service";
import { Utilities } from "../../services/utilities";
import { UserCadastro } from "../../models/user-cadastro";
import { ModelUtil } from "../../models/model-utils";

@Component({
    selector: "app-cadastro",
    templateUrl: "./cadastro.component.html",
    styleUrls: ["./cadastro.component.css"],
})

export class CadastroComponent {
    userCadastro = new UserCadastro();
    formResetToggle = true;
    isLoading = false;
    maskTelefone = ModelUtil.TELEFONE_MASK;

    constructor(private alertService: AlertService,
                private authService: AuthService,
                private configurations: ConfigurationService) {
    }

    showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

    cadastro() {

        if (!this.userCadastro.CheckConfirmaEmail) {
            this.showErrorAlert("Emails não batem", "Email deve ser igual ao de confirmação");
            return;
        }
        if (!this.userCadastro.CheckConfirmaSenha) {
            this.showErrorAlert("Senhas não batem", "Digite as duas senhas iguais");
            return;
        }
        this.isLoading = true;
        this.alertService.startLoadingMessage("", "Enviando cadastro...");
        this.authService.cadastro(this.userCadastro).subscribe(
            (user) => {
                // console.log(user);
                setTimeout(() => {
                    this.alertService.stopLoadingMessage();
                    this.isLoading = false;
                    this.alertService.showMessage("Cadastrado",
                       `Você recebera um link de que confirmação, cheque ${user.email}!`,
                       MessageSeverity.success);
                }, 1500);
                this.authService.gotoPage("login");
            },
            (error) => {
                {
                     this.alertService.stopLoadingMessage();

                     if (Utilities.checkNoNetwork(error)) {
                                        this.alertService.showStickyMessage(Utilities.noNetworkMessageCaption,
                                            Utilities.noNetworkMessageDetail, MessageSeverity.error, error);
                                        // this.offerAlternateHost();
                                    } else {
                                        const errorMessage = Utilities.findHttpResponseMessage("", error);
                                        // console.log(error)
                                        if (errorMessage) {
                                            // tslint:disable-next-line:max-line-length
                                            this.alertService.showStickyMessage("Não deu pra se cadastrar", errorMessage, MessageSeverity.error, error);
                                        } else {
                                            // tslint:disable-next-line:max-line-length
                                            this.alertService.showStickyMessage("Não deu pra se cadastrar", "Deu ruim, tenta depois amigo.\nError: " + error.statusText || error.status, MessageSeverity.error, error);
                                        }
                                    }
                     setTimeout(() => {
                                        this.isLoading = false;
                                    }, 500);
                                }

            },
        );
    }

}
