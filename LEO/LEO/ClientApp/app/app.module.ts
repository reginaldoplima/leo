import { TurnoService } from "./services/turno/turno-service";
import { TurnoEndpointService } from "./services/turno/turno-endpoint.service";
import { UserPreferencesComponent } from "./components/usuario/user-preferences.component";
import { UserInfoComponent } from "./components/usuario/user-info.component";
import { UsersManagementComponent } from "./components/usuario/users-management.component";

import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule, UrlSerializer } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpModule } from "@angular/http";
import { TextMaskModule } from "angular2-text-mask";

import "bootstrap";

import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { ToastyModule } from "ng2-toasty";
import { ModalModule } from "ngx-bootstrap/modal";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { PopoverModule } from "ngx-bootstrap/popover";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { ChartsModule } from "ng2-charts";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { TimepickerModule  } from "ngx-bootstrap/timepicker";
import { ptBr } from "ngx-bootstrap/locale";
import { defineLocale } from "ngx-bootstrap/bs-moment";
defineLocale("pt-br", ptBr);

import {DragAndDropModule} from "angular-draggable-droppable";

import { AppRoutingModule } from "./app-routing.module";
import { AppErrorHandler } from "./app-error.handler";
import { AppTitleService } from "./services/app-title.service";
import { AppTranslationService, TranslateLanguageLoader } from "./services/app-translation.service";
import { ConfigurationService } from "./services/configuration.service";
import { AlertService } from "./services/alert.service";
import { LocalStoreManager } from "./services/local-store-manager.service";
import { EndpointFactory } from "./services/endpoint-factory.service";
import { NotificationService } from "./services/notification.service";
import { NotificationEndpoint } from "./services/notification-endpoint.service";
import { AccountService } from "./services/account/account.service";
import { AccountEndpoint } from "./services/account/account-endpoint.service";

import { EqualValidator } from "./directives/equal-validator.directive";
import { LastElementDirective } from "./directives/last-element.directive";
import { AutofocusDirective } from "./directives/autofocus.directive";
import { BootstrapTabDirective } from "./directives/bootstrap-tab.directive";
import { BootstrapToggleDirective } from "./directives/bootstrap-toggle.directive";
import { BootstrapSelectDirective } from "./directives/bootstrap-select.directive";
import { BootstrapDatepickerDirective } from "./directives/bootstrap-datepicker.directive";
import { GroupByPipe } from "./pipes/group-by.pipe";

import { AppComponent } from "./components/app.component";
import { LoginComponent } from "./components/login/login.component";
import { CadastroComponent } from "./components/cadastro/cadastro.component";
import { HomeComponent } from "./components/home/home.component";
import { CustomersComponent } from "./components/customers/customers.component";
import { LocaisComponent } from "./components/locais/locais-management.component";
import { OrdersComponent } from "./components/orders/orders.component";
// import { SettingsComponent } from "./components/settings/settings.component";
import { AboutComponent } from "./components/about/about.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";

import { BannerDemoComponent } from "./components/controls/banner-demo.component";
import { TodoDemoComponent } from "./components/controls/todo-demo.component";
import { StatisticsDemoComponent } from "./components/controls/statistics-demo.component";
import { NotificationsViewerComponent } from "./components/controls/notifications-viewer.component";
import { SearchBoxComponent } from "./components/controls/search-box.component";
import { RolesManagementComponent } from "./components/controls/roles-management.component";
import { RoleEditorComponent } from "./components/controls/role-editor.component";
import { ConfirmaComponent } from "./components/cadastro/confirma.component";
import { InputMaskComponent } from "./components/_shared/input-mask.compoent";
import { CepService } from "./services/cep.service";
import { LocalService } from "./services/local/local-service";
import { LocalEndpointService } from "./services/local/local-endpoint.service";
import { NewTurnoComponent } from "./components/turno/new-turno.component";
import { TurnoResolve } from "./services/turno/turno.resolve";
import { LocalResolve } from "./services/local/local.resolve";
import { MeusTurnosListResolve } from "./services/turno/meus-turno-list.resolve";
import { TurnoListComponent } from "./components/turno/turno-list.component";
import { UtcDatePipe } from "./pipes/data-notimezone.pipe";
import { OcorrenciaComponent } from "./components/ocorrencia/ocorrencia-management.component";
import { OcorrenciaEndpointService } from "./services/livro/ocorrencia-endpoit.service";
import { OcorrenciaService } from "./services/livro/ocorrencia-service";
import { LocaisModule } from "./components/locais/locais.module";
import { PtBrDatePipe } from "./pipes/date-ptBr.pipe";
import { FuncionarioModule } from "./components/funcionario/funcionario.module";
import { FuncionarioEndPointService } from "./services/funcionario/funcionario-endpoint.service";
import { FuncionarioService } from "./services/funcionario/funcionario.service";
import { LivroRoutingModule } from "./components/livro/livro.module";
import { SettingsModule } from "./components/settings/settings.module";

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        FormsModule,
        TextMaskModule,
        LocaisModule,
        LivroRoutingModule,
        FuncionarioModule,
        AppRoutingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useClass: TranslateLanguageLoader,
            },
        }),
        NgxDatatableModule,
        ToastyModule.forRoot(),
        TooltipModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        CarouselModule.forRoot(),
        ModalModule.forRoot(),
        DragAndDropModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        ChartsModule,
        // SettingsModule,

    ],
    declarations: [
        AppComponent,
        LoginComponent,
        CadastroComponent,
        ConfirmaComponent,
        HomeComponent,
        CustomersComponent,
        OrdersComponent,
        UsersManagementComponent, UserInfoComponent, UserPreferencesComponent,
        RolesManagementComponent, RoleEditorComponent,
        AboutComponent,
        NotFoundComponent,
        NotificationsViewerComponent,
        StatisticsDemoComponent, TodoDemoComponent, BannerDemoComponent,
        EqualValidator,
        LastElementDirective,
        AutofocusDirective,
        BootstrapToggleDirective,
        BootstrapSelectDirective,
        BootstrapDatepickerDirective,
        GroupByPipe,
        InputMaskComponent,
        NewTurnoComponent,
        TurnoListComponent,
        OcorrenciaComponent,

    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA],
    providers: [
        { provide: "BASE_URL", useFactory: getBaseUrl },
        { provide: ErrorHandler, useClass: AppErrorHandler },
        AlertService,
        ConfigurationService,
        AppTitleService,
        AppTranslationService,
        NotificationService,
        NotificationEndpoint,
        AccountService,
        AccountEndpoint,
        LocalStoreManager,
        EndpointFactory,
        CepService,
        LocalService,
        LocalEndpointService,
        TurnoEndpointService,
        OcorrenciaEndpointService,
        TurnoService,
        TurnoResolve,
        LocalResolve,
        MeusTurnosListResolve,
        OcorrenciaService,
        FuncionarioEndPointService,
        FuncionarioService,

    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName("base")[0].href;
}
