import { User } from "./user.model";

export class Escala {

    constructor(id?: string, inicio?: Date, fim?: Date) {
        this.id = id;
        this.inicio = inicio;
        this.fim = fim;
    }

    public id: string;
    public inicio: Date;
    public fim: Date;
    public applicationUser: User;
    public applicationUserId: string;
    public nomeSetor: string;
    public logradouro: string;
    public bairro: string;
    public numero: string;
    public isOpen: boolean;
    public setorId: string;
}
