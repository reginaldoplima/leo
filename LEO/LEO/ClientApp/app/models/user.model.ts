﻿export class User {
    // tslint:disable-next-line:max-line-length
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, userName?: string, nome?: string, expDate?: string ,
                email?: string, matricula?: string, phoneNumber?: string, roles?: string[]) {

        this.id = id;
        this.userName = userName;
        this.nome = nome;
        this.email = email;
        this.matricula = matricula;
        this.phoneNumber = phoneNumber;
        this.roles = roles;
        this.expDate = expDate;
    }

    get friendlyName(): string {
        let name = this.nome || this.userName;

        if (this.nome) {
            name = this.nome + " " + name;
        }

        return name;
    }

    public id: string;
    public userName: string;
    public nome: string;
    public email: string;
    public matricula: string;
    public phoneNumber: string;
    public isEnabled: boolean;
    public isLockedOut: boolean;
    public roles: string[];
    public expDate: string;
}
