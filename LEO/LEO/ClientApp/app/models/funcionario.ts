
export class Funcionario {

    constructor(nome?: string, id?: string) {

        this.nome = nome;
        this.id = id;
    }

    public id: string;
    public nome: string;

}
