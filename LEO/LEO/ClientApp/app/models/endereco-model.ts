
export class Endereco {

    constructor(
        cep?: string,
        logradouro?: string,
        numero?: string,
        localidade?: string,
        bairro?: string,
        uf?: string,
        complemento?: string,
    ) {
        this.cep = cep;
        this.logradouro = logradouro;
        this.numero = numero;
        this.bairro  = bairro;
        this.uf = uf;
        this.complemento = complemento;
        this.localidade = localidade;
    }

    public cep: string;
    public logradouro: string;
    public numero: string;
    public localidade: string;
    public bairro: string;
    public uf: string;
    public complemento: string;

}
