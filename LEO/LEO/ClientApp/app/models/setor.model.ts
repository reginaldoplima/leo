import { Escala } from "./escala.model";

export class Setor {

    constructor(id: string, nome?: string, escalas?: Escala[], setorId?: string) {
        this.id = id;
        this.nome = nome;
        this.escalas = escalas;
        this.setorId = setorId;
    }

    public id: string;
    public nome: string;
    public escalas: Escala[];
    public setorId: string;
    public localEntityId: string;
}
