import { Injectable } from "@angular/core";

@Injectable()
export class ModelUtil {
    public static readonly BLACKLIST = [
        "00000000000",
        "11111111111",
        "22222222222",
        "33333333333",
        "44444444444",
        "55555555555",
        "66666666666",
        "77777777777",
        "88888888888",
        "99999999999",
        "12345678909",
    ];

    public static readonly STRICT_STRIP_REGEX = /[.-]/g;
    private static readonly LENGTH_CPF_NUMBERS = 9;
    private static readonly LENGTH_CNPJ_NUMBERS = 12;
    private static readonly MOD_11 = 11;

    public static readonly TABELA_MULTIPICADOR_CNPJ_DIG_1 = [
       5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2,
    ];
    public static readonly TABELA_MULTIPICADOR_CNPJ_DIG_2 = [
        6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2,
    ];

    public static readonly CEP_MASK =  [/\d/, /\d/, /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/];
    public static readonly CPF_MASK =  [/\d/, /\d/, /\d/, ".", /\d/,
       /\d/, /\d/, ".", /\d/, /\d/, /\d/, "-", /\d/, /\d/];
    public static readonly TELEFONE_MASK =  ["(", /\d/, /\d/, ")",
       /\d*/, /\d/, /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/];
    public static readonly CNPJ_MASK =  [/\d/, /\d/, ".", /\d/, /\d/,
       /\d/, ".", /\d/, /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/, "-", /\d/, /\d/];

    public static IsCepValid(cep: string) {
        const regex = new RegExp("[0-9]{8}", "g");
        if (!regex.test(cep)) {
            return false;
        } else { return true; }
    }

    public static IsCnpjValid(cnpj: string) {
        const regex = new RegExp("[0-9]{14}", "g");
        if (!regex.test(cnpj)) {
            return false;
        }

        const numbers = this.getArrayNumbersForString(cnpj, 14);
        let sum = 0;
        let index = 0;
        this.TABELA_MULTIPICADOR_CNPJ_DIG_1.map( (n) => {
            sum = +n * numbers[index] + sum;
            index++;
        });

        let mod = sum % this.MOD_11;

        const verifyDigitOne = mod < 2 ? 0 : this.MOD_11 - mod;
        if (verifyDigitOne !== numbers[this.LENGTH_CNPJ_NUMBERS] ) {
            return false;
        }
        sum = 0;
        index = 0;

        this.TABELA_MULTIPICADOR_CNPJ_DIG_2.map( (n) => {
            sum = +n * numbers[index] + sum;
            index++;
        });

        mod = sum % this.MOD_11;
        const verifyDigitTwo = mod < 2 ? 0 : this.MOD_11 - mod;
        if (verifyDigitTwo !== numbers[this.LENGTH_CNPJ_NUMBERS + 1] ) {
            return false;
        } else {
            return true;
        }
    }

    private static getArrayNumbersForString(strNumbers: string, length: number) {
        const retVal = [];
        strNumbers.split("", length).map((n) => {retVal.push(+n); });
        return retVal;
    }

    public static IsCpfValid(cpf: string) {
        const regex = new RegExp("[0-9]{11}", "g");
        if (!regex.test(cpf)) {
            // console.log("this.CPF_NUMBER_REGEX.test");
            return false;
        }
        if (this.BLACKLIST.indexOf(cpf) >= 0) { return false; }
        const numbers =  this.getArrayNumbersForString(cpf, this.MOD_11);
        const verifyDigitOne =
          this.getVerifyDigitOne(numbers.slice(0, this.LENGTH_CPF_NUMBERS));
        if (verifyDigitOne !== numbers[this.LENGTH_CPF_NUMBERS]) {
            // console.log("verifyDigitOne", verifyDigitOne);
            // console.log("verifyDigitOne", numbers[this.LENGTH_CPF_NUMBERS]);
            return false;
        }
        const digitTwo = this.getVerifyDigitTwo(numbers.slice(0, this.LENGTH_CPF_NUMBERS + 1 ));
        if ( digitTwo  !== numbers[this.MOD_11 - 1]) {

            return false;
        }
        // console.log("true");
        return true;
    }

    // tslint:disable-next-line:ban-types
    private static getVerifyDigitOne(numbers: Number[]) {

        if (numbers.length !== this.LENGTH_CPF_NUMBERS ) {
            throw new RangeError(`Numero de digitos validos deve
                    ser exatamente ${this.LENGTH_CPF_NUMBERS}`);
        }

        let init = this.LENGTH_CPF_NUMBERS + 1;
        let sum = 0;
        numbers.map((n) => {
            sum = (init * +n) + sum;
            init = init - 1;
        });

        const mod = sum %  this.MOD_11;

        return (mod < 2 ? 0 : this.MOD_11 - mod  );

    }

    // tslint:disable-next-line:ban-types
    private static getVerifyDigitTwo(numbers: Number[]) {
        if (numbers.length !== this.LENGTH_CPF_NUMBERS + 1 ) {
            throw new RangeError(`Numero de digitos validos deve
                    ser exatamente ${this.LENGTH_CPF_NUMBERS + 1}`);
        }

        let init = this.LENGTH_CPF_NUMBERS + 2;
        let sum = 0;
        numbers.map((n) => {
            sum = (init * +n) + sum;
            init = init - 1;
        });

        const mod = sum %  this.MOD_11;

        return (mod < 2 ? 0 : this.MOD_11 - mod  );
    }
}
