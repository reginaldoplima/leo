import { Funcionario } from "./funcionario";
import { Pagina } from "./livro/pagina-model";
import { SituacaoDePagina, SituacaoDeHorario } from "./enums";
import { distanceInWordsToNow } from "date-fns";

export class Turno {

    constructor(inicio: Date, fim: Date, localId: string, escalados?: Funcionario []) {
        this.inicio = inicio;
        this.fim = fim;
        this.localId = localId;
        this.escalados = [];

    }
    public id: string;
    public inicio: Date;
    public fim: Date;
    public escalados: Funcionario[];
    public localId: string;
    public pagina: Pagina;
    public totalMinutosParaInicioDoTurno: string;
    public situacaoHorarioFinal: string;


    toJSON() {
        return { inicio: this.inicio, fim: this.fim,
            localId: this.localId, escalados: this.escalados };
    }

}
