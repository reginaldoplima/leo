﻿// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
// 
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================

export enum Gender {
    None,
    Female,
    Male
}

export enum Documento {
    None,
    Cnpj,
    Cpf
}

export enum SituacaoDePagina {
    Sem,
    Aberta,
    Fechada
}

export enum SituacaoDeHorario {
    NaoIniciado,
    DentroDoHorario,
    Passou
}

export enum TipoDeOcorrencia {
    Obrigatoria,
    NaoObrigatoria
}

export enum NomeOcorrencia {
    Garagem = "Gararem",
    Oficina = "Oficina"
}