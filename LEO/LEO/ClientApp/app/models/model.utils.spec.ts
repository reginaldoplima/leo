import { ModelUtil }  from './model-utils';
describe('Service: ModelUtils Services', () => {
    it('should return `true` for this valids Cpf', 
       () => {
        expect(ModelUtil.IsCpfValid("03921117488")).toBe(true);
        expect(ModelUtil.IsCpfValid("65564419707")).toBe(true);
    });
    it('should return `false` for this invalids Cpf', 
     () => {
       expect(ModelUtil.IsCpfValid("83961134000104")).toBe(false);
    });
    it('should return `true` for this valids Cnpj', 
     () => {
       expect(ModelUtil.IsCnpjValid("83961134000104")).toBe(true);
    });
    it('should return `false` for this invalids Cnpj', 
    () => {
      expect(ModelUtil.IsCnpjValid("82961134000104")).toBe(false);
   });
});