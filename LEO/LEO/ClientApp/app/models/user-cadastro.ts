
export class UserCadastro {
    // Note: Using only optional constructor
    // properties without backing store disables typescript's type checking for the type
    constructor(Nome?: string,
                Email?: string,
                EmailConfirma?: string,
                NumeroDoDocumento?: string,
                Telefone?: string,
                Cep?: string,
                Logradouro?: string,
                Numero?: string,
                Localidade?: string,
                Bairro?: string,
                Uf?: string,
                Complemento?: string,
                Senha?: string,
                SenhaConfirma?: string,
    ) {

        this.Nome = Nome;
        this.Email = Email;
        this.EmailConfirma = EmailConfirma;
        this.Senha = Senha;
        this.SenhaConfirma = SenhaConfirma;

        this.NumeroDoDocumento = NumeroDoDocumento;
        this.Telefone = Telefone;
        // endereco
        this.Cep = Cep;
        this.Logradouro = Logradouro;
        this.Numero = Numero;
        this.Localidade = Localidade;
        this.Bairro = Bairro;
        this.Uf = Uf;
        this.Complemento = Complemento;
        //
    }

    public Nome: string;

    public Email: string;
    public EmailConfirma: string;
    public Senha: string;
    public SenhaConfirma: string;

    public NumeroDoDocumento: string;
    public Telefone: string;
    // endereco
    public Cep: string;
    public Logradouro: string;
    public Numero: string;
    public Localidade: string;
    public Bairro: string;
    public Uf;
    public Complemento;
    //

    get CheckConfirmaEmail() {
        return this.Email === this.EmailConfirma;
    }

    get CheckConfirmaSenha() {
        return this.Senha === this.SenhaConfirma;
    }
}
