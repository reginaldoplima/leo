import { Page } from "./pagina";
// tslint:disable:max-classes-per-file
export class Livro {
    public id: string;
    public local: string;
    public setor: string;
    public criador: string;
    public paginas: Page[];
    public nome: string;
    public tabelaOcorrenciasId: string;
    public configView: ConfigView;
    public applicationUserId: string;
}

export class ConfigView {
    public configProperties: ConfigProperties[];
}
export class ConfigProperties {

    public htmlInput: string;
    public isColumn: boolean;
    public labelName: string;
    public mask: string;
    public propertyName: string;
}
