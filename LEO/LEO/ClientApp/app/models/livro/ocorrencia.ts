import { TipoDeOcorrencia } from "../enums";

export class Ocorrencia {
  public  nome: string;
  public  paginaId: string ;
  public  anexos: string[];
  public  descricao: string ;
  public  ocorrido: string ;
  public  emailsParaComunicacao: string[] ;
  public  isComunicacaoObrigatoria: boolean ;
  public tipoDeOcorrencia: TipoDeOcorrencia;
  public data: Date;
}
