import { FechamentoDeTurno } from "./fechamento";
import { AberturaDeTurno } from "./abertura";
import { Ocorrencia } from "./ocorrencia";
import { SituacaoDePagina } from "../enums";


export class Pagina {

    constructor(turnoId: string) {
        this.turnoId = turnoId;
    }

    public id: string;
    public turnoId: string;
    public abertura: AberturaDeTurno;
    public fechamento: FechamentoDeTurno;
    public ocorrencias: Ocorrencia[];

    public get situacaoPagina() : SituacaoDePagina {
        let _value : SituacaoDePagina = SituacaoDePagina.Sem;
        const isAberto = this.abertura !== null;
        const isFechado = this.abertura !== null;
        _value = !isFechado ? SituacaoDePagina.Aberta: SituacaoDePagina.Fechada;
        return _value;
    }
}
