
export class Page {

    public  id: string;
    public  inicio: Date;
    public  fim: Date;
    public  recebidoDe: string;
    public  criador: string;
    public observacoesAbertura: string;
    public observacoesFechamento: string;
    // The number of elements in the page
    size: number = 0;
    // The total number of elements
    totalElements: number = 0;
    // The total number of pages
    totalPages: number = 0;
    // The current page number
    pageNumber: number = 0;
}
