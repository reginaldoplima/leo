
export class FechamentoDeTurno {

    public id: string;
    public paginas: string[] ;
    public funcionarioId: string;
    public dataFechamento: Date;
    public chaveAbertura: string;
    public observacoes: string;
}
