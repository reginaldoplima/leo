
export class AberturaDeTurno {
    public id: string;
    public idEscalaAnterior: string ;
    public dataAbertura: Date;
    public observacoes: string;

}
