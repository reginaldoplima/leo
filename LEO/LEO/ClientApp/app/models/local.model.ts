import { Endereco } from "./endereco-model";
import { Turno } from "./turno-model";

export class Local {

    constructor(observaqcao?: string , Id?: string , nome?: string, descricao?: string,
                endereco?: Endereco, idCriador?: string, turnos?: Turno[]) {

        this.nome = nome;
        this.descricao = descricao;
        this.endereco = endereco;
        this.idCriador = idCriador;
        this.observacao = observaqcao;
        this.turnos = turnos;
    }

    public Id: string;
    public nome: string;
    public descricao: string;
    public idCriador: string;
    public endereco: Endereco;
    public observacao: string;
    public turnos: Turno[];
}
