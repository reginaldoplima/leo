﻿
// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
//
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================

import { Injector } from "@angular/core";
import { Injectable } from "@angular/core";
import { Router, NavigationExtras } from "@angular/router";
import { Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import "rxjs/add/operator/map";

import { LocalStoreManager } from "./local-store-manager.service";
import { EndpointFactory } from "./endpoint-factory.service";
import { ConfigurationService } from "./configuration.service";
import { DBkeys } from "./db-keys";
import { JwtHelper } from "./jwt-helper";
import { Utilities } from "./utilities";
import { User } from "../models/user.model";
import { Permission, PermissionNames, PermissionValues } from "../models/permission.model";
import { UserCadastro } from "../models/user-cadastro";

@Injectable()
export class AuthService {

    public get loginUrl() { return this.configurations.loginUrl; }
    public get homeUrl() { return this.configurations.homeUrl; }

    public loginRedirectUrl: string;
    public logoutRedirectUrl: string;

    public reLoginDelegate: () => void;

    private previousIsLoggedInCheck = false;
    // tslint:disable-next-line:variable-name
    private _loginStatus = new Subject<boolean>();
    private endpointFactory: EndpointFactory;

    constructor(private router: Router,
                private configurations: ConfigurationService,
                private injector: Injector,
                private localStorage: LocalStoreManager) {
        this.endpointFactory = injector.get(EndpointFactory);
        this.initializeLoginStatus();
    }

    private initializeLoginStatus() {
        this.localStorage.getInitEvent().subscribe(() => {
            this.reevaluateLoginStatus();
        });
    }

    gotoPage(page: string, preserveParams = true) {

        const navigationExtras: NavigationExtras = {
            queryParamsHandling: preserveParams ? "merge" : "",
            preserveFragment: preserveParams,
        };

        this.router.navigate([page], navigationExtras);
    }

    redirectLoginUser() {
        const redirect = this.loginRedirectUrl && this.loginRedirectUrl !== "/" &&
           this.loginRedirectUrl !== ConfigurationService.defaultHomeUrl ?
           this.loginRedirectUrl : this.homeUrl;
        this.loginRedirectUrl = null;

        const urlParamsAndFragment = Utilities.splitInTwo(redirect, "#");
        const urlAndParams = Utilities.splitInTwo(urlParamsAndFragment.firstPart, "?");

        const navigationExtras: NavigationExtras = {
            fragment: urlParamsAndFragment.secondPart,
            queryParams: Utilities.getQueryParamsFromString(urlAndParams.secondPart),
            queryParamsHandling: "merge"};

        this.router.navigate([urlAndParams.firstPart], navigationExtras);
    }

    redirectLogoutUser() {
        const redirect = this.logoutRedirectUrl ? this.logoutRedirectUrl : this.loginUrl;
        this.logoutRedirectUrl = null;

        this.router.navigate([redirect]);
    }

    redirectForLogin() {
        this.loginRedirectUrl = this.router.url;
        this.router.navigate([this.loginUrl]);
    }

    reLogin() {

        this.localStorage.deleteData(DBkeys.TOKEN_EXPIRES_IN);

        if (this.reLoginDelegate) {
            this.reLoginDelegate();
        } else {
            this.redirectForLogin();
        }
    }

    refreshLogin() {
        return this.endpointFactory.getRefreshLoginEndpoint()
            .map((response: Response) => this.processLoginResponse(response, this.rememberMe));
    }

    login(userName: string, password: string, rememberMe?: boolean) {

        if (this.isLoggedIn) {
            this.logout();
        }

        return this.endpointFactory.getLoginEndpoint(userName, password)
            .map((response: Response) => this.processLoginResponse(response, rememberMe));
    }

    cadastro(user: UserCadastro) {
       return this.endpointFactory.getCadastroEndpoint(user)
        .map((response: Response) => this.processCadastroResponse(response));
    }

    confirmaCadastro(token: string) {
        return this.endpointFactory.getConfirmaEndpoint(token)
         .map((response: Response) => this.processConfirmaResponse(response));
     }

     confirmaDados(user: UserCadastro, userId: string) {
        return this.endpointFactory.getCadastroDadosEndpoint(user, userId)
         .map((response: Response) => this.processConfirmaResponse(response));
     }

     private processConfirmaResponse(response: Response) {
        const responseObj = response;
        return responseObj;
     }

    private processCadastroResponse(response: Response) {
        const responseObj = response.json();
        const user = new User();
        user.email = responseObj.email;
        return user;
    }

    private processLoginResponse(response: Response, rememberMe: boolean) {

        // tslint:disable-next-line:variable-name
        const response_token = response.json();
        const accessToken = response_token.access_token;

        if (accessToken == null) {
            throw new Error("Received accessToken was empty");
        }

        const idToken: string = response_token.id_token;
        const refreshToken: string = response_token.refresh_token;
        const expiresIn: number = response_token.expires_in;

        const tokenExpiryDate = new Date();
        tokenExpiryDate.setSeconds(tokenExpiryDate.getSeconds() + expiresIn);

        const accessTokenExpiry = tokenExpiryDate;

        const jwtHelper = new JwtHelper();
        const decodedIdToken = jwtHelper.decodeToken(response_token.id_token);

        const permissions: PermissionValues[] =
          Array.isArray(decodedIdToken.permission) ?
             decodedIdToken.permission : [decodedIdToken.permission];

        if (!this.isLoggedIn) {
            this.configurations.import(decodedIdToken.configuration);
        }
        // console.log(decodedIdToken);
        const user = new User(
            decodedIdToken.sub,
            decodedIdToken.name,
            decodedIdToken.fullname,
            decodedIdToken.expiration,
            decodedIdToken.email,
            decodedIdToken.jobtitle,
            decodedIdToken.phone,
            Array.isArray(decodedIdToken.role) ?
             decodedIdToken.role : [decodedIdToken.role]);
        user.isEnabled = true;

        this.saveUserDetails(user, permissions, accessToken, idToken,
             refreshToken, accessTokenExpiry, rememberMe);

        this.reevaluateLoginStatus(user);

        return user;
    }

    private saveUserDetails(user: User,
                            permissions: PermissionValues[],
                            accessToken: string,
                            idToken: string,
                            refreshToken: string,
                            expiresIn: Date, rememberMe: boolean) {

        if (rememberMe) {
            this.localStorage.savePermanentData(accessToken, DBkeys.ACCESS_TOKEN);
            this.localStorage.savePermanentData(idToken, DBkeys.ID_TOKEN);
            this.localStorage.savePermanentData(refreshToken, DBkeys.REFRESH_TOKEN);
            this.localStorage.savePermanentData(expiresIn, DBkeys.TOKEN_EXPIRES_IN);
            this.localStorage.savePermanentData(permissions, DBkeys.USER_PERMISSIONS);
            this.localStorage.savePermanentData(user, DBkeys.CURRENT_USER);
        } else {
            this.localStorage.saveSyncedSessionData(accessToken, DBkeys.ACCESS_TOKEN);
            this.localStorage.saveSyncedSessionData(idToken, DBkeys.ID_TOKEN);
            this.localStorage.saveSyncedSessionData(refreshToken, DBkeys.REFRESH_TOKEN);
            this.localStorage.saveSyncedSessionData(expiresIn, DBkeys.TOKEN_EXPIRES_IN);
            this.localStorage.saveSyncedSessionData(permissions, DBkeys.USER_PERMISSIONS);
            this.localStorage.saveSyncedSessionData(user, DBkeys.CURRENT_USER);
        }

        this.localStorage.savePermanentData(rememberMe, DBkeys.REMEMBER_ME);
    }

    logout(): void {
        this.localStorage.deleteData(DBkeys.ACCESS_TOKEN);
        this.localStorage.deleteData(DBkeys.ID_TOKEN);
        this.localStorage.deleteData(DBkeys.REFRESH_TOKEN);
        this.localStorage.deleteData(DBkeys.TOKEN_EXPIRES_IN);
        this.localStorage.deleteData(DBkeys.USER_PERMISSIONS);
        this.localStorage.deleteData(DBkeys.CURRENT_USER);

        this.configurations.clearLocalChanges();

        this.reevaluateLoginStatus();
    }

    private reevaluateLoginStatus(currentUser?: User) {

        const user = currentUser || this.localStorage.getDataObject<User>(DBkeys.CURRENT_USER);
        const isLoggedIn = user != null;

        if (this.previousIsLoggedInCheck !== isLoggedIn) {
            setTimeout(() => {
                this._loginStatus.next(isLoggedIn);
            });
        }

        this.previousIsLoggedInCheck = isLoggedIn;
    }

    getLoginStatusEvent(): Observable<boolean> {
        return this._loginStatus.asObservable();
    }

    get currentUser(): User {

        const user = this.localStorage.getDataObject<User>(DBkeys.CURRENT_USER);
        this.reevaluateLoginStatus(user);
        return user;
    }

    get userPermissions(): PermissionValues[] {
        return this.localStorage.getDataObject<PermissionValues[]>(DBkeys.USER_PERMISSIONS) || [];
    }

    get accessToken(): string {

        this.reevaluateLoginStatus();
        return this.localStorage.getData(DBkeys.ACCESS_TOKEN);
    }

    get accessTokenExpiryDate(): Date {

        this.reevaluateLoginStatus();
        return this.localStorage.getDataObject<Date>(DBkeys.TOKEN_EXPIRES_IN, true);
    }

    get isSessionExpired(): boolean {

        if (this.accessTokenExpiryDate == null) {
            return true;
        }

        return !(this.accessTokenExpiryDate.valueOf() > new Date().valueOf());
    }

    get idToken(): string {

        this.reevaluateLoginStatus();
        return this.localStorage.getData(DBkeys.ID_TOKEN);
    }

    get refreshToken(): string {

        this.reevaluateLoginStatus();
        return this.localStorage.getData(DBkeys.REFRESH_TOKEN);
    }

    get isLoggedIn(): boolean {
        return this.currentUser != null;
    }

    get rememberMe(): boolean {
        return this.localStorage.getDataObject<boolean>(DBkeys.REMEMBER_ME) === true;
    }
}
