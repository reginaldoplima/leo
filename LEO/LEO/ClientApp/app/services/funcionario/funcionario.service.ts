import { FechamentoDeTurno } from "./../../models/livro/fechamento";
import { AberturaDeTurno } from "./../../models/livro/abertura";
import { Injectable } from "@angular/core";
import { FuncionarioEndPointService } from "./funcionario-endpoint.service";
import { Http, Response } from "@angular/http";
import { Escala } from "../../models/escala.model";

@Injectable()
export class FuncionarioService {

    constructor(private entpointService: FuncionarioEndPointService) {
    }

    getUserEscalas() {
        return this.entpointService.getUserEscalasEndpoint()
            .map((response: Response) => response.json() as Escala []);
    }
    openEscala(escalaId: string, abertura: AberturaDeTurno) {
        return this.entpointService.openEscalaEndpoint(escalaId, abertura)
         .map((response: Response) => response.json() as string);
    }

    fecharEscala(escalaId: string, fechamento: FechamentoDeTurno ) {
        return this.entpointService.fecharEscalaEndPoint(escalaId, fechamento)
        .map((response: Response) => response.json() as string);
    }

    ultimasPaginasFechadasByEscalaId(escalaId: string) {
        return this.entpointService.ultimasPaginasFechadasByEscalaIdEndpoint(escalaId)
         .map((response: Response) => response.json() as any []);
    }

}
