import { FechamentoDeTurno } from "./../../models/livro/fechamento";
import { AberturaDeTurno } from "./../../models/livro/abertura";
import { Injectable, Injector } from "@angular/core";
import { EndpointFactory } from "../endpoint-factory.service";
import { Http, Response } from "@angular/http";
import { ConfigurationService } from "../configuration.service";
import { Observable } from "rxjs/Observable";

@Injectable()
export class FuncionarioEndPointService  extends EndpointFactory {
    private readonly uri: string = "/api/funcionario";
    get funcionarioUri() { return this.configurations.baseUrl + this.uri; }

    constructor(http: Http, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }

    getUserEscalasEndpoint(): Observable<Response> {
        const endpointUrl = this.funcionarioUri + "/escala";
        return this.http.get(endpointUrl, this.getAuthHeader())
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.getUserEscalasEndpoint());
        });
    }
    openEscalaEndpoint(escalaId: string , abertura: AberturaDeTurno): Observable<Response> {
        const endpointUrl = this.funcionarioUri + `/escala/${escalaId}/abrir`;
        return this.http.post(endpointUrl, abertura , this.getAuthHeader(true))
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.getUserEscalasEndpoint());
        });
    }

    fecharEscalaEndPoint(escalaId: string , fechamento: FechamentoDeTurno ): Observable<Response> {
        const endpointUrl = this.funcionarioUri + `/escala/${escalaId}/fechar`;
        return this.http.post(endpointUrl, fechamento , this.getAuthHeader(true))
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.getUserEscalasEndpoint());
        });
    }

    ultimasPaginasFechadasByEscalaIdEndpoint(escalaId: string) {
        const endpointUrl = this.funcionarioUri + `/escala/${escalaId}/ultimaPaginaFechada`;
        return this.http.get(endpointUrl, this.getAuthHeader())
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.ultimasPaginasFechadasByEscalaIdEndpoint(escalaId));
        });
    }

}
