import { Turno } from "./../../models/turno-model";

import { TurnoService } from "./turno-service";
import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class MeusTurnosListResolve implements Resolve<Turno[]> {

  constructor(private turnoService: TurnoService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.turnoService.getMeusTurnos(-1, -1).map(
        (turnos) => turnos,
        (error) => {
          
        }
    );
  }
}
