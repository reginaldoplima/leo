import { Turno } from "./../../models/turno-model";

import { TurnoService } from "./turno-service";
import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class TurnoResolve implements Resolve<Turno[]> {

  constructor(private turnoService: TurnoService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const localId = route.paramMap.get("localId");
    return this.turnoService.getTurnos(-1, -1, localId).map(
        (turnos) => turnos,
    );
  }
}
