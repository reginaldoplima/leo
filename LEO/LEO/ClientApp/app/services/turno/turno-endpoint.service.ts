import { Injectable, Injector } from "@angular/core";
import { EndpointFactory } from "../endpoint-factory.service";
import { Http, Response } from "@angular/http";
import { ConfigurationService } from "../configuration.service";
import { Observable } from "rxjs/Observable";
import { Turno } from "../../models/turno-model";
import { AberturaDeTurno } from "../../models/livro/abertura";

@Injectable()
export class TurnoEndpointService extends EndpointFactory {

    // tslint:disable-next-line:variable-name
    private readonly _turnoUrl: string = "/api/turno";
    get turnoUrl() { return this.configurations.baseUrl + this._turnoUrl; }

    constructor(http: Http, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }

    getMeusTurnosEndpoint(): Observable<Response> {
        const endpointUrl = this.turnoUrl;

        return this.http.get(endpointUrl + `/me` , this.getAuthHeader())
            .map((response: Response) => {
                return response;
            })
            .catch((error) => {
                return this.handleError(error, () => this.getMeusTurnosEndpoint());
            });
    }

    getTurnoEndpoint(localId: string): Observable<Response> {
        const endpointUrl = this.turnoUrl;

        return this.http.get(endpointUrl + `/${localId}` , this.getAuthHeader())
            .map((response: Response) => {
                return response;
            })
            .catch((error) => {
                return this.handleError(error, () => this.getTurnoEndpoint(localId));
            });
    }

    abrirTurnoEndpoint(turnoId: string, eventoAbertura: AberturaDeTurno ) : Observable<Response> {
        const endpointUrl = this.turnoUrl + `/evento/abertura/${turnoId}`;
        return this.http.post(endpointUrl, eventoAbertura,
            this.getAuthHeader(true))
         .map((response: Response) => {
             // console.log(response);
             return response;
         })
         .catch((error) => {
             return this.handleError(error, () => this.abrirTurnoEndpoint(turnoId, eventoAbertura));
         });

    }

    setNewTurnoEndpoint(turnoObject: Turno): Observable<Response> {
        const endpointUrl = this.turnoUrl;
        const offSeTimezoneCorrection = turnoObject.inicio.getTimezoneOffset() / -60;
        turnoObject.inicio.setHours(turnoObject.inicio.getHours() + offSeTimezoneCorrection);
        turnoObject.fim.setHours(turnoObject.fim.getHours() + offSeTimezoneCorrection);
        return this.http.post(endpointUrl, turnoObject,
           this.getAuthHeader(true))
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.setNewTurnoEndpoint(turnoObject));
        });
    }

    deleteTurnoEndpoint(id: string) {
        const endpointUrl = this.turnoUrl + `/${id}`;
        return this.http.delete(endpointUrl,
            this.getAuthHeader(true))
         .map((response: Response) => {
             return response;
         })
         .catch((error) => {
             return this.handleError(error, () => this.deleteTurnoEndpoint(id));
         });

    }
}
