
import { TurnoEndpointService } from "./turno-endpoint.service";
import { AuthService } from "../auth.service";
import { Turno } from "../../models/turno-model";

import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Http, Response } from "@angular/http";
import { AberturaDeTurno } from "../../models/livro/abertura";

@Injectable()
export class TurnoService {

    constructor(private router: Router, private http: Http, private authService: AuthService,
                private turnoEndpoint: TurnoEndpointService) {

    }

    getMeusTurnos(page?: number, pageSize?: number) {
        return this.turnoEndpoint.getMeusTurnosEndpoint()
            .map((response: Response) => response.json() as Turno[]);
    }

    getTurnos(page?: number, pageSize?: number, localId?: string) {
        return this.turnoEndpoint.getTurnoEndpoint(localId)
            .map((response: Response) => response.json() as Turno[]);
    }

    abrirTurno(turnoId: string, eventoAbertura: AberturaDeTurno ) {
        return this.turnoEndpoint.abrirTurnoEndpoint(turnoId, eventoAbertura)
            .map((response: Response) => response.json() as Turno);
    }

    newTurno(turno: Turno) {
        return this.turnoEndpoint.setNewTurnoEndpoint(turno)
            .map((response: Response) => response.json() as Turno);
    }

    deleteTurno(id: string) {
      return this.turnoEndpoint.deleteTurnoEndpoint(id)
        .map((response: Response) => response.json() as any);
    }
}
