
import { LocalService } from "./local-service";
import { Local } from "./../../models/local.model";

import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class LocalResolve implements Resolve<Local> {

  constructor(private service: LocalService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const localId = route.paramMap.get("localId");
    return this.service.getLocal(localId).map(
        (local) => local,
    );
  }
}
