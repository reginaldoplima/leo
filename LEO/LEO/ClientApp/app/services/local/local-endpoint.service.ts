import { Setor } from "./../../models/setor.model";
import { Injectable, Injector } from "@angular/core";
import { EndpointFactory } from "../endpoint-factory.service";
import { Http, Response } from "@angular/http";
import { ConfigurationService } from "../configuration.service";
import { Observable } from "rxjs/Observable";

@Injectable()
export class LocalEndpointService extends EndpointFactory {

    // tslint:disable-next-line:variable-name
    private readonly _localUrl: string = "/api/local";
    get localUrl() { return this.configurations.baseUrl + this._localUrl; }

    constructor(http: Http, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }

    getAllLocalEndpoint(): Observable<Response> {
        const endpointUrl = this.localUrl;

        return this.http.get(endpointUrl, this.getAuthHeader())
            .map((response: Response) => {
                return response;
            })
            .catch((error) => {
                return this.handleError(error, () => this.getAllLocalEndpoint());
            });
    }

    getLocalEndpoint(id: string): Observable<Response> {
        const endpointUrl = this.localUrl + `/${id}`;

        return this.http.get(endpointUrl, this.getAuthHeader())
            .map((response: Response) => {
                return response;
            })
            .catch((error) => {
                return this.handleError(error, () => this.getLocalEndpoint(id));
            });
    }

    getSetoresEndpoint(id: string): Observable<Response> {
        const endpointUrl = this.localUrl + `/${id}/setores`;
        return this.http.get(endpointUrl, this.getAuthHeader())
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.getSetoresEndpoint(id));
        });
    }

    novoSetorEndPoint(localId: string, nome: string): Observable<Response> {
        //
        const endpointUrl = this.localUrl + `/${localId}/setores`;
        const setor = new Setor("");
        setor.nome = nome;
        return this.http.post(endpointUrl, setor , this.getAuthHeader(true))
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.novoSetorEndPoint(localId, nome));
        });
    }

    novaEscalaEndPoint(escala: any , localId: string, setorId: string): Observable<Response> {
        //
        const endpointUrl = this.localUrl + `/${localId}/setores/${setorId}/escalas`;
        return this.http.post(endpointUrl, escala , this.getAuthHeader(true))
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.novaEscalaEndPoint(escala, localId, setorId));
        });
    }

    getEscalasEndPoint(localId: string, setorId: string): Observable<Response> {
        const endpointUrl = this.localUrl + `/${localId}/setores/${setorId}/escalas`;
        return this.http.get(endpointUrl, this.getAuthHeader())
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.getEscalasEndPoint(localId, setorId));
        });
    }

    setNewLocalEndpoint(localObject: any): Observable<Response> {
        const endpointUrl = this.localUrl;

        // tslint:disable-next-line:no-console
        console.log(JSON.stringify(localObject));
        return this.http.post(endpointUrl, localObject,
           this.getAuthHeader(true))
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.setNewLocalEndpoint(localObject));
        });
    }
}
