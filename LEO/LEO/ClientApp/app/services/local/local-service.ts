import { Injectable } from "@angular/core";

import { Local } from "../../models/local.model";
import { Router } from "@angular/router";
import { Http, Response } from "@angular/http";
import { AuthService } from "../auth.service";
import { LocalEndpointService } from "./local-endpoint.service";
import { Setor } from "../../models/setor.model";
import { Escala } from "../../models/escala.model";

@Injectable()
export class LocalService {

    constructor(private router: Router, private http: Http, private authService: AuthService,
                private localEndpoint: LocalEndpointService) {

    }

    getLocais(page?: number, pageSize?: number) {
        return this.localEndpoint.getAllLocalEndpoint()
            .map((response: Response) => response.json() as Local[]);
    }

    getLocal(id: string) {
        return this.localEndpoint.getLocalEndpoint(id)
            .map((response: Response) => response.json() as Local);
    }

    getSetores(id: string) {
        return this.localEndpoint.getSetoresEndpoint(id)
            .map((response: Response) => response.json() as Setor[]);
    }

    newLocal(local: Local) {
        return this.localEndpoint.setNewLocalEndpoint(local)
            .map((response: Response) => response.json() as Local);
    }

    novoSetor(locaId: string, nome: string ) {
        return this.localEndpoint.novoSetorEndPoint(locaId, nome)
          .map( (response: Response ) => response.json() as Setor );
    }

    novaEscala(escala: Escala, localId: string, setorId: string) {
        return this.localEndpoint.novaEscalaEndPoint(escala, localId, setorId)
        .map( (response: Response ) => response.json() as Escala );
    }

    getEscalas(localId: string, setorId: string) {
        return this.localEndpoint.getEscalasEndPoint(localId, setorId)
        .map( (response: Response ) => response.json() as Escala []);
    }
}
