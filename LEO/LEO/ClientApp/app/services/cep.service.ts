import { AlertService, MessageSeverity } from "./alert.service";
import { Endereco } from "./../models/endereco-model";
/* tslint:disable */
import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map';



@Injectable()
export class CepService {
    /**
     *
     */
    constructor(protected http: Http, private alertService: AlertService) {
        
        
    }

    public GetEndereco (cep:string){
        let cepOnlyNumbers = cep.replace("-","");
        console.log(cep.replace("-",""));
        return this.http.get(`https://viacep.com.br/ws/${cepOnlyNumbers}/json/`)
          .map( (response: Response) => this._processResponse(response));
    }
    
    private _processResponse(response:Response){
        let endereco: Endereco;
        let response_json =  response.json();
        endereco = new Endereco(
            response_json.cep,
            response_json.logradouro,
            "",
            response_json.localidade,
            response_json.bairro,
            response_json.uf,
            response_json.complemento
        );
        console.log(endereco)
        return endereco;
    }

    public SearchCep(cep) {
        let retEndereco = new Endereco();
        if (cep.valid) {
            this.alertService.showMessage("Procurando Endereço", "", MessageSeverity.info);
            this.GetEndereco(cep.value).subscribe(
                (endereco) => {
                // console.log(endereco);
                    retEndereco.bairro = endereco.bairro;
                    retEndereco.logradouro = endereco.logradouro;
                    retEndereco.localidade = endereco.localidade;
                    retEndereco.uf = endereco.uf;
                    retEndereco.complemento = endereco.complemento;
                },
                    (error) => {
                    // console.log(error);
                },
            );
         }
        
    }

}