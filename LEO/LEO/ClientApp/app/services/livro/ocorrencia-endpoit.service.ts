import { Injectable, Injector } from "@angular/core";
import { EndpointFactory } from "../endpoint-factory.service";
import { Http, Response } from "@angular/http";
import { ConfigurationService } from "../configuration.service";
import { Observable } from "rxjs/Observable";

// tslint:disable:no-console
@Injectable()
export class OcorrenciaEndpointService extends EndpointFactory {

    // tslint:disable-next-line:variable-name
    private readonly _livroUrl: string = "/api/livro";
    get livroUrl() { return this.configurations.baseUrl + this._livroUrl; }

    constructor(http: Http, configurations: ConfigurationService, injector: Injector) {
        super(http, configurations, injector);
    }

    getOcorrenciasByTurnoEndpoint(livroId: string , paginaId: string ): Observable<Response> {
        const endpointUrl = this.livroUrl + `/${livroId}/pagina/${paginaId}/ocorrencias`;
        return this.http.get(endpointUrl, this.getAuthHeader())
            .map((response: Response) => {
                return response;
            })
            .catch((error) => {
                return this.handleError(error, () => this.getOcorrenciasByTurnoEndpoint(livroId, paginaId));
            });
    }

    getLivrosBySetorIdEndPoint(setorId: string): Observable<Response> {
        const endpointUrl = this.livroUrl + `/setor/${setorId}`;
        return this.http.get(endpointUrl, this.getAuthHeader())
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.getLivrosBySetorIdEndPoint(setorId));
        });
    }

    getLastOpenPageBySetorIdEndPoint(setorId: string): Observable<Response> {
        const endpointUrl = this.livroUrl + `/setor/${setorId}/paginas/vazias`;
        return this.http.get(endpointUrl, this.getAuthHeader())
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error,
                () => this.getLastOpenPageBySetorIdEndPoint(setorId));
        });
    }

    getLocalEndpoint(id: string): Observable<Response> {
        const endpointUrl = this.livroUrl + `/${id}`;

        return this.http.get(endpointUrl, this.getAuthHeader())
            .map((response: Response) => {
                return response;
            })
            .catch((error) => {
                return this.handleError(error, () => this.getLocalEndpoint(id));
            });
    }

    novaOcorrenciaEndpoint(livroId: string, paginaId: string, ocorrenciaObj: any): Observable<Response> {
        const endpointUrl = this.livroUrl + `/${livroId}/pagina/${paginaId}`;
        console.log(JSON.stringify(ocorrenciaObj));
        return this.http.post(endpointUrl, ocorrenciaObj,
           this.getAuthHeader(true))
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.novaOcorrenciaEndpoint(livroId, paginaId, ocorrenciaObj));
        });
    }
    getPaginaEndpoint(paginaId: string): Observable<Response>  {
        const endpointUrl = this.livroUrl + `/${paginaId}`;
        return this.http.get(endpointUrl, this.getAuthHeader())
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.getLocalEndpoint(paginaId));
        });
    }

    getDetalhesEndpoint(idLivro: string, idDoc: string): Observable<Response> {
        const endpointUrl = this.livroUrl +  `/${idLivro}/${idDoc}/detalhes`;
        return this.http.get(endpointUrl, this.getAuthHeader())
        .map((response: Response) => {
            return response;
        })
        .catch((error) => {
            return this.handleError(error, () => this.getDetalhesEndpoint(idLivro, idDoc));
        });
    }

    addArquivoEmOcorrenciaEndpoint(livroid: string,
                                   paginaid: string, ocorrenciaid: number, arquivos: any) {
        const endpointUrl = this.livroUrl + `/${livroid}/pagina/${paginaid}/ocorrencias/${ocorrenciaid}`;
        return this.http.post(endpointUrl, arquivos)
         .map((response: Response) => {
             return response;
         })
         .catch((error) => {
             return this.handleError(error, () =>
               this.addArquivoEmOcorrenciaEndpoint(livroid, paginaid, ocorrenciaid, arquivos));
         });
    }
}
