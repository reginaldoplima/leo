import { Injectable } from "@angular/core";

import { Router } from "@angular/router";
import { Http, Response } from "@angular/http";
import { AuthService } from "../auth.service";
import { Ocorrencia } from "../../models/livro/ocorrencia";
import { OcorrenciaEndpointService } from "./ocorrencia-endpoit.service";
import { Livro } from "../../models/livro/livro";
import { Page } from "../../models/livro/pagina";
import { OcorrenciaView } from "../../models/livro/ocorrencia-view";

@Injectable()
export class OcorrenciaService {

    constructor(private router: Router, private http: Http, private authService: AuthService,
                private endPointService: OcorrenciaEndpointService) {
    }

    newOcorrencia(ocorrenciaObj: any, turnoId: string) {
        return this.endPointService.
             novaOcorrenciaEndpoint(turnoId, "", "").map(
                (response: Response) => response.json() as Ocorrencia,
             );
    }

    novaOcorrencia(livroId: string, paginaId: string, ocorrenciaObj: any) {
        return this.endPointService.
             novaOcorrenciaEndpoint(livroId, paginaId, ocorrenciaObj).map(
                (response: Response) => response.json() as any,
             );
    }
    getOcorrencias(livroId: string, paginaId: string) {
        return this.endPointService.
          getOcorrenciasByTurnoEndpoint(livroId, paginaId).map(
           (response: Response) => response.json() as any [],
        );
    }
    getLivrosBySetorId(setorId: string) {
        return this.endPointService.
        getLivrosBySetorIdEndPoint(setorId).map(
         (response: Response) => response.json() as Livro [],
      );
    }

    getLastOpenPageBySetorId(setorId: string) {
        return this.endPointService.
        getLastOpenPageBySetorIdEndPoint(setorId).map(
         (response: Response) => response.json() as Livro [],
      );
    }
    getDetalhes(id: string, idDoc: string) {
        return this.endPointService.
        getDetalhesEndpoint(id, idDoc ).map(
         (response: Response) => response.json() as OcorrenciaView,
      );
    }

    addArquivoEmOcorrencia(livroId: string, paginaId: string, ocorrenciaId: number, arquivos: any) {
        return this.endPointService.
        addArquivoEmOcorrenciaEndpoint(livroId, paginaId, ocorrenciaId, arquivos).map(
         (response: Response) => response.json() as any,
      );
    }

    getPagina(paginaId: string) {
        return this.endPointService.
          getPaginaEndpoint(paginaId).map(
           (response: Response) => response.json() as Page,
        );
    }
}
