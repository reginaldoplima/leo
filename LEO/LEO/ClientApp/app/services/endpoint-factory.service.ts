// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
//
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================
import { Injectable, Injector } from "@angular/core";
import { Http, Headers, RequestOptions, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/catch";

import { AuthService } from "./auth.service";
import { ConfigurationService } from "./configuration.service";
import { Utilities } from "./utilities";

@Injectable()
export class EndpointFactory {

    static readonly apiVersion: string = "1";

    // tslint:disable:variable-name
    private readonly _loginUrl: string = "/connect/token";
    private readonly _cadastroUrl: string = "/api/account/admin";
    private readonly _confirmaCadastro: string = "/api/account/confirm";
    private readonly _confirmaDados: string = "/api/account/users";

    private get loginUrl() { return this.configurations.baseUrl + this._loginUrl; }
    private get cadastroUrl() { return this.configurations.baseUrl + this._cadastroUrl; }
    private get confirmaUrl() { return this.configurations.baseUrl + this._confirmaCadastro; }
    private get confirmaDadosUrl() { return this.configurations.baseUrl + this._confirmaDados; }

    private taskPauser: Subject<any>;
    private isRefreshingLogin: boolean;

    private _authService: AuthService;

    private get authService() {
        if (!this._authService) {
          this._authService = this.injector.get(AuthService);
        }

        return this._authService;
    }

    constructor(protected http: Http, protected configurations: ConfigurationService,
                private injector: Injector) {

    }

    getLoginEndpoint(userName: string, password: string): Observable<Response> {

        const header = new Headers();
        header.append("Content-Type", "application/x-www-form-urlencoded");

        const searchParams = new URLSearchParams();
        searchParams.append("username", userName);
        searchParams.append("password", password);
        searchParams.append("grant_type", "password");
        searchParams.append("scope", "openid email phone profile offline_access roles");
        searchParams.append("resource", window.location.origin);

        const requestBody = searchParams.toString();

        return this.http.post(this.loginUrl, requestBody, { headers: header });
    }

    getCadastroEndpoint(user: any): Observable<Response> {
                const header = new Headers();
                header.append("Content-Type", "application/json");
                return this.http.post(this.cadastroUrl, user, { headers: header });
    }

    getCadastroDadosEndpoint(user: any, userId: string): Observable<Response> {

                const header = new Headers();
                header.append("Content-Type", "application/json");
                const retVal = Utilities.getPropertiesClass(user);
                // console.log(retVal)
                return this.http.patch(this._confirmaDados + `/${userId}/confirm`,
                   JSON.stringify(retVal), { headers: header });
    }

    getConfirmaEndpoint(token: string): Observable<Response> {

        const header = new Headers();
        header.append("Content-Type", "application/json");
        return this.http.get(this.confirmaUrl + `/${token}`, { headers: header });
    }

    getRefreshLoginEndpoint(): Observable<Response> {

        const header = new Headers();
        header.append("Content-Type", "application/x-www-form-urlencoded");

        const searchParams = new URLSearchParams();
        searchParams.append("refresh_token", this.authService.refreshToken);
        searchParams.append("grant_type", "refresh_token");
        searchParams.append("scope", "openid email phone profile offline_access roles");

        const requestBody = searchParams.toString();

        return this.http.post(this.loginUrl, requestBody, { headers: header })
            .map((response: Response) => {
                return response;
            })
            .catch((error) => {
                return this.handleError(error, () => this.getRefreshLoginEndpoint());
            });
    }

    protected getAuthHeader(includeJsonContentType?: boolean): RequestOptions {
        // tslint:disable-next-line:object-literal-key-quotes
        const headers = new Headers({ "Authorization": "Bearer " + this.authService.accessToken });

        if (includeJsonContentType) {
            headers.append("Content-Type", "application/json");
        }

        // tslint:disable-next-line:max-line-length
        headers.append("Accept", `application/vnd.iman.v${EndpointFactory.apiVersion}+json, application/json, text/plain, */*`);
        headers.append("App-Version", ConfigurationService.appVersion);

        return new RequestOptions({ headers });
    }

    protected handleError(error, continuation: () => Observable<any>) {

        if (error.status === 401) {
            if (this.isRefreshingLogin) {
                return this.pauseTask(continuation);
            }

            this.isRefreshingLogin = true;

            return this.authService.refreshLogin()
                .mergeMap((data) => {
                    this.isRefreshingLogin = false;
                    this.resumeTasks(true);

                    return continuation();
                })
                .catch((refreshLoginError) => {
                    this.isRefreshingLogin = false;
                    this.resumeTasks(false);

                    // tslint:disable-next-line:max-line-length
                    if (refreshLoginError.status === 401 || (refreshLoginError.url && refreshLoginError.url.toLowerCase().includes(this.loginUrl.toLowerCase()))) {
                        this.authService.reLogin();
                        return Observable.throw("session expired");
                    } else {
                        return Observable.throw(refreshLoginError || "server error");
                    }
                });
        }

        if (error.url && error.url.toLowerCase().includes(this.loginUrl.toLowerCase())) {
            this.authService.reLogin();
            return Observable.throw("session expired");
        } else {
            return Observable.throw(error || "server error");
        }
    }

    private pauseTask(continuation: () => Observable<any>) {
        if (!this.taskPauser) {
            this.taskPauser = new Subject();
        }

        return this.taskPauser.switchMap((continueOp) => {
            return continueOp ? continuation() : Observable.throw("session expired");
        });
    }

    private resumeTasks(continueOp: boolean) {
        setTimeout(() => {
            if (this.taskPauser) {
                this.taskPauser.next(continueOp);
                this.taskPauser.complete();
                this.taskPauser = null;
            }
        });
    }
}
