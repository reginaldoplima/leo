﻿// ======================================
// Author: Ebenezer Monney
// Email:  info@ebenmonney.com
// Copyright (c) 2017 www.ebenmonney.com
//
// ==> Gun4Hire: contact@ebenmonney.com
// ======================================

import { Directive, ElementRef, Input, Output, EventEmitter, OnInit, OnDestroy } 
   from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";
import "rxjs/add/observable/fromEvent";

import * as $ from "jquery";
import "bootstrap-datepicker/dist/js/bootstrap-datepicker";

@Directive({
    selector: "[bootstrapDatepicker]",
    exportAs: "bootstrap-datepicker",
})
export class BootstrapDatepickerDirective implements OnInit, OnDestroy {

    // tslint:disable-next-line:variable-name
    private _isShown = false;
    private updateTimeout;
    private changedSubscription: Subscription;
    private shownSubscription: Subscription;
    private hiddenSubscription: Subscription;

    get isShown() {
        return this._isShown;
    }

    @Input()
    options = {};

    @Input()
    set ngModel(value) {
        this.tryUpdate(value);
    }

    @Output()
    ngModelChange = new EventEmitter();

    constructor(private el: ElementRef) {
        // tslint:disable:max-line-length
        this.changedSubscription = Observable.fromEvent($(this.el.nativeElement) as any, "change")
          .subscribe((e: any) => setTimeout(() => this.ngModelChange.emit(e.target.value)));
        this.shownSubscription = Observable.fromEvent($(this.el.nativeElement) as any, "show")
          .subscribe((e: any) => this._isShown = true);
        this.hiddenSubscription = Observable.fromEvent($(this.el.nativeElement) as any, "hide")
          .subscribe((e: any) => this._isShown = false);
    }

    ngOnInit() {
        this.initialize(this.options);
    }

    ngOnDestroy() {
        this.destroy();
    }

    initialize(options?: any) {
        ($(this.el.nativeElement) as any).datepicker(options);
    }

    destroy() {
        if (this.changedSubscription) {
            this.changedSubscription.unsubscribe();
            this.shownSubscription.unsubscribe();
            this.hiddenSubscription.unsubscribe();
        }

        ($(this.el.nativeElement) as any).datepicker("destroy");
    }

    show() {
        ($(this.el.nativeElement) as any).datepicker("show");
    }

    hide() {
        ($(this.el.nativeElement) as any).datepicker("hide");
    }

    toggle() {
        this.isShown ? this.hide() : this.show();
    }

    private tryUpdate(value) {

        clearTimeout(this.updateTimeout);

        if (!$(this.el.nativeElement).is(":focus")) {
            this.update(value);
        } else {
            this.updateTimeout = setTimeout(() => {
                this.updateTimeout = null;
                this.tryUpdate(value);
            }, 100);
        }
    }

    update(value) {
        setTimeout(() => ($(this.el.nativeElement) as any).datepicker("update", value));
    }

    setDate(value) {
        setTimeout(() => ($(this.el.nativeElement) as any).datepicker("setDate", value));
    }

    setUTCDate(value) {
        setTimeout(() => ($(this.el.nativeElement) as any).datepicker("setUTCDate", value));
    }

    clearDates() {
        setTimeout(() => ($(this.el.nativeElement) as any).datepicker("clearDates"));
    }

    getDate() {
        ($(this.el.nativeElement) as any).datepicker("getDate");
    }

    getUTCDate() {
        ($(this.el.nativeElement) as any).datepicker("getUTCDate");
    }
}
