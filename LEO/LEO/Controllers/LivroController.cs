﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DAL;
using LEO.ViewModels;
using DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Models.LivroEletronico;
using System;
using LEO.Gerenciador;
using LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG;
using Newtonsoft.Json;
using System.Dynamic;
using DAL.Core.Interfaces;
using LEO.Helpers;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;

namespace LEO.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/livro")]
    public class LivroController : Controller
    {
        readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGerenciadorOcorrencias _gerenciadorOcorrencias;
        private readonly IAccountManager _accountManager;
        public LivroController(IUnitOfWork unitOfWork, 
            IGerenciadorOcorrencias gerenciadorOcorrencias,
            IAccountManager accountManager
            )
        {
            _unitOfWork = unitOfWork;
            _accountManager = accountManager;
            _gerenciadorOcorrencias = gerenciadorOcorrencias;
        }

        [HttpPost("{id}/pagina/{paginaId}")]
        public async Task<ActionResult> AdicionarOcorrencia(string id, string paginaId,
            [FromBody] ExpandoObject ocorrencia)
        {
            var tabelaOcorrencia = _unitOfWork.Ocorrencias
                .Find(o => o.LivroEntityId.Equals(id)).First();
            JObject ocorrenciaRet = _gerenciadorOcorrencias.AddOcorrencia(tabelaOcorrencia.Id, ocorrencia) as JObject;
            if (((IDictionary<String, object>)ocorrencia).ContainsKey("notificar"))
            {

                ((IDictionary<String, object>)ocorrencia).TryGetValue("notificar", out object result);
                bool notificar = (bool)result;
                if (notificar)
                {
                    var livro = _unitOfWork.Livros.FindBy(s => s.Id.Equals(id), s => s.Setor).First();
                    var setorId = livro.SetorId;
                    var local = livro.Setor.Nome;
                    var user = await _accountManager.GetUserByIdAsync(_unitOfWork.GetCurrentId);
                    var userSup = await _accountManager.GetUserByIdAsync(user.CreatedBy);
                    var hoje = DateTime.Now;
                    string id_doc = ocorrenciaRet.GetValue("id").ToObject<string>();
                    // ((IDictionary<String, object>)ocorrenciaRet).TryGetValue("id", out object id_doc);
                    await EmailNotificacaoOcorrencia(userSup.Email,
                        hoje.ToShortDateString(), paginaId, livro.Id, local, id_doc );
                }

            }
            return Ok(ocorrenciaRet);
        }

        [HttpPost("{id}/pagina/{paginaId}/ocorrencias/{ocorrenciaId}")]
        [Consumes("application/json", "application/json-patch+json", "multipart/form-data")]
        public async Task<ActionResult> AddArquivoEmOcorrencia(string id, 
            string paginaId, int ocorrenciaId, ICollection<IFormFile> files)
        {
            var nomeBase = _unitOfWork.Ocorrencias
                .FindBy( o=> o.LivroEntityId.Equals(id) ).FirstOrDefault().NomeTabela;

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    var result = await _gerenciadorOcorrencias
                        .AddArquivoEmDocumento(formFile,
                        nomeBase , ocorrenciaId.ToString() , "arquivos");
                   
                }
            }
            return Ok("ok");
        }

        [HttpGet("setor/{setorId}")]
        [Produces(typeof(LivroAdminView))]
        public async Task<ActionResult> LivroPorSetorId(string setorId)
        {
            List<LivroAdminView> livrosView = new List<LivroAdminView>();
            List<LivroEntity> livros;
            livros = _unitOfWork.Livros
                .FindBy( l=> l.SetorId.Equals(setorId), l=> l.Setor, l => l.Paginas ).ToList();
            livros.ForEach( l=> {
                var paginas = _unitOfWork.Paginas
                     .FindBy(p => p.IsClosedPage() && p.LivroEntityId.Equals(l.Id) ,
                       p => p.Escala.ApplicationUser,
                       p => p.EscalaAnterior,
                       p => p.InformacoesAberturaPagina,
                       p => p.InformacoesFechamentoPagina,
                       p => p.Livro);
                var lv = _creatLivroView(l, paginas.ToList() );
                livrosView.Add(lv);
            });
            return Ok(livrosView);
        }

        [HttpGet("pagina/{paginaId}")]
        [Produces(typeof(PaginaView))]
        public async Task<ActionResult> GetPagina(string paginaId)
        {
            var livroReturn = new LivroAdminView();

            return Ok(livroReturn);
        }

        [HttpGet("{id}/pagina/{paginaId}/ocorrencias")]
        [Produces(typeof(List<Object>))]
        public async Task<ActionResult> GetOcorrencias(string id,string paginaId)
        {
            var tbOcorrenciasId = _unitOfWork.Ocorrencias
                .Find(o => o.LivroEntityId.Equals(id)).First().NomeTabela;
            var ocorrenciasDaPagina = _gerenciadorOcorrencias
                .GetOcorrenciasByPagina(tbOcorrenciasId, paginaId);
            return Ok(ocorrenciasDaPagina);
        }

        [HttpGet("{id}/pagina/{paginaId}/ocorrencias/{ocorrenciaId}")]
        [Produces(typeof(Object))]
        public async Task<ActionResult> GetOcorrencias(string id, string paginaId, string ocorrenciaId)
        {
            var retVal = new Object();
            var tbOcorrenciasId = _unitOfWork.Ocorrencias
                        .Find(o => o.LivroEntityId.Equals(id)).First().Id;
            var ocorrenciasDaPagina = _gerenciadorOcorrencias
                .GetOcorrenciasByPagina(tbOcorrenciasId, paginaId);

            ocorrenciasDaPagina.ForEach( o=> {
                JObject jObject = o as JObject;
                var jTokenId = jObject.GetValue("id").ToObject<string>();
                if(jTokenId.Equals(ocorrenciaId))
                {
                    retVal = o;
                }

            });
            return Ok(retVal);
        }

        [HttpGet("setor/{setorId}/paginas/vazias")]
        [Produces(typeof(List<LivroAdminView>))]
        public async Task<ActionResult> PaginasVazias(string setorId)
        {
            var livrosReturn = new List<LivroAdminView>();
            List<LivroEntity> livros;
            livros = _unitOfWork.Livros
                .FindBy(l => l.SetorId.Equals(setorId), l => l.Setor, l => l. Paginas ).ToList();
            
            livros.ForEach(l => {
                var paginas = _unitOfWork.Paginas
                     .FindBy(p => !p.IsClosedPage() && p.LivroEntityId.Equals(l.Id),
                       p => p.Escala.ApplicationUser, 
                       p => p.EscalaAnterior, 
                       p => p.InformacoesAberturaPagina,
                       p => p.InformacoesFechamentoPagina,
                       p => p.Livro);
                var lv = _creatLivroView(l, paginas.ToList() );
                livrosReturn.Add(lv);
            });

            return Ok(livrosReturn);
        }


        [HttpGet("{id}/{id_doc}/detalhes")]
        public async Task<ActionResult> DetalhesDaOcorrencia(string id, string id_doc)
        {
            List<string> links = new List<string>();
            OcorrenciaDetalheView detalheView = new OcorrenciaDetalheView();

            var nomeDoLivro = _unitOfWork
                .Ocorrencias.FindBy( to=> to.LivroEntityId.Equals(id) ).First().NomeTabela;
            var arquivos = await  _gerenciadorOcorrencias.GetFilesId(nomeDoLivro, id_doc);
            string observacao = await _gerenciadorOcorrencias
                .GetField(nomeDoLivro, id_doc, "observacao");
            var url = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            arquivos?.ForEach( strUrl=> {
                var retVal = $"{url}/api/livro/{nomeDoLivro}/{strUrl}/download";
                links.Add(retVal);
            });
            detalheView.Observacao = observacao;
            detalheView.UrlArquivos = links;
            return Ok(detalheView);
        }

        [HttpGet("{nomeLivro}/{id_file}/download")]
        public async Task<IActionResult> DownLoadArquivo(string nomeLivro, string id_file)
        {

            var lbgHttpResponse =  await _gerenciadorOcorrencias.DownloadFile(nomeLivro, id_file);
            Stream stream = await lbgHttpResponse.Content.ReadAsStreamAsync();
            var response = File(stream , "image/png");
            return response;
        }

        private LivroAdminView _creatLivroView(LivroEntity livroEntityResult, List<PaginaEntity> paginas)
        {
             var livroReturn = new LivroAdminView();
             var listPaginasView = new List<PaginaView>();

             var tabela =_unitOfWork.Ocorrencias
                .Find(o => o.LivroEntityId.Equals(livroEntityResult.Id)).First();
             livroReturn.Local = _unitOfWork.Locais.Get(livroEntityResult.Setor.LocalEntityId).Descricao;
             livroReturn.Criador = "Admin";
             livroReturn.SetorId = livroEntityResult.SetorId;
             livroReturn.Id = livroEntityResult.Id;
             livroReturn.DataCriacao = livroEntityResult.DataCriacao;
             livroReturn.Setor = livroEntityResult.Setor.Nome;
             livroReturn.Nome = tabela.Descricao;
             livroReturn.TabelaOcorrenciasId = tabela.Id;

             var configTable = _unitOfWork
                .ConfiguracoesDeTabelasDeOcorrencias
                .Find(c => c.Id.Equals(tabela.OcorrenciasViewConfigurationId) )
                .First();
              List<PropertyConfig> properties = JsonConvert
                .DeserializeObject<List<PropertyConfig>>(configTable.JsonConfigString);
            livroReturn.ConfigView = new ConfigTdo
            {
                ConfigProperties = properties
            };
             listPaginasView = _criarPaginas(paginas);
             livroReturn.Paginas = listPaginasView;
            return livroReturn;
        }

        private List<PaginaView> _criarPaginas(List<PaginaEntity> paginas)
        {
            var listPaginasView = new List<PaginaView>();
            foreach (var pag in paginas)
            {
                var criador = "";
                var recebidoDe = "";
                var dataAbertura = pag.InformacoesAberturaPagina.DataAbertura;
                var encerammento = pag.InformacoesFechamentoPagina != null
                         ? pag.InformacoesFechamentoPagina.DataFechamento : DateTime.Now;
                var observacaoFechamento = pag?.InformacoesFechamentoPagina?.Obeservacoes;
                var observacaoAbertura = pag?.InformacoesAberturaPagina?.Obeservacoes;

                var retVal = new PaginaView
                {
                    Id = pag.Id,
                    Criador = criador,
                    RecebidoDe = recebidoDe,
                    Inicio = dataAbertura , 
                    Fim = encerammento,
                    ObservacoesAbertura = observacaoAbertura,
                    ObservacoesFechamento = observacaoFechamento

                };

                listPaginasView.Add(retVal);
            }

            return listPaginasView;
        }

        private async Task<string> EmailNotificacaoOcorrencia(string email,string data ,
            string paginaId, string livroId, string local, string id_doc)
        {
            string recepientName = "Email de confirmação de cadastro."; //         <===== Put the recepient's name here
            string recepientEmail = email; //   <===== Put the recepient's email here
            string message = EmailTemplates.GetNotificacaoOcorrencia(data, paginaId , livroId, id_doc);

            (bool success, string errorMsg) response = await EmailSender.SendEmailAsync(recepientName, recepientEmail,
                "Notificacao Ocorrencia: " + local, message);

            if (response.success)
                return "Success";

            return "Error: " + response.errorMsg;
        }
    }
}