﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DAL;
using System.Linq;
using DAL.Models.LivroEletronico;
using LEO.Auth.DAL.Models.LivroEletronico;
using LEO.ViewModels;
using System;
using LEO.Gerenciador;

namespace LEO.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/local")]
    public class LocalController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGerenciadorOcorrencias _gerenciadorOcorrencias;

        public LocalController(IUnitOfWork uow , IGerenciadorOcorrencias gerenciadorOcorrencias)
        {
            _unitOfWork = uow;
            _gerenciadorOcorrencias = gerenciadorOcorrencias;
        }

        [HttpPost]
        [Produces(typeof(LocalEntity))]
        public async Task<ActionResult> CriarLocal([FromBody] LocalEntity local)
        {
            _unitOfWork.Locais.Add(local);
            _unitOfWork.SaveChanges();
            return Ok(local);
        }

        [HttpGet]
        [Produces(typeof(List<LocalEntity>))]
        public async Task<ActionResult> GetAll()
        {
            var meusLocais = _unitOfWork.Locais.MeusLocais(); 
            return Ok(meusLocais);
        }

        [HttpPost("{locaid}/setores")]
        [Produces(typeof(SetorAddView))]
        public async Task<ActionResult> AddSetor(string locaid, [FromBody] SetorEntity setor)
        {
            var novoSetor = new SetorEntity { LocalEntityId = locaid, Nome = setor.Nome };
            _unitOfWork.Setores.Add(novoSetor);
            var livro = new LivroEntity
            {
                DataCriacao = DateTime.UtcNow,
                SetorId = novoSetor.Id,
            };
            var livro2 = new LivroEntity
            {
                DataCriacao = DateTime.UtcNow,
                SetorId = novoSetor.Id,
            };
            var livro3 = new LivroEntity
            {
                DataCriacao = DateTime.UtcNow,
                SetorId = novoSetor.Id,
            };
            _unitOfWork.Livros.Add(livro);
            _unitOfWork.Livros.Add(livro2);
            _unitOfWork.Livros.Add(livro3);

            var tabelaEntrada = new OcorrenciaLBG
            {
                Descricao = "Entrada Veiculos",
                NomeTabela = livro.Id + "_entrada_veiculos",
                IsComunicacaoObrigatoria = false,
                LivroEntityId = livro.Id,
                OcorrenciasViewConfigurationId =
                  _unitOfWork.ConfiguracoesDeTabelasDeOcorrencias
                  .Find(c => c.Name.Equals("EntradaVeiculos")).FirstOrDefault().Id
            };

            var tabelasaida = new OcorrenciaLBG
            {
                Descricao = "Saida Veiculos",
                NomeTabela = livro3.Id + "_saida_veiculos",
                IsComunicacaoObrigatoria = false,
                LivroEntityId = livro3.Id,
                OcorrenciasViewConfigurationId =
                    _unitOfWork.ConfiguracoesDeTabelasDeOcorrencias
                            .Find(c => c.Name.Equals("SaidaVeiculos")).FirstOrDefault().Id
            };

            var tabelaAnotacoes = new OcorrenciaLBG
            {
                Descricao = "Anotações",
                NomeTabela = livro2.Id+"_anotacoes",
                IsComunicacaoObrigatoria = false,
                LivroEntityId = livro2.Id,
                OcorrenciasViewConfigurationId =
                  _unitOfWork.ConfiguracoesDeTabelasDeOcorrencias
                  .Find(c => c.Name.Equals("Anotacoes")).FirstOrDefault().Id
            };

            var tabelasOcorrencias = new List<OcorrenciaLBG>
            {
                tabelaEntrada, tabelaAnotacoes, tabelasaida
            };

            _unitOfWork.Ocorrencias.AddRange(tabelasOcorrencias);
            tabelasOcorrencias.ForEach( to => {
                _gerenciadorOcorrencias.AddTabela(to);
            });
            _unitOfWork.SaveChanges();
            var setorReturn = new SetorAddView
            {
                Id = novoSetor.Id,
                Nome = novoSetor.Nome,
                LocalEntityId = novoSetor.LocalEntityId,
                
            };
            return Ok(setorReturn);
        }

        [HttpGet("{id}/setores")]
        [Produces(typeof(List<SetorEntity>))]
        public async Task<ActionResult> GetByIdSetores(string id)
        {
            var retVal = _unitOfWork.Setores.GetAll()
                .Where(s => s.LocalEntityId.Equals(id)).ToList();
            return Ok(retVal);
        }

        [HttpGet("{id}/setores/{setorId}/escalas")]
        [Produces(typeof(List<EscalaEntity>))]
        public async Task<ActionResult> GetEscalas(string id,string setorId)
        {
            List<EscalaEntity> escalas = new List<EscalaEntity>();
            escalas = _unitOfWork.Escalas.
               AllEscalasWithUserBySetorId(setorId).ToList();
            return Ok(escalas);
        }

        [HttpPost("{id}/setores/{setorId}/escalas")]
        [Produces(typeof(EscalaFuncionarioView))]
        public async Task<ActionResult> AddEscala(string id, string setorId, [FromBody] EscalaEntity escala)
        {
             var setor = _unitOfWork.Setores.Get(setorId);
            if (setor.Escalas != null)
                setor.Escalas.Add(escala);
            else
            {
                setor.Escalas = new List<EscalaEntity> { escala };
            }
            _unitOfWork.SaveChanges();
            var userEscalado = _unitOfWork.Escalas.FindBy( e => escala.Id.Equals(e.Id), 
                e => e.ApplicationUser ).First();
            var escalaView = new EscalaFuncionarioView
            {
                ApplicationUserId = userEscalado.ApplicationUserId,
                Inicio = userEscalado.Inicio.ToUniversalTime(),
                Fim = userEscalado.Fim.ToUniversalTime(),
                Id = userEscalado.Id,
                Tags = userEscalado.Tags,
                NomeSetor = userEscalado.Setor.Nome,
                ApplicationUser = new FuncionarioView
                {
                    ApplicationUserId = userEscalado.ApplicationUserId,
                    Matricula = userEscalado.ApplicationUser.UserName,
                    Nome = userEscalado.ApplicationUser.Nome,
                }
            };
            return Ok(escalaView);
        }
    }
}