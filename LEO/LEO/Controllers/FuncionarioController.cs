﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DAL;
using Microsoft.Extensions.Logging;
using LEO.Auth.DAL.Models.LivroEletronico;
using DAL.Models.LivroEletronico;
using LEO.ViewModels;
using LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG;
using Newtonsoft.Json;

namespace LEO.Controllers
{
    [Produces("application/json")]
    [Route("api/funcionario")]
    public class FuncionarioController : Controller
    {
        private IUnitOfWork _unitOfWork;
        readonly ILogger _logger;

        public FuncionarioController(IUnitOfWork unitOfWork, 
            ILogger<FuncionarioController> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        [HttpGet("escala/{escalaId}/ultimaPaginaFechada")]
        public async Task<ActionResult> UltimaPaginaFechada(string escalaId)
        {
            var livrosReturn = new List<LivroAdminView>();
            List<LivroEntity> livros;
            var setorId = _unitOfWork.Escalas.Get(escalaId).SetorEntityId;
            livros = _unitOfWork.Livros.FindBy( l=> l.SetorId.Equals(setorId) && !String.IsNullOrEmpty(l.PaginaAnteriorId) 
               , l=> l.Setor).ToList();
            livros.ForEach(l => {
                var paginas = _unitOfWork.Paginas
                     .FindBy(p => p.IsClosedPage() && p.Id.Equals(l.PaginaAnteriorId),
                       p => p.Escala.ApplicationUser,
                       p => p.EscalaAnterior,
                       p => p.InformacoesAberturaPagina,
                       p => p.InformacoesFechamentoPagina,
                       p => p.Livro);
                var lv = _creatLivroView(l, paginas.ToList());
                livrosReturn.Add(lv);
            });
            return Ok(livrosReturn);
        }

        [HttpPost("escala/{id}/abrir")]
        public async Task<ActionResult> AbrirEscala(string id, [FromBody] AberturaEscalaView abertura)
        {
            var escala = _unitOfWork.Escalas.Get(id);
            var paginas = new List<PaginaEntity>();
            var escalaAnterior = _unitOfWork.Escalas.Get(abertura.IdEscalaAnterior);
            var livros = _unitOfWork.Livros
                .Find(l => l.SetorId.Equals(escala.SetorEntityId)).ToList();

            livros.ForEach( l =>
                {
                    PaginaEntity paginaAbertura = new PaginaEntity
                    {
                        InformacoesAberturaPagina = new InformacoesAberturaPagina
                        {
                            DataAbertura = abertura.DataAbertura,
                            IdUsuarioAnterior = escalaAnterior.ApplicationUserId,
                            Obeservacoes = abertura.Observacoes

                        },
                        EscalaEntityId = escala.Id,
                        EscalaAnteriorId = escalaAnterior.Id,
                        LivroEntityId = l.Id,
                        Numero = String.IsNullOrEmpty(l.PaginaAnteriorId) ?
                                   1 : _unitOfWork.Paginas.Get(l.PaginaAnteriorId).Numero + 1 

                    };
                    paginas.Add(paginaAbertura);
                }
             );
            escala.IsOpen = true;
            _unitOfWork.Escalas.Update(escala);
            _unitOfWork.Paginas.AddRange(paginas);
            _unitOfWork.SaveChanges();
            
            return Ok("ok");
        }

        [HttpPost("escala/{id}/fechar")]
        public async Task<ActionResult> FecharEscala(string id, [FromBody] FecharEscalaView fechamento)
        {
            var escala = _unitOfWork.Escalas.Get(id);
            var paginas = _unitOfWork.Paginas
                .FindBy(p => fechamento.Paginas.Contains(p.Id), p=> p.Livro ).ToList();

            paginas.ForEach( p=> {
                p.InformacoesFechamentoPagina = new InformacoesFechamentoPaginaEntity
                {
                    DataFechamento = DateTime.UtcNow,
                    Obeservacoes = fechamento.Observacoes,
                };
                p.Escala = escala;
                p.Livro.PaginaAnteriorId = p.Id;
            });
            _unitOfWork.Paginas.UpdateRange(paginas);
            escala.IsOpen = false;
            _unitOfWork.SaveChanges();

            return Ok("ok");
        }

        [HttpGet("escala")]
        [Produces(typeof(List<EscalaFuncionarioView>))]
        public async Task<ActionResult> MeusSetores()
        {
            List<EscalaFuncionarioView> escalas = new List<EscalaFuncionarioView>();
            var minhaEscala = _unitOfWork.Escalas
                .AllEscalasByUserId(_unitOfWork.GetCurrentId);
            foreach (var esc in minhaEscala)
            {
                var escalaView = new EscalaFuncionarioView
                {
                    ApplicationUserId = esc.ApplicationUserId,
                    Inicio = esc.Inicio.ToUniversalTime(),
                    Fim = esc.Fim.ToUniversalTime(),
                    Id = esc.Id,
                    Tags = esc.Tags,
                    NomeSetor = esc.Setor.Nome,
                    Logradouro = esc.Setor.Local.Endereco.Logradouro,
                    Bairro = esc.Setor.Local.Endereco.Bairro,
                    Numero = esc.Setor.Local.Endereco.Numero,
                    IsOpen = esc.IsOpen,
                    SetorId = esc.SetorEntityId,
                };

                escalas.Add(escalaView);
            }
            return Ok(escalas);
        }

        private LivroAdminView _creatLivroView(LivroEntity livroEntityResult, List<PaginaEntity> paginas)
        {
            var livroReturn = new LivroAdminView();
            var listPaginasView = new List<PaginaView>();

            var tabela = _unitOfWork.Ocorrencias
               .Find(o => o.LivroEntityId.Equals(livroEntityResult.Id)).First();
            livroReturn.Local = _unitOfWork.Locais.Get(livroEntityResult.Setor.LocalEntityId).Descricao;
            livroReturn.Criador = "Admin";
            livroReturn.SetorId = livroEntityResult.SetorId;
            livroReturn.Id = livroEntityResult.Id;
            livroReturn.DataCriacao = livroEntityResult.DataCriacao;
            livroReturn.Setor = livroEntityResult.Setor.Nome;
            livroReturn.Nome = tabela.Descricao;
            livroReturn.TabelaOcorrenciasId = tabela.Id;

            var configTable = _unitOfWork
               .ConfiguracoesDeTabelasDeOcorrencias
               .Find(c => c.Id.Equals(tabela.OcorrenciasViewConfigurationId))
               .First();
            List<PropertyConfig> properties = JsonConvert
              .DeserializeObject<List<PropertyConfig>>(configTable.JsonConfigString);
            livroReturn.ConfigView = new ConfigTdo
            {
                ConfigProperties = properties
            };
            listPaginasView = _criarPaginas(paginas);
            livroReturn.Paginas = listPaginasView;
            return livroReturn;
        }

        private List<PaginaView> _criarPaginas(List<PaginaEntity> paginas)
        {
            var listPaginasView = new List<PaginaView>();
            foreach (var pag in paginas)
            {
                var criador = "";
                var recebidoDe = "";
                var dataAbertura = pag.InformacoesAberturaPagina.DataAbertura;
                var encerammento = pag.InformacoesFechamentoPagina != null
                         ? pag.InformacoesFechamentoPagina.DataFechamento : DateTime.Now;

                var retVal = new PaginaView
                {
                    Id = pag.Id,
                    Criador = criador,
                    RecebidoDe = recebidoDe,
                    Inicio = dataAbertura,
                    Fim = encerammento,
                };

                listPaginasView.Add(retVal);
            }

            return listPaginasView;
        }
    }
}