﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEO.ViewModels
{
    public class AberturaEscalaView
    {
        public string IdEscalaAnterior { get; set; }
        public DateTime DataAbertura { get; set; }
        public string Observacoes { get; set; }
    }
}
