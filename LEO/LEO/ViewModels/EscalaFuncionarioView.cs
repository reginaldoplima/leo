﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEO.ViewModels
{
    public class EscalaFuncionarioView
    {
        public string Id { get; set; }
        public string ApplicationUserId { get; set; }
        public string Tags { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public string NomeSetor { get; set; }
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string Numero { get; set; }
        public FuncionarioView ApplicationUser { get; set; }
        public bool IsOpen { get; set; }
        public string SetorId { get; set; }
    }
}
