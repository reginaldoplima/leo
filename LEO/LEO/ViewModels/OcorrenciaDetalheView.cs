﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEO.ViewModels
{
    public class OcorrenciaDetalheView
    {
        public string Observacao { get; set; }
        public List<string> UrlArquivos { get; set; }
    }
}
