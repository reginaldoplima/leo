﻿using LEO.Auth.DAL.Models.LivroEletronico;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEO.ViewModels
{
    public class EntradaVeiculosView : OcorrenciaBase
    {

        public string Placa { get; set; }
        public HorarioFormat Entrada { get; set; }
        public HorarioFormat Saida { get; set; }
    }

    public class HorarioFormat
    {
        public string Data { get; set; }
        public string Hora { get; set; }
    }
}
