﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEO.ViewModels
{
    public class SetorAddView
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string LocalEntityId { get; set; }
    }
}
