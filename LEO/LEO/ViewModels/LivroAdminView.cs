﻿using LEO.Auth.DAL.Models.LivroEletronico.OcorrenciasLBG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEO.ViewModels
{
    public class LivroAdminView
    {

        public string Id { get; set; }
        public string SetorId { get; set; }
        public string ApplicationUserId { get; set; }
        public string TabelaOcorrenciasId { get; set; }
        public string Nome { get; set; }
        public DateTime DataCriacao { get; set; }

        public string Local { get; set; }
        public string Setor { get; set; }
        public string Criador { get; set; }
        public ConfigTdo ConfigView { get; set; }
        public List<PaginaView> Paginas { get; set; }

    }

    public class PaginaView
    {
        public PaginaView()
        {
            Id = Guid.NewGuid().ToString();

        }
        public string Id { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public string RecebidoDe { get; set; }
        public string Criador { get; set; }
        public int NumeroOcorrencias { get; set; }
        public string ObservacoesAbertura { get; set; }
        public string ObservacoesFechamento { get; set; }
    }
}
