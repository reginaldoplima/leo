﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEO.ViewModels
{
    public class FuncionarioView
    {
        public string ApplicationUserId { get; set; }
        public string Nome { get; set; }
        public string Matricula { get; set; }
    }
}
