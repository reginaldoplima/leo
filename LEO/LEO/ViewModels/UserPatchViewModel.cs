﻿


namespace LEO.ViewModels
{
    public class UserPatchViewModel
    {
        public string Logradouro { get; set; }
        public string Localidade { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Uf { get; set; }

        public string NumeroDoDocumento { get; set; }

        public string Telefone { get; set; }
        public string Empresa { get; set; }

        public string Configuration { get; set; }
    }
}
