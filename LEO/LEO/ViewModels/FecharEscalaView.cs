﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEO.ViewModels
{
    public class FecharEscalaView
    {
        public string IdEscalaAnterior { get; set; }
        public DateTime DataFechamaneto { get; set; }
        public string[] Paginas { get; set; }
        public string Observacoes { get; set; }
    }
}
