﻿
using DAL.Core;
using LEO.Helpers;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LEO.Policies
{
    public class UserExpDateRequirement : IAuthorizationRequirement
    {

    }


    public class UserExpDateHandler : AuthorizationHandler<UserExpDateRequirement, string>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            UserExpDateRequirement requirement, string targetUserId)
        {
            if (context.User.HasClaim(CustomClaimTypes.Permission, 
                ApplicationPermissions.ViewUsers) || GetIsSameUser(context.User, targetUserId))
                context.Succeed(requirement);

            return Task.CompletedTask;
        }


        private bool GetIsSameUser(ClaimsPrincipal user, string targetUserId)
        {
            return Utilities.GetUserId(user) == targetUserId;
        }
    }
}
