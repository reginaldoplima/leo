﻿using Microsoft.Extensions.DependencyInjection;
using LEO.Gerenciador;
using Microsoft.AspNetCore.Hosting;

namespace LEO.LivroEletronico.Service
{
    public class IocLivro
    {
        public static void Register(IServiceCollection services, string lbgUrl)
        {
            LightBaseNeoLib.Utils.Ioc.Register(services, lbgUrl);
            services.AddScoped<IGerenciadorOcorrencias ,GeranciadorLivros>();
        }


        public static void Init(IHostingEnvironment env)
        {
            Helpers.TemplatesLivros.Initialize(env);
        }
    }
}
