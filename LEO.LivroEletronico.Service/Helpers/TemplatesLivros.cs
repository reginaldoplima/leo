﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LEO.LivroEletronico.Service.Helpers
{
    public class TemplatesLivros
    {
        static IHostingEnvironment _hostingEnvironment;
        static string testEmailTemplate;

        public static void Initialize(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public static string GetStringJsonTemplate(string livroId, TiposLivro tiposLivro)
        {
            var nomeLivro = tiposLivro.ToString("G");
            var caminhoArquivo = "Helpers/Templates/LBGJsonTables/" + nomeLivro + ".json";
            testEmailTemplate = ReadPhysicalFile(caminhoArquivo);
            string novoLivro = testEmailTemplate
                .Replace("$id_livro$", livroId);
            testEmailTemplate = null;
            return novoLivro;
        }

        private static string ReadPhysicalFile(string path)
        {
            if (_hostingEnvironment == null)
                throw new InvalidOperationException($"{nameof(TemplatesLivros)} is not initialized");

            IFileInfo fileInfo = _hostingEnvironment.ContentRootFileProvider.GetFileInfo(path);

            if (!fileInfo.Exists)
                throw new FileNotFoundException($"Template file located at \"{path}\" was not found. " +
                    $"Env content root \"{_hostingEnvironment.ContentRootPath}\"" +
                    $"file info \"{fileInfo.PhysicalPath}\"");

            using (var fs = fileInfo.CreateReadStream())
            {
                using (var sr = new StreamReader(fs))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }

    public enum TiposLivro
    {
        Anotacoes,
        EntradaVeiculos,
        SaidaVeiculos
    }
}
