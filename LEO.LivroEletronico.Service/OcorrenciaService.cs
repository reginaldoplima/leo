﻿using DAL;

using LEO.LivroEletronico.Domain;
using LEO.LivroEletronico.Interfaces.Repositories;
using LEO.LivroEletronico.Interfaces.Services;
using Microsoft.AspNetCore.Http;

namespace LEO.LivroEletronico.Service
{
    public class OcorrenciaService : LeoAppBase<Ocorrencia>,IOcorrenciaService
    {
        public OcorrenciaService(IOcorrenciaRepository ocorrenciaRepository , IHttpContextAccessor httpAccessor) 
            : base(ocorrenciaRepository, httpAccessor)
        {
        }
    }
}
