﻿

using DAL;
using LEO.LivroEletronico.Domain;
using LEO.LivroEletronico.Interfaces.Repositories;
using LEO.LivroEletronico.Interfaces.Services;
using Microsoft.AspNetCore.Http;

namespace LEO.LivroEletronico.Service
{
    public class PaginaService : LeoAppBase<Pagina>, IPaginaService
    {
        public PaginaService(IPaginaRepository livroRepository, IHttpContextAccessor httpAccessor) 
         : base(livroRepository, httpAccessor)
        {
        }
    }
}
