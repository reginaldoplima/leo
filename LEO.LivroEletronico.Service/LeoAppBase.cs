﻿using AspNet.Security.OpenIdConnect.Primitives;
using LEO.LivroEletronico.Domain;
using LEO.LivroEletronico.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Text;

namespace LEO.LivroEletronico.Service
{
    public class LeoAppBase<TEntity> : ILeoAppBase<TEntity> where TEntity : LeoBase
    {
        private readonly ILeoRepositoryBase<TEntity> _repository;
        private readonly IHttpContextAccessor _httpAccessor;

        public LeoAppBase(ILeoRepositoryBase<TEntity> leoRepository , IHttpContextAccessor httpAccessor)
        {
            _repository = leoRepository;
            _httpAccessor = httpAccessor;
        }

        public virtual TEntity Criar(TEntity entity)
        {
            return _repository.Insert(entity);
        }

        public bool Deletar(TEntity entity)
        {
            return _repository.Delete(entity);
        }

        public List<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public TEntity GetById(string id)
        {
            return _repository.GetById(id);
        }

        public bool Update(TEntity entity)
        {
            return _repository.Update(entity);
        }

        public string GetCurrentId
        {
            get { return _httpAccessor.HttpContext.User
                .FindFirst(OpenIdConnectConstants.Claims.Subject)?.Value?.Trim(); }
        }
    }
}
