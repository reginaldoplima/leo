﻿using System;
using System.Collections.Generic;
using DAL.Models.LivroEletronico;
using LEO.Gerenciador;
using DAL;
using Newtonsoft.Json.Linq;
using LightBaseNeoLib.Interfaces;
using LEO.LivroEletronico.Service.Helpers;
using Newtonsoft.Json;
using LightBaseNeoLib.Interfaces.Model;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace LEO.LivroEletronico.Service
{
    public class GeranciadorLivros : IGerenciadorOcorrencias
    {
        private static Dictionary<string, List<JObject>> 
            dictionary = new Dictionary<string, List<JObject>>();
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILightBaseNeoApp _lightBaseNeoApp;
        public GeranciadorLivros(IUnitOfWork unitOfWork, 
            ILightBaseNeoApp lightBaseNeoApp)
        {
            _unitOfWork = unitOfWork;
            _lightBaseNeoApp = lightBaseNeoApp;
        }

        public Object AddOcorrencia(string tabelaOcorrenciaId, Object ocorrencia)
        {
            JObject retVal = JObject.FromObject(ocorrencia);
            string ocorrenciaJsonString = JsonConvert.SerializeObject(retVal);
            var nomeTabelaLBG = _unitOfWork.Ocorrencias.Get(tabelaOcorrenciaId).NomeTabela;
            var id = _lightBaseNeoApp
                .InsertDocument(ocorrenciaJsonString , nomeTabelaLBG).Result;
            retVal["id"] = id;
            return retVal;
        }

        public OcorrenciaLBG AddTabela(OcorrenciaLBG tabelaOcorrencia)
        {
            var nomeTemplete = _unitOfWork
                .ConfiguracoesDeTabelasDeOcorrencias
                .Get(tabelaOcorrencia.OcorrenciasViewConfigurationId).Name;
            TiposLivro tiposLivro = (TiposLivro)Enum.Parse(typeof(TiposLivro), nomeTemplete);
            var tabela = TemplatesLivros
                .GetStringJsonTemplate(tabelaOcorrencia.LivroEntityId, tiposLivro);
            var result = _lightBaseNeoApp.CreateBase(tabela).Result;
            return tabelaOcorrencia;
        }

        public List<Object> GetOcorrenciasAllLivroId(string livroId, string tabela)
        {
            throw new NotImplementedException();
        }

        public List<Object> GetOcorrenciasByPagina(string tabela, string paginaId)
        {
            List<Object> ocorrenciasObj = new List<object>();

            var all = _lightBaseNeoApp
                .SelectDocumentAll<SelectDocumentResult<JObject>>(tabela, 10000).Result;
            var enumarator = all.Results.GetEnumerator();
            while (enumarator.MoveNext())
            {
                var retVal = enumarator.Current;
                var isEquals = retVal.GetValue("paginaid")
                    .ToObject<string>().Equals(paginaId);
                if(isEquals)
                {
                    retVal["id"] = retVal["_metadata"]["id_doc"];
                    ocorrenciasObj.Add(retVal);
                }
            }    

            return ocorrenciasObj;
        }

        public List<OcorrenciaLBG> GetTabelasOcorrenciaByLivroId(string livroId)
        {
            throw new NotImplementedException();
        }

        public async Task<string> AddArquivoEmDocumento(IFormFile ff, 
            string baseName,  string id_doc , string campo = "arquivos")
        {
            var contentStream = new StreamContent(ff.OpenReadStream());
            var fileName = id_doc;
            var fileInsertResult = await _lightBaseNeoApp
                .InsertFileIntoBase(baseName, contentStream, fileName);

            var resultadosAnteriores = await _lightBaseNeoApp
                .SelectDocumentByIdDoc<JArray>(id_doc: id_doc ,  baseName: baseName , field: campo );
            if (resultadosAnteriores != null)
            {
                resultadosAnteriores.Add(fileInsertResult.Uuid);
                var result = await _lightBaseNeoApp
                    .PatchDocument<JArray>(resultadosAnteriores, baseName, id_doc, campo);
            }
            else
            {
                string[] novoIdArquivo = new string[] { fileInsertResult.Id_File  }; 
                var result = await _lightBaseNeoApp
                    .PatchDocument<string[]>(novoIdArquivo, baseName, id_doc, campo);
            }
            contentStream.Dispose();

            return "";
        }

        public async Task<HttpResponseMessage> DownloadFile(string basePath, string id_file)
        {

            var lbgHttpResponse = await _lightBaseNeoApp.DownloadFile(basePath, id_file);
            return lbgHttpResponse;
        }

        public async Task<List<string>> GetFilesId(string basePath, string id_doc)
        {
            List<string> retVal = new List<string>();
            var result = await _lightBaseNeoApp
                            .SelectDocumentByIdDoc<JArray>(id_doc: id_doc, baseName: basePath, field: "arquivos");
            retVal = result?.ToObject<List<string>>();
            return retVal;
        }

        public async Task<string> GetField(string nomeDoLivro, string id_doc, string field)
        {
            var result = await _lightBaseNeoApp
                            .SelectDocumentByIdDoc<string>(id_doc: id_doc, baseName: nomeDoLivro, field: field);
            return result;
        }
    }
}
