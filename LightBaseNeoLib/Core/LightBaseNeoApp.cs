﻿using LightBaseNeoLib.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LightBaseNeoLib.Interfaces.Model;

namespace LightBaseNeoLib.Core
{
    public class LightBaseNeoApp : ILightBaseNeoApp
    {

        private ILightbaseNeoClient _lbgHttpClient { get; }
        private ISerializer _serializer { get; }
        public LightBaseNeoApp(ILightbaseNeoClient lbgHttpClient, ISerializer serializer)
        {
            _lbgHttpClient = lbgHttpClient;
            _serializer = serializer;
        }

        

        public Task<HttpResponseMessage> Command(ICommand _command)
        {
            throw new NotImplementedException();
        }

        public Task<HttpResponseMessage> CreateBase(NeoBase _base)
        {
            if (_base != null)
            {
                if (String.IsNullOrEmpty(_base.Metadata?.Name) ||
                    String.IsNullOrWhiteSpace(_base.Metadata?.Name)) throw new Exception("base nao contem nome");
                string jsonBase = _serializer.Serialize<NeoBase>(_base);
                List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("json_base", jsonBase)
                };
                return _lbgHttpClient.Post("", nameValueCollection);
            }
            else
            {
                throw new ArgumentNullException("base nao pode ser nula");
            }
            
        }

        public async Task<int> InsertDocument(IDocument _document, string _basePath)
        {
            
            if (_document != null)
            {
                var path = _basePath + "/doc";
                
                string jsonString = _serializer.Serialize<IDocument>(_document);
                List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("value", jsonString),
                    new KeyValuePair<string, string>("validate", "0")
                };
                var response = await _lbgHttpClient.Post(path, nameValueCollection);
                var json = "";
                if (response.IsSuccessStatusCode)
                {
                    Stream receiveStream = await response.Content.ReadAsStreamAsync();
                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    json = readStream.ReadToEnd();
                }
                return int.Parse(json);
            }
            else
            {
                throw new ArgumentNullException("_document nao pode ser nula");
            }
        }

        public async Task<int> InsertDocument(string jsonString, string _basePath)
        {

            if (jsonString != null)
            {
                var path = _basePath + "/doc";
                List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("value", jsonString),
                    new KeyValuePair<string, string>("validate", "0")
                };
                var response = await _lbgHttpClient.Post(path, nameValueCollection);
                var json = "";
                if (response.IsSuccessStatusCode)
                {
                    Stream receiveStream = await response.Content.ReadAsStreamAsync();
                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    json = readStream.ReadToEnd();
                }
                return int.Parse(json);
            }
            else
            {
                throw new ArgumentNullException("string nao pode ser nula");
            }
        }


        public Task<HttpResponseMessage> DeleteBase(string _basePath)
        {
            return  _lbgHttpClient.Delete(_basePath);
        }

        public Task<HttpResponseMessage> DeleteDocument(string _documentId, string _basePath)
        {
            var path = $"{_basePath}/doc/{_documentId}";
            return _lbgHttpClient.Delete(path);
        }

        public void Dispose()
        {
            _lbgHttpClient.Dispose();
        }

        public async Task<TEntity> GetDocument<TEntity>(string _documentId, string _basePath)
        {
            //get /{ base}/ doc /{ id}
            var path = _basePath + "/doc/" + _documentId;
            var response = await _lbgHttpClient.Get(path);
            string json = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                json = readStream.ReadToEnd();
            }
            TEntity retval =_serializer.Deserialize<TEntity>(json);

            return retval;
        }

        public Task<NeoBase> RecoverBase(string _basePath)
        {
            throw new NotImplementedException();
        }

        public async Task<HttpResponseMessage> PatchDocument<TDocument>(TDocument _document, 
            string _basePath, string _documentId)
        {
            var path = _basePath + "/doc/" + _documentId;

            string jsonString = _serializer.Serialize<TDocument>(_document);
            List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>
            {
                    new KeyValuePair<string, string>("value", jsonString),
                    new KeyValuePair<string, string>("validate", "0")
            };

            var response = await _lbgHttpClient.Patch(path, nameValueCollection);
            return response;
        }
        public async Task<HttpResponseMessage> PatchDocument<TDocument>(TDocument _document, 
            string _basePath, string _documentId, string field)
        {
            var path = _basePath + "/doc/" + _documentId;

            string jsonString = _serializer.Serialize<TDocument>(_document);
            string withField = $"{{\"{field}\": {jsonString} }}";
            List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>
            {
                    new KeyValuePair<string, string>("value", withField),
                    new KeyValuePair<string, string>("validate", "0")
            };

            var response = await _lbgHttpClient.Patch(path, nameValueCollection);
            return response;
        }

        public async Task<SelectBaseResult> SelectBases(string baseName)
        {
            var searchJsonCommand = $"{{\"commands\": [\"SELECT id_base, name FROM lb_base WHERE name LIKE '%{baseName}%';\"]}}";
            var response = await _lbgHttpClient.PostRawJson("sql", searchJsonCommand);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }
            List<SelectBaseResult> jsonBase = _serializer.Deserialize<List<SelectBaseResult>>(contentBody);
            if (jsonBase.Count >= 1)
            {
                return jsonBase[0];
            }

            return default(SelectBaseResult);
        }

        public async Task<TEntity> SelectDocument<TEntity>(SelectWithFilters selectString, string baseName)
        {
            var searchJsonCommand = _serializer.Serialize<SelectWithFilters>(selectString);
            string seachPath = $"{baseName}/doc?$$={searchJsonCommand}";
            var response = await _lbgHttpClient.Get(seachPath);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }
            var retVal = contentBody;
            return default(TEntity);
        }

        public async Task<TEntity> SelectDocumentByIdDoc<TEntity>(string id_doc , string baseName)
        {
            string seachPath = $"{baseName}/doc/{id_doc}";
            var response = await _lbgHttpClient.Get(seachPath);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }
            var retVal = contentBody;
            return default(TEntity);
        }

        public async Task<TEntity> SelectDocumentByIdDoc<TEntity>(string id_doc, string baseName, string field)
        {
            string seachPath = $"{baseName}/doc/{id_doc}/{field}";
            var response = await _lbgHttpClient.Get(seachPath);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }
            var retVal = contentBody;
            var jarray = _serializer.Deserialize<TEntity>(retVal);
            return jarray;
        }

        public async Task<SelectDocumentResult> SelectDocumentAll<SelectDocumentResult>(string baseName, int? limit)
        {
            if (String.IsNullOrEmpty(baseName))
                throw new ArgumentException("Nome da base nao pode ser nulo");
            var limitResult = limit.HasValue ? limit : 100;
            string seachPath = $"{baseName}/doc?$$={{\"select\":[\"*\"],\"limit\":{limitResult}}}";
            var response = await _lbgHttpClient.Get(seachPath);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }

            var retVal = contentBody;
            SelectDocumentResult base_results =  _serializer.Deserialize<SelectDocumentResult>(contentBody);
            return base_results;
        }

        public async Task<InsertFileIntoBaseResult> InsertFileIntoBase(string baseName, 
            StreamContent streamContent,string fileName)
        {
            if (String.IsNullOrEmpty(baseName))
                throw new ArgumentException("Nome da base nao pode ser nulo");
            if (!( streamContent!=null ))
                throw new ArgumentException("Stream nao pode ser nulo");
            var path = baseName + "/file";
            var response = await _lbgHttpClient.PostFile(path, streamContent, "file", fileName);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }
            InsertFileIntoBaseResult insertResult = _serializer.Deserialize<InsertFileIntoBaseResult>(contentBody);
            return insertResult;
        }

        public async Task<string> AttachFileIntoDocument(InsertFileIntoBaseResult jsonFileDesc, string baseName, 
            string documentId, string fileField)
        {
            var path = baseName + "/doc/" + documentId + "/" + fileField;
            string jsonString = _serializer.Serialize<InsertFileIntoBaseResult>(jsonFileDesc);
            List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>
            {
                    new KeyValuePair<string, string>("value", jsonString)
            };

            HttpContent httpContent = new FormUrlEncodedContent(nameValueCollection);
            var response = await _lbgHttpClient.Put(path, httpContent);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }
            return contentBody;
        }

        public async Task<string> InsertFile(FileStream fileStream,string baseName, string documentId, string fileField)
        {
            var contentStream = new StreamContent(fileStream);
            var fullName = fileStream.Name;
            var fileName = Path.GetFileName(fullName);
            InsertFileIntoBaseResult inserBaseResult = 
                await InsertFileIntoBase(baseName, contentStream, fileName);
            string  result = await AttachFileIntoDocument(inserBaseResult, baseName, documentId, fileField);

            return result;
        }

        // TODO https://www.newtonsoft.com/json/help/html/JsonSchema.htm
        // Validar schema

        public Task<HttpResponseMessage> CreateBase(string json_base)
        {
            if (json_base != null)
            {
                if (String.IsNullOrEmpty(json_base) ||
                    String.IsNullOrWhiteSpace(json_base))
                      throw new Exception("base nao contem nome");
                List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("json_base", json_base)
                };
                return _lbgHttpClient.Post("", nameValueCollection);
            }
            else
            {
                throw new ArgumentNullException("base nao pode ser nula");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basePath">Base Name</param>
        /// <param name="jsonSearch">exemple: {“literal”: “id_doc = 1”} </param>
        /// <param name="commands"> exemple: http://www.brlight.org/en/documentacao/#mode-patch </param>
        /// <returns></returns>
        public async Task<string> PartiallyChangeDocumentNode(string basePath, 
            string jsonSearch, List<ChangeDocumentNode> commands)
        {
            string searchPath = $"{basePath}/doc?$$={jsonSearch}";
            string jsonStringCommands = _serializer.Serialize<List<ChangeDocumentNode>>(commands);
            List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>
            {
                    new KeyValuePair<string, string>("path", jsonStringCommands),
            };
            var response = await _lbgHttpClient.Patch(searchPath, nameValueCollection);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }

            return contentBody;
        }

        public async Task<string> GetFileMetadata(string basePath, string id_file)
        {
            string searchPath = $"{basePath}/file/{id_file}";
            var response = await _lbgHttpClient.Get(searchPath);
            var contentBody = "";
            if (response.IsSuccessStatusCode)
            {
                Stream receiveStream = await response.Content.ReadAsStreamAsync();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                contentBody = readStream.ReadToEnd();
            }

            var retVal = contentBody;
            return retVal;
        }
        // TODO: usar getfilemetadata para tirarar esses 1000 de buff e usar nome tmb
        public async Task<HttpResponseMessage> DownloadFile(string basePath , string id_file)
        {
            string searchPath = $"{basePath}/file/{id_file}/download?disposition=attachment";
            var response = await _lbgHttpClient.Get(searchPath);
            return response;
        }
    }
}
