﻿using System;
using System.Collections.Generic;
using System.Text;


namespace LightBaseNeoLib.Interfaces
{
    public class IBase
    {
        public BaseContent Content { get; set; }
        public BaseMetadata Metadata { get; set; }
    }
}
