﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightBaseNeoLib.Interfaces
{
    public interface ISerializer
    {
        string Serialize<TEntity>(TEntity entity);
        TEntity Deserialize<TEntity>(string entity);
    }
}
