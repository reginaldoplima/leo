﻿using LightBaseNeoLib.Core;
using LightBaseNeoLib.Interfaces.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace LightBaseNeoLib.Interfaces
{
    public interface ILightBaseNeoApp : IDisposable
    {
        Task<HttpResponseMessage> Command(ICommand _command);

        Task<HttpResponseMessage> CreateBase(NeoBase _base);
        Task<HttpResponseMessage> CreateBase(string json_base);
        Task<NeoBase> RecoverBase(string _basePath);
        Task<HttpResponseMessage> DeleteBase(string _basePath);

        Task<SelectBaseResult> SelectBases(string baseName);

        Task<int> InsertDocument(IDocument _document, string _basePath);
        Task<int> InsertDocument(string jsonString, string _basePath);
        Task<InsertFileIntoBaseResult> InsertFileIntoBase(string baseName, StreamContent streamContent, string fileName);
        Task<string> InsertFile(FileStream fileStream, string baseName, string documentId, string fileField);
        Task<string> AttachFileIntoDocument(InsertFileIntoBaseResult json_result,string baseName,string documentId, string fileField);
        Task<HttpResponseMessage> PatchDocument<TDocument>(TDocument _document, string _basePath, string _documentId);
        Task<string> PartiallyChangeDocumentNode(string basePath, string jsonSearch, List<ChangeDocumentNode> commands);
        Task<TEntity> GetDocument<TEntity>(string _documentId, string _basePath);
        Task<TEntity> SelectDocument<TEntity>(SelectWithFilters selectString,string baseName);
        Task<SelectDocumentResult> SelectDocumentAll<SelectDocumentResult>(string baseName, int? limit);
        Task<HttpResponseMessage> DeleteDocument(string _documentId, string _basePath);
        Task<TEntity> SelectDocumentByIdDoc<TEntity>(string id_doc, string baseName);
        Task<TEntity> SelectDocumentByIdDoc<TEntity>(string id_doc, string baseName, string field);
        Task<string> GetFileMetadata(string basePath, string id_file);
        Task<HttpResponseMessage> DownloadFile(string basePath , string id_file);
        Task<HttpResponseMessage> PatchDocument<TDocument>(TDocument _document, string _basePath, string _documentId, string field);
    }
}
