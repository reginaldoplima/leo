﻿using LightBaseNeoLib.Utils;
using System;
using System.Runtime.Serialization;

namespace LightBaseNeoLib.Interfaces
{
    public class BaseMetadata
    {  
        public string[] Admin_Users { get; set; }
        public bool File_Ext { get; set; }
        public string Idx_exp_url { get; set; }
        public bool Idx_exp { get; set; }
        public int File_ext_time { get; set; }
        public string Owner { get; set; }
        public int Id_Base { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public string Dt_Base { get; set; }
        public dynamic Model { get; set; }
        public string Name { get; set; }
        public int Idx_Exp_Time { get; set; }
    }
}
