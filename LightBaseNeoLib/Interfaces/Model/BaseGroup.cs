﻿using LightBaseNeoLib.Utils;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace LightBaseNeoLib.Interfaces
{
    public class BaseGroup
    {
        public IList<BaseContentItem> Content { get; set; }
        public GroupMetadata Metadata { get; set; }
    }
}