﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightBaseNeoLib.Interfaces
{

    public class BaseContentItem
    {
        public BaseField Field { get; set; }
        public BaseGroup Group { get; set; }
    }
}
