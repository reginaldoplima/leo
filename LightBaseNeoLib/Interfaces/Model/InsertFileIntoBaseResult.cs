﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightBaseNeoLib.Interfaces.Model
{
    public class InsertFileIntoBaseResult
    {
        public string Mimetype { get; set; }
        public int Filesize { get; set; }
        public string Id_File { get; set; }
        public string Uuid { get; set; }
        public string Filename { get; set; }
    }
}
