﻿using LightBaseNeoLib.Utils;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace LightBaseNeoLib.Interfaces
{
    public  class  BaseContent 
    {
        public IList<BaseContentItem> Content { get; set; }
        public BaseMetadata Metadata { get; set; }
    }
}
