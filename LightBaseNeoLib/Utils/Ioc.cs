﻿using LightBaseNeoLib.Core;
using LightBaseNeoLib.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LightBaseNeoLib.Utils
{
    public class Ioc
    {
        //private static Uri _lbneoUri = new Uri("http://lbapp:6543");
        public static void Register(IServiceCollection services, string lbgUrl)
        {
            Uri _lbneoUri = new Uri($"http://{lbgUrl}");
            var config = new ClientConfig(_lbneoUri);
            var serializer = new JsonSerializer();
            services.AddSingleton<ILightBaseNeoConfig>(config);
            services.AddSingleton<ISerializer>(serializer);
            services.AddScoped<ILightbaseNeoClient, LightBaseNeoClient>();
            services.AddScoped<ILightBaseNeoApp, LightBaseNeoApp>();
        }
    }
}
