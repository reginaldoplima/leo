﻿using LightBaseNeoLib.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LightBaseNeoLib.Utils
{
    public class JsonSerializer : ISerializer
    {
        public string Serialize<TEntity>(TEntity entity)
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new LowercaseContractResolver()
            };
            return JsonConvert.SerializeObject(entity,Formatting.None,settings);
        }

        public TEntity Deserialize<TEntity>(string entity)
        {
            TEntity retVal = JsonConvert.DeserializeObject<TEntity>(entity);
            return retVal;
        }


    }

    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }
}
